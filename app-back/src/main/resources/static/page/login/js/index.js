let app = Vue.createApp({
    template: `    
    <el-card class="login" @keydown="keydown">
        <el-header>
            <h2>系统登录</h2>
        </el-header>
        <el-form ref="formRef" :model="form" :rules="rules">
            <el-form-item prop="account" label="账号">
                <el-input v-model="form.account"></el-input>
            </el-form-item>
            <el-form-item prop="password" label="密码">
                <el-input type="password" v-model="form.password"></el-input>
            </el-form-item>
            <el-button type="primary" @click="login">登录</el-button>
        </el-form>
    </el-card>`, setup() {
        const form = Vue.reactive({
            account: 'lanmuc', password: '123456'
        })
        const rules = Vue.reactive({
            account: [{required: true, message: '请输入账号', trigger: 'blur'}, {
                min: 2, max: 16, message: '长度在 2 到 16 个字符', trigger: 'blur'
            }], password: [{required: true, message: '请输入密码', trigger: 'blur'}, {
                min: 2, max: 16, message: '长度在 2 到 16 个字符', trigger: 'blur'
            }]
        })

        const formRef = Vue.ref(null)

        const keydown = (e) => {
            if (e.keyCode === 13) login()
        }
        const login = async () => {
            await formRef.value.validate()
            const res = await axios.post('pt/user/login', form)
            localStorage.setItem('token', res.body.token)
            localStorage.setItem('user', JSON.stringify(res.body.user))
            location.href = '/index.html'
        }
        return {
            keydown, login, form, formRef, rules
        }
    }
})
app.use(ElementPlus, ele_plus_ext).mount('#app')
