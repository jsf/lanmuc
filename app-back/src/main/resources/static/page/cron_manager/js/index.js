importCSS("/page/role_manager/css/index.css");
import api from "../config/api.js";

export default {
    name: "data_manager",
    template: `
    <div id="cron_manager" lanmu="cron_manager">
        <mySearch @add="tableOperation.addClick" @search="loadData.loadTable"></mySearch>
        <myTable ref="myTableRef">
            <el-table-column prop="name" sortable label="任务名称" width="300"/>
            <el-table-column prop="tag" label="任务标识" width="300" />
            <el-table-column prop="code" label="定时规则" />
            <el-table-column prop="url" label="调用路径" />
            <el-table-column prop="version" label="执行次数" />
            <el-table-column prop="state" label="任务状态" width="100" align="center">
                <template #default="scope">
                    <el-tag v-if="scope.row.state" type="success">启用</el-tag>
                    <el-tag v-else type="danger">禁用</el-tag>
                </template>
            </el-table-column>
            <el-table-column label="操作" width="340" align="center">
                <template  #default="scope">
                    <el-button type="warning" size="small" @click="tableOperation.editClick(scope.row)">编辑</el-button>
                    <confirmButton @ok="tableOperation.del" :row="scope.row">删除</confirmButton>
                </template>
            </el-table-column>
        </myTable>
        <saveForm ref="saveFormRef" @save="tableOperation.save"></saveForm>
    </div>`,
    setup(props, context) {
        const myTableRef = Vue.ref(), saveFormRef = Vue.ref();
        let myTable, saveForm;
        const loadData = {
            loadTable: async () => {
                myTable.loading = true
                let json = {
                    pageIndex: myTable.page.index,
                    pageSize: myTable.page.size,
                }
                const {page, list} = await api.queryTable(json)
                myTable.page.index = page.index
                myTable.page.total = page.total
                myTable.tableData = list
                myTable.loading = false
            }
        }
        const tableOperation = {
            addClick() {
                saveForm.title = "新增定时任务"
                saveForm.formClear()
                saveForm.visible = true
            },
            editClick(row) {
                saveForm.title = "编辑定时任务"
                saveForm.formClear()
                Object.assign(saveForm.formData, row)
                saveForm.visible = true
            },
            async save(data) {
                await api.save(data)
                saveForm.visible = false
                await loadData.loadTable()
            },
            async del({id}) {
                await api.delete({id})
                await loadData.loadTable()
            }
        }
        const init = (async () => {
            [myTable, saveForm] = await getRefs([myTableRef, saveFormRef])
            await loadData.loadTable()
        })()
        return {
            myTableRef,
            saveFormRef,
            loadData,
            tableOperation
        }
    },
    components: {
        mySearch: (await import('/component/search/js/index.js')).default,
        myTable: (await import('/component/table/js/index.js')).default,
        confirmButton: (await import('/component/confirm_button/js/index.js')).default,
        saveForm: (await import('/page/cron_manager/js/form.js')).default,
    }
}