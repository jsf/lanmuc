export default {
    queryTable: async (json) => {
        const {body} = await axios.post("pt/cron/list", {})
        return body
    },
    save: async (json) => {
        let body
        if (json.id) {
            const {body} = await axios.post("pt/cron/put", json)
        } else {
            const {body} = await axios.post("pt/cron/post", json)
        }
        ElementPlus.ElMessage.success({message: "操作成功", duration: 2000});
        return body
    },
    delete: async (json) => {
        const {body} = await axios.post("pt/cron/delete", json)
        ElementPlus.ElMessage.success({message: "操作成功", duration: 2000});
        return body
    },
}