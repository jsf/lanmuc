const formSource = () => ({
    id: null,
    name: '',
    tag: '',
    code: '',
    des: '',
    url: '',
    token: localStorage.getItem("token"),
    state: true
})
export const form = {
    formRules: {
        name: [
            {required: true, message: '任务名称为必填', trigger: 'blur'},
        ],
        tag: [
            {required: true, message: '任务标识为必填', trigger: 'blur'},
        ],
        code: [
            {required: true, message: '定时规则为必填', trigger: 'blur'},
        ],
        sql: [
            {required: true, message: '定时规则为必填', trigger: 'blur'},
        ],
        url: [
            {required: true, message: '调用路径为必填', trigger: 'blur'},
        ],
        token: [
            {required: true, message: '凭证为必填', trigger: 'blur'},
        ],
        version: [
            {required: true, message: '执行次数为必填', trigger: 'blur'},
        ],
        state: [
            {required: true, message: '任务状态为必选择', trigger: 'change'},
        ],
    },
    formSource,
    formData: Vue.reactive(formSource()),
    formClear: () => Object.assign(form.formData, formSource())
}