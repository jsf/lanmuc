export default {
    queryAll: async () => {
        const {body} = await axios.post("pt/module/all", {})
        return body
    }, queryRoots: async () => {
        const {body} = await axios.post("pt/module/roots", {})
        return body
    }, queryTable: async (id) => {
        const {body} = await axios.post(`pt/module/root`, {id})
        return body
    }, queryPermission: async () => {
        const {body} = await axios.post("pt/permission/all", {})
        return body
    }, permission: async (json) => {
        const {body} = await axios.post("pt/module/permission", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    }, save: async (json) => {
        const {body} = await axios.post("pt/module/save", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    }, del: async (id) => {
        const {body} = await axios.post(`pt/module/del`, {id})
        ElementPlus.ElMessage.success({message: "删除成功", duration: 2000});
        return body
    }, interface: async (json) => {
        const {body} = await axios.post(`pt/permission/saveAndBingModule`, json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    },
}