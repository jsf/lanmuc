const saveFormSource = () => ({id: null, pid: null, name: "", path: null, component: null, meta: null, sort: null})
export const save = {
    formRules: {
        name: [{required: true, message: '角色名为必填', trigger: 'blur'}, {
            min: 2,
            max: 20,
            message: '长度2~20',
            trigger: 'blur'
        },], pid: [{required: true, message: '父级菜单为必填', trigger: 'blur'}]
    },
    formSource: saveFormSource,
    formData: Vue.reactive(saveFormSource()),
    formClear: () => Object.assign(save.formData, saveFormSource())
}

//permission.js资源
const permissionFormSource = () => ({id: null, permission: []})
export const permission = {
    formData: Vue.reactive(permissionFormSource()),
    formClear: () => Object.assign(permission.formData, permissionFormSource())
}