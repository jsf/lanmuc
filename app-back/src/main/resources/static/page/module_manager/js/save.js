import {save as form} from "../config/index.js";

export default {
    template: `
         <el-dialog
            v-model="visible"
            :title="title"
            width="30%"
          >
            <el-form :model="formData" :rules="formRules" ref="formRef" label-width="80px">
                <el-form-item label="模块名称" prop="name">
                    <el-input clearable v-model="formData.name" />
                </el-form-item>
                <el-form-item label="父级菜单" prop="pid">
                    <el-cascader style="width: 100%;user-select: none"
                        v-model="formData.pid"  
                        :options="options" 
                        :props="{label:'name',value:'id',checkStrictly:true,emitPath:false}"}" 
                        clearable
                    />
                </el-form-item>
                <el-form-item label="url路径" prop="path">
                    <el-input clearable v-model="formData.path" />
                </el-form-item>
                <el-form-item label="组件路径" prop="component">
                    <el-input clearable v-model="formData.component" />
                </el-form-item>
                <el-form-item label="拓展信息" prop="meta">
                    <el-input clearable v-model="formData.meta" />
                </el-form-item>
                <el-form-item label="排序号" prop="sort">
                    <el-input clearable v-model="formData.sort" />
                </el-form-item>
            </el-form>
            <template #footer>
              <span class="dialog-footer">
                <el-button @click="visible=false">取消</el-button>
                <el-button type="primary" @click="operation.save">保存</el-button>
              </span>
            </template>
        </el-dialog>
    `,
    setup(props, context) {
        const formRef = Vue.ref(null);
        const visible = Vue.ref(false)
        const options = Vue.ref([])
        const title = Vue.ref("")
        const operation = {
            save: async () => {
                (await getRef(formRef)).validate(async (valid) => {
                    if (valid) {
                        context.emit("save", form.formData)
                    }
                })
            }
        }
        Vue.watch(visible, async val => val || (await getRef(formRef)).resetFields())
        return Object.assign({
            visible,
            options,
            title,
            formRef,
            operation
        }, form)
    }
}