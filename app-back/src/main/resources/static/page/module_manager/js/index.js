importCSS("/page/module_manager/css/index.css");
import api from "../config/api.js";

export default {
    template: `
    <div id="permission_manager" lanmu="permission_manager">
        <mySearch @add="tableOperation.addClick" @search="loadData.loadTable">
            <el-select @change="loadData.loadTable" v-model="select.value" placeholder="请选择模块">
                <el-option v-for="item in select.options" :label="item.name" :value="item.id" />
            </el-select>
        </mySearch>
        <myTable ref="myTableRef" :showPage="false">
            <el-table-column type="index" label="序号" width="80" align="center" />
            <el-table-column prop="name" label="模块名称" align="center"/>
            <el-table-column prop="tag" label="tag标志" align="center" />
            <el-table-column prop="path" label="url路径" align="center" />
            <el-table-column prop="component" label="组件路径" align="center" />
            <el-table-column prop="meta" label="拓展信息" align="center" />
            <el-table-column prop="sort" label="排序号" align="center" />
            <el-table-column label="操作" width="400" align="center">
                <template  #default="scope">
                    <el-button type="success" size="small" @click="tableOperation.copyClick(scope.row)">复制模块</el-button>
                    <el-button type="primary" size="small" @click="tableOperation.createClick(scope.row)">创建接口</el-button>
                    <el-button type="info" size="small" @click="tableOperation.permissionClick(scope.row)">绑定接口</el-button>
                    <el-button type="warning" size="small" @click="tableOperation.editClick(scope.row)">编辑</el-button>
                    <confirmButton @ok="tableOperation.del" :row="scope.row">删除</confirmButton>
                </template>
            </el-table-column>
        </myTable>
        <saveForm ref="saveFormRef" @save="tableOperation.save"></saveForm>
        <permissionForm ref="permissionFormRef" @save="tableOperation.permission"></permissionForm>
        <interfaceForm ref="interfaceFormRef" @save="tableOperation.interface"></interfaceForm>
    </div>`, setup(props, context) {
        const myTableRef = Vue.ref(), saveFormRef = Vue.ref(), permissionFormRef = Vue.ref(),
            interfaceFormRef = Vue.ref();
        let myTable, saveForm, permissionForm, interfaceForm, moduleId;
        const select = Vue.reactive({value: null, options: []});
        const loadData = {
            loadRoots: async () => {
                const res = await api.queryRoots()
                select.options.push(...res)
                select.value = res[0].id
            },
            loadTable: async () => {
                if (!!select.value) {
                    myTable.loading = true
                    const list = await api.queryTable(select.value)
                    list.forEach(e => {
                        e.permission = e.permissionId ? e.permissionId.split(",").map(e => Number(e)) : []
                    })
                    myTable.tableData = list
                    myTable.loading = false
                }

            },
            loadAll: async () => {
                const res = await api.queryAll()
                saveForm.options = res
            },
        }
        const tableOperation = {
            addClick() {
                saveForm.title = "新增模块"
                saveForm.formClear()
                saveForm.visible = true
            }, createClick({id, permission}) {
                interfaceForm.title = "新增接口"
                interfaceForm.formClear()
                moduleId = id
                interfaceForm.visible = true
            }, async permissionClick({id, permission}) {
                permissionForm.formClear()
                Object.assign(permissionForm.formData, {id, permission})
                permissionForm.visible = true
                while (!permissionForm.treeRef) await sleep(10)
                permissionForm.treeRef.setCheckedKeys(permission)
            }, editClick({id, pid, name, path, component, meta, sort}) {
                saveForm.title = "编辑模块"
                saveForm.formClear()
                Object.assign(saveForm.formData, {id, pid, name, path, component, meta, sort})
                saveForm.visible = true
            }, copyClick({pid, name, path, component, meta, sort}) {
                saveForm.title = "新增模块"
                saveForm.formClear()
                Object.assign(saveForm.formData, {pid, name, path, component, meta, sort})
                saveForm.visible = true
            }, async permission(data) {
                await api.permission(data);
                permissionForm.visible = false
                await loadData.loadTable()
            }, async save(data) {
                await api.save(data);
                saveForm.visible = false
                await loadData.loadTable()
            }, async del({id}) {
                await api.del(id)
                await loadData.loadTable()
            }, async interface(data) {
                await api.interface({...data, moduleId});
                interfaceForm.visible = false
            }
        }
        const init = (async () => {
            [myTable, saveForm, permissionForm, interfaceForm] = await getRefs([myTableRef, saveFormRef, permissionFormRef, interfaceFormRef])
            await loadData.loadRoots()
            await loadData.loadTable()
            await loadData.loadAll()
        })()
        return {
            select, myTableRef, saveFormRef, permissionFormRef, interfaceFormRef, loadData, tableOperation, dateFormat
        }
    }, components: {
        mySearch: (await import('/component/search/js/index.js')).default,
        myTable: (await import('/component/table/js/index.js')).default,
        confirmButton: (await import('/component/confirm_button/js/index.js')).default,
        saveForm: (await import('/page/module_manager/js/save.js')).default,
        interfaceForm: (await import('/page/interface_manager/js/save.js')).default,
        permissionForm: (await import('/page/module_manager/js/permission.js')).default,
    }
}