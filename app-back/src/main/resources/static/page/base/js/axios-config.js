axios.defaults.timeout = 30000;
const baseURL = ''
axios.interceptors.request.use(config => {
    config.url = baseURL + config.url
    if (!config.url.startsWith('http')) {
        config.headers['Content-Type'] = config.headers['Content-Type'] || 'application/json'
        let token = localStorage.getItem('token') || ''
        if (token) config.headers['token'] = token;
    }
    return config;
}, err => {
    console.log(err)
})
axios.interceptors.response.use(res => {
    if (!res.data.code) return res.data;
    if (res.data.code?.value !== 200) {
        if (res.data.code?.value === 401) {
            localStorage.removeItem('token')
            location.href = '/login.html'
        }
        ElementPlus.ElMessage.error(res.data.code.describe);
        throw new Error(res.data.code.describe);
    }
    return res.data;
}, err => {
    ElementPlus.ElMessage.error("服务端关闭");
})