window.importCSS = url => {
    let css = document.createElement('link');
    css.setAttribute('rel', 'stylesheet');
    css.setAttribute('href', url);
    document.body.appendChild(css);
}
window.getRef = async (ref, i = 0) => {
    if (ref.value) {
        return ref.value
    } else {
        await Vue.nextTick()
        if (i < 3) return getRef(ref, ++i)
        else {
            console.log(`获取不到Ref节点`)
            return undefined
        }

    }
}
window.getRefs = async (refs) => {
    return await Promise.all([...refs.map(async (ref) => await getRef(ref))])
}
//将时间戳转换为日期格式yyyy-MM-dd HH:mm:ss
window.dateFormat = (time) => {
    if (!time) return "";
    let date = new Date(time);
    let year = date.getFullYear();
    let month = ('0' + (date.getMonth() + 1)).slice(-2);
    let day = ('0' + date.getDate()).slice(-2);
    let hour = ('0' + date.getHours()).slice(-2);
    let minute = ('0' + date.getMinutes()).slice(-2);
    let second = ('0' + date.getSeconds()).slice(-2);
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}
//使用promise写一个睡眠函数
window.sleep = (time) => {
    return new Promise((resolve) => setTimeout(resolve, time))
}
/**
 * json字符串转json对象
 * @param { String } jsonStr json字符串
 */
window.parseJson = (jsonStr) => {
    return JSON.parse(jsonStr, (k, v) => {
        try {
            return eval(v);
        } catch (e) {
            return v;
        }
    });
}

/**
 * json对象转json字符串
 * @param { Object } json json对象
 */
window.stringifyJson = (json) => {
    return JSON.stringify(json, (k, v) => {
        if (v instanceof RegExp) return v.toString();
        return v;
    });
}