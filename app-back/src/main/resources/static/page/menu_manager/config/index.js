//form.js资源
const formSource = () => ({id: null, pid: 0, title: "", name: "", path: "", component: ""})
export const form = {
    formRules: {
        pid: [
            {required: true, message: '父级菜单为必选', trigger: 'blur'}
        ],
        title: [
            {required: true, message: '菜单标题为必填', trigger: 'blur'},
            {min: 2, max: 8, message: '长度2~8', trigger: 'blur'},
        ],
        name: [
            {required: true, message: '路由名为必填', trigger: 'blur'},
            {min: 2, max: 20, message: '长度2~20', trigger: 'blur'},
        ],
        path: [
            {required: true, message: '路由路径为必填', trigger: 'blur'},
            {min: 2, max: 60, message: '长度2~60', trigger: 'blur'},
        ],
        component: [
            {min: 2, max: 80, message: '长度2~80', trigger: 'blur'},
        ]
    },
    formSource,
    formData: Vue.reactive(formSource()),
    formClear: () => Object.assign(form.formData, formSource())
}