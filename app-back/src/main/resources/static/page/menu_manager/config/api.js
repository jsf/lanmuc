export default {
    queryTable: async (json) => {
        const {body} = await axios.post("pt/menu/all", {})
        return body
    },
    save: async (json) => {
        const {body} = await axios.post("pt/menu/save", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    },
    del: async (id) => {
        const {body} = await axios.post(`pt/menu/del`, {id})
        ElementPlus.ElMessage.success({message: "删除成功", duration: 2000});
        return body
    },
    setSort: async (json) => {
        const {body} = await axios.post(`pt/menu/setSort`, json)
        ElementPlus.ElMessage.success({message: "已完成", duration: 500});
        return body
    }
}