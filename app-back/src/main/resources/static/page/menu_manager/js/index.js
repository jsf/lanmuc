importCSS("/page/menu_manager/css/index.css");
import api from "../config/api.js";

export default {
    template: `
    <div id="menu_manager" lanmu="menu_manager">
        <mySearch @add="tableOperation.addClick" @search="loadData.loadTable">
            <el-button type="primary" v-show="!sortFlag" :icon="Rank" @click="tableOperation.sortClick">排序</el-button>    
            <el-button type="danger" v-show="sortFlag" :icon="Rank" @click="tableOperation.commitSort">提交</el-button>    
        </mySearch>
        <myTable ref="myTableRef" :showPage="false" @page-change="loadData.loadTable">
            <el-table-column prop="title" label="菜单标题" align="left" />
            <el-table-column prop="name" label="路由名" align="left" />
            <el-table-column prop="path" label="路由路径" align="left" />
            <el-table-column prop="component" label="组件路径" align="left">
                <template #default="scope">
                    <span v-text="scope.row.component||'-'"></span>
                </template>
            </el-table-column>
            <el-table-column label="操作" width="240" align="center">
                <template  #default="scope">
                    <el-button type="warning" size="small" @click="tableOperation.editClick(scope.row)">编辑</el-button>
                    <confirmButton @ok="tableOperation.del" :row="scope.row">删除</confirmButton>
                </template>
            </el-table-column>
        </myTable>
        <myForm ref="myFormRef" :options="tableData" @save="tableOperation.save"></myForm>
    </div>`,
    setup(props, context) {
        const myTableRef = Vue.ref(), myFormRef = Vue.ref(), tableData = Vue.ref([]), sortFlag = Vue.ref(false);
        let myTable, myForm, tableArr, sortObj;
        const loadData = {
            loadTable: async () => {
                myTable.loading = true
                let json = {
                    pageIndex: myTable.page.index,
                    pageSize: myTable.page.size,
                }
                const list = await api.queryTable(json)
                tableData.value = myTable.tableData = list
                tableArr = []
                indexToId(list, {i: 0})
                myTable.loading = false
            }
        }
        const tableOperation = {
            addClick() {
                myForm.title = "新增菜单"
                myForm.formClear()
                myForm.visible = true
            },
            editClick({id, pid, title, name, path, component}) {
                myForm.title = "编辑菜单"
                myForm.formClear()
                Object.assign(myForm.formData, {id, pid, title, name, path, component})
                myForm.visible = true
            },
            sortClick() {
                tableMove()
                sortFlag.value = true
            },
            async commitSort() {
                myTable.loading = true
                sortFlag.value = false
                let arr = []
                tableArr.map((item, i) => arr.push(...item))
                await api.setSort({list: arr.map((item, i) => ({id: item, sort: i}))})
                await loadData.loadTable()
                sortObj.options.disabled = true
            },
            async save(data) {
                await api.save(data)
                myForm.visible = false
                await loadData.loadTable()
            },
            async del({id}) {
                await api.del(id)
                await loadData.loadTable()
            }
        }
        const indexToId = (arr, index) => {
            arr.forEach(item => {
                tableArr.push([item.id])
                if (item.children) indexToId(item.children, index)
            })
        }
        const tableMove = () => {
            sortObj = new Sortable(document.querySelector('.el-table__body-wrapper tbody'), {
                group: 'sort',
                draggable: '.el-table__row',
                animation: 400,
                async onEnd({newIndex: n, oldIndex: o}) {
                    if (n > o) {
                        // 向下移动
                        tableArr[n].push(...tableArr[o])
                    } else {
                        // 向上移动
                        tableArr[n].unshift(...tableArr[o])
                    }
                    tableArr.splice(o, 1)
                    let arr = []
                    tableArr.forEach((item) => {
                        item.forEach((i) => {
                            arr.push([i])
                        })
                    })
                    tableArr = arr
                }
            });
        }
        const init = (async () => {
            [myTable, myForm] = await getRefs([myTableRef, myFormRef])
            await loadData.loadTable()
        })()
        return {
            Rank: ElementPlusIconsVue.Rank,
            sortFlag,
            myTableRef,
            myFormRef,
            loadData,
            tableData,
            tableOperation
        }
    },
    components: {
        mySearch: (await import('/component/search/js/index.js')).default,
        myTable: (await import('/component/table/js/index.js')).default,
        confirmButton: (await import('/component/confirm_button/js/index.js')).default,
        myForm: (await import('/page/menu_manager/js/form.js')).default,
    }
}