const {body: userInfo} = await axios.post("pt/user/userInfo", {})
if (!userInfo) location.href = '/login.html'
const app = Vue.createApp({
    template: `<layout></layout>`,
    setup() {
        Vue.provide("user", Vue.ref(JSON.parse(localStorage.getItem('user'))))
        return {}
    }
})
window.app = app
//同时发起请求获取js模块，解决资源顺序等待，影响渲染速度的问题
await Promise.all([
    (async () => app.component('layout', (await import('./layout.js')).default))(),
])
const router = VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes: []
})
window.router = router
//加载element-plus图标
for (let key in ElementPlusIconsVue) app.component(key, ElementPlusIconsVue[key])
app.use(ElementPlus, ele_plus_ext).use(router).mount('#app')
