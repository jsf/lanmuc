importCSS("/page/index/css/layout.css");
import api from "../config/api.js";

export default {
    name: "layout", template: `
    <div id="layout" lanmu="layout">
        <div class="left" :style="{'min-width':(collapse?'unset':'200px')}">
            <div class="top">
                <div class="icon">
                    <img src="/assets/img/logo.png" >
                </div>
                <div class="title" v-show="!collapse">
                    开发者平台
                </div>
            </div>
            <div class="bottom">
                <!--菜单-->
                <el-menu :collapse="collapse" :default-active="activeIndex">
                    <myMenu :menus="menus"></myMenu>
                </el-menu>
            </div>
        </div>
        <div class="right">
            <div class="top">
                <!--伸展和收缩图标-->
                <div class="collapse" @click="collapse=!collapse">
                    <Expand v-if="collapse" style="width: 1.5rem;" />
                    <Fold v-else style="width: 1.5rem;" />
                </div>
                <!--面包屑-->
                <div class="crumbs">
                    <el-breadcrumb separator="/">
                        <template v-for="(item,i) in crumbs" :key="i">
                            <el-breadcrumb-item to="?" v-if="i!==crumbs.length-1" >{{item.title}}</el-breadcrumb-item>
                            <el-breadcrumb-item v-else>{{item.title}}</el-breadcrumb-item>
                        </template>
                    </el-breadcrumb>
<!--                    <span style="flex: 1;text-align: center;color: blue;font-weight: bolder">兼容编码开发接口方式和配置接口方式，订购请加微信：lanmuc，备注姓名和从哪得知该系统</span>-->
                </div>
                <!--操作栏-->
                <div class="operation">
                    <!--用户信息及操作菜单-->
                    <div class="info">
                        <el-dropdown trigger="click">
                            <div class="title">
                                <img src="/favicon.ico">
                                <span v-text="user?.name"></span>
                            </div>
                            <template #dropdown>
                              <el-dropdown-menu>
                                <el-dropdown-item @click="changePasswordClick">修改密码</el-dropdown-item>
                                <el-dropdown-item @click="logout">退出系统</el-dropdown-item>
                              </el-dropdown-menu>
                            </template>
                          </el-dropdown>
                      </div>
                </div>
            </div>
            <div class="bottom">
                <router-view v-slot="{ Component }">
                    <transition name="fade" mode="out-in">
                        <keep-alive include="data_manager,interface_manager" >
                            <component :is="Component" />
                        </keep-alive>
                    </transition>
                </router-view> 
            </div>
        </div>
        <changePassword ref="changePasswordRef"  @save="changePassword"></changePassword>
    </div>`, setup() {
        //form表单,修改密码
        const changePasswordRef = Vue.ref();
        //组件引用
        let myChangePassword;
        //折叠菜单
        const collapse = Vue.ref(false)
        //菜单
        const menus = Vue.ref([])
        //树根节点递归加子节点
        const toTree = (root, arr) => {
            root.forEach(r => {
                arr.forEach(e => {
                    if (e.pid === r.id) {
                        r.children = r.children || []
                        r.children.push(e)
                    }
                })
                if (r.children && r.children.length > 0) toTree(r.children, arr)
            })
        }
        //渲染菜单树
        const renderMenuTree = async () => {
            const res = await axios.post('pt/user/menu', {})
            res.body.forEach(e => {
                e.meta = {title: e.title, id: e.id}
                if (e.component) {
                    e.componentDir = e.component
                    e.component = async () => (await import(`${e.componentDir}/js/index.js`)).default
                }
            })
            const root = res.body.filter(e => e.pid === 0)
            toTree(root, res.body)
            root.forEach(e => router.addRoute(e))
            menus.value = root
            let routers = router.getRoutes()
            for (let i = 0; i < routers.length; i++) {
                if (routers[i].components) {
                    while (router.currentRoute.value.path !== routers[i].path) {
                        router.replace({path: routers[i].path})
                        await sleep(5)
                    }
                    break
                }
            }
            return true
        }
        //面包屑
        const crumbs = Vue.computed(() => router.currentRoute.value.matched.map(e => ({
            path: e.path, title: e.meta.title
        })))
        //面包屑
        const activeIndex = Vue.computed(() => router.currentRoute.value.meta.id)
        //退出系统
        const logout = () => {
            localStorage.removeItem("token")
            location.href = "/login.html"
        }
        //刷新时根据路由路径载入菜单
        const refreshLoad = () => {
            let url = location.href
            router.replace({path: url.split("#")[1]})
        }
        //点击修改密码
        const changePasswordClick = () => {
            myChangePassword.title = "修改密码"
            myChangePassword.formClear()
            Object.assign(myChangePassword.formData, {})
            myChangePassword.visible = true
        }
        //修改密码
        const changePassword = async () => {
            await api.changePassword(myChangePassword.formData)
            logout()
        }
        //集合初始化方法
        const init = (async () => {
            await renderMenuTree();
            [myChangePassword] = await getRefs([changePasswordRef])
            refreshLoad()
        })()
        const user = Vue.inject("user")
        return {
            changePasswordRef, collapse, menus, crumbs, activeIndex, logout, changePasswordClick, changePassword, user
        }
    }, components: {
        myMenu: (await import('/component/menu/js/index.js')).default,
        changePassword: (await import('/page/index/js/my_change_password.js')).default,
    }
}