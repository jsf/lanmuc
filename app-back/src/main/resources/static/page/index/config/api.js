export default {
    changePassword: async (json) => {
        const {body} = await axios.post("pt/user/changePassword", json)
        return body
    },
}