//save.js资源
const saveFormSource = () => ({id: null, oldPassword: "", newPassword: ""})
export const save = {
    formRules: {
        oldPassword: [
            {required: true, message: '旧密码为必填', trigger: 'blur'},
            {min: 2, max: 16, message: '长度2~16', trigger: 'blur'},
        ],
        newPassword: [
            {required: true, message: '新密码为必填', trigger: 'blur'},
            {min: 2, max: 16, message: '长度2~16', trigger: 'blur'},
        ]
    },
    formSource: saveFormSource,
    formData: Vue.reactive(saveFormSource()),
    formClear: () => Object.assign(save.formData, saveFormSource())
}