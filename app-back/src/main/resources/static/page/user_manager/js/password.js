import {password as form} from "../config/index.js";
import api from "../config/api.js";

export default {
    template: `
         <el-dialog
            v-model="visible"
            title="重置密码"
            width="30%"
          >
            <el-form :model="formData" :rules="formRules" ref="formRef" label-width="80px">
                <el-form-item label="名字" prop="name">
                    <el-input disabled clearable v-model="formData.name" />
                </el-form-item>
                <el-form-item label="账号" prop="account">
                    <el-input disabled clearable v-model="formData.account" />
                </el-form-item>
                <el-form-item label="密码" prop="password">
                    <el-input clearable type="password" v-model="formData.password" />
                </el-form-item>
            </el-form>
            <template #footer>
              <span class="dialog-footer">
                <el-button @click="visible=false">取消</el-button>
                <el-button type="primary" @click="operation.save">保存</el-button>
              </span>
            </template>
        </el-dialog>
    `,
    setup(props, context) {
        const formRef = Vue.ref(null);
        const visible = Vue.ref(false)
        const roleOptions = Vue.ref([])
        const operation = {
            save: async () => {
                (await getRef(formRef)).validate(async (valid) => {
                    if (valid) {
                        context.emit("save", form.formData)
                    }
                })
            }
        }
        const init = (async () => {
            roleOptions.value = await api.queryRole()
        })()
        Vue.watch(visible, async val => val || (await getRef(formRef)).resetFields())
        return Object.assign({
            visible,
            roleOptions,
            formRef,
            operation
        }, form)
    }
}