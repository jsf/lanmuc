importCSS("/page/user_manager/css/index.css");
import api from "../config/api.js";

export default {
    template: `
    <div id="user_manager" lanmu="user_manager">
        <mySearch @add="tableOperation.addClick" @search="loadData.loadTable">
            <el-input v-model="search.name" placeholder="请输入姓名/账号" clearable />
        </mySearch>
        <myTable ref="myTableRef" @page-change="loadData.loadTable">
            <el-table-column type="index" label="序号" width="80" align="center" />
            <el-table-column prop="name" label="名字" align="center" />
            <el-table-column prop="account" label="账号" align="center" />
            <el-table-column label="角色" align="center">
                <template  #default="scope">
                    <div style="display: flex;gap:10px;justify-content: center;">
                        <el-tag v-for="item in scope?.row.roleName">{{item}}</el-tag>
                    </div>
                </template>
            </el-table-column>
            <el-table-column label="操作" width="340" align="center">
                <template  #default="scope">
                    <el-button type="info" size="small" @click="tableOperation.roleClick(scope.row)">设置角色</el-button>
                    <el-button type="success" size="small" @click="tableOperation.passwordClick(scope.row)">重置密码</el-button>
                    <confirmButton @ok="tableOperation.del" :row="scope.row">删除</confirmButton>
                </template>
            </el-table-column>
        </myTable>
        <addForm ref="addFormRef" @save="tableOperation.add"></addForm>
        <roleForm ref="roleFormRef" @save="tableOperation.role"></roleForm>
        <passwordForm ref="passwordFormRef" @save="tableOperation.password"></passwordForm>
    </div>`,
    setup(props, context) {
        const myTableRef = Vue.ref(), addFormRef = Vue.ref(), roleFormRef = Vue.ref(), passwordFormRef = Vue.ref();
        let myTable, addForm, roleForm, passwordForm;
        const search = Vue.reactive({name: ""});
        const loadData = {
            loadTable: async () => {
                myTable.loading = true
                let json = {
                    name: search.name,
                    pageIndex: myTable.page.index,
                    pageSize: myTable.page.size,
                }
                const {page, list} = await api.queryTable(json)
                list.forEach(e => {
                    e.roleName = e.roleName?.split(',')
                    e.role = e.roleId ? e.roleId.split(',').map(e => Number(e)) : []
                })
                myTable.page.index = page.index
                myTable.page.total = page.total
                myTable.tableData = list
                myTable.loading = false
            }
        }
        const tableOperation = {
            addClick() {
                addForm.formClear()
                addForm.visible = true
            },
            roleClick({id, name, account, role}) {
                roleForm.formClear()
                Object.assign(roleForm.formData, {
                    id,
                    name,
                    account,
                    role
                })
                roleForm.visible = true
            },
            passwordClick({id, name, account}) {
                passwordForm.formClear()
                Object.assign(passwordForm.formData, {id, name, account})
                passwordForm.visible = true
            },
            async add(data) {
                await api.add(data)
                addForm.visible = false
                await loadData.loadTable()
            },
            async role(data) {
                await api.setRole(data)
                roleForm.visible = false
                await loadData.loadTable()
            },
            async password(data) {
                await api.setPassword(data)
                passwordForm.visible = false
                await loadData.loadTable()
            },
            async del({id}) {
                await api.del(id)
                await loadData.loadTable()
            }
        }
        const init = (async () => {
            [myTable, addForm, roleForm, passwordForm] = await getRefs([myTableRef, addFormRef, roleFormRef, passwordFormRef])
            await loadData.loadTable()
        })()
        return {
            search,
            myTableRef,
            addFormRef,
            roleFormRef,
            passwordFormRef,
            loadData,
            tableOperation
        }
    },
    components: {
        mySearch: (await import('/component/search/js/index.js')).default,
        myTable: (await import('/component/table/js/index.js')).default,
        confirmButton: (await import('/component/confirm_button/js/index.js')).default,
        addForm: (await import('/page/user_manager/js/add.js')).default,
        roleForm: (await import('/page/user_manager/js/role.js')).default,
        passwordForm: (await import('/page/user_manager/js/password.js')).default,
    }
}