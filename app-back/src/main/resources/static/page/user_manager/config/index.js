//add.js资源
const addFormSource = () => ({id: null, name: "", account: "", password: "", role: []})
export const add = {
    formRules: {
        name: [
            {required: true, message: '用户名为必填', trigger: 'blur'},
            {min: 2, max: 10, message: '长度2~10', trigger: 'blur'},
        ],
        account: [
            {required: true, message: '账号为必填', trigger: 'blur'},
            {min: 2, max: 10, message: '长度2~10', trigger: 'blur'},
        ],
        password: [
            {required: true, message: '密码为必填', trigger: 'blur'},
            {min: 2, max: 20, message: '长度2~20', trigger: 'blur'},
        ],
    },
    formSource: addFormSource,
    formData: Vue.reactive(addFormSource()),
    formClear: () => Object.assign(add.formData, addFormSource())
}

//role.js资源
const roleFormSource = () => ({id: null, name: "", account: "", role: []})
export const role = {
    formSource: roleFormSource,
    formData: Vue.reactive(roleFormSource()),
    formClear: () => Object.assign(role.formData, roleFormSource())
}

//password.js资源
const passwordFormSource = () => ({id: null, name: "", account: "", password: ""})
export const password = {
    formRules: {
        password: [
            {required: true, message: '密码为必填', trigger: 'blur'},
            {min: 2, max: 20, message: '长度2~20', trigger: 'blur'},
        ],
    },
    formSource: passwordFormSource,
    formData: Vue.reactive(passwordFormSource()),
    formClear: () => Object.assign(password.formData, passwordFormSource())
}