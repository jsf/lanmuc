export default {
    queryTable: async (json) => {
        const {body} = await axios.post("pt/user/list", json)
        return body
    },
    add: async (json) => {
        const {body} = await axios.post("pt/user/add", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    },
    del: async (id) => {
        const {body} = await axios.post(`pt/user/del`, {id})
        ElementPlus.ElMessage.success({message: "删除成功", duration: 2000});
        return body
    },
    setRole: async (json) => {
        const {body} = await axios.post("pt/user/setRole", json)
        ElementPlus.ElMessage.success({message: "修改成功", duration: 2000});
        return body
    },
    setPassword: async (json) => {
        const {body} = await axios.post("pt/user/setPassword", json)
        ElementPlus.ElMessage.success({message: "修改成功", duration: 2000});
        return body
    },
    queryRole: async () => {
        const {body} = await axios.post(`pt/role/all`, {})
        return body
    }
}