export default {
    queryTable: async (json) => {
        const {body} = await axios.post("pt/role/list", json)
        return body
    },
    save: async (json) => {
        const {body} = await axios.post("pt/role/save", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    },
    setMenu: async (json) => {
        const {body} = await axios.post("pt/role/setMenu", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    },
    setModule: async (json) => {
        const {body} = await axios.post("pt/role/setModule", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    }, del: async (id) => {
        const {body} = await axios.post(`pt/role/del`, {id})
        ElementPlus.ElMessage.success({message: "删除成功", duration: 2000});
        return body
    }, queryMenu: async (json) => {
        const {body} = await axios.post("pt/menu/all", {})
        return body
    }, queryModule: async (json) => {
        const {body} = await axios.post("pt/module/all", {})
        return body
    },
}