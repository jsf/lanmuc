//save.js资源
const saveFormSource = () => ({id: null, name: ""})
export const save = {
    formRules: {
        name: [
            {required: true, message: '角色名为必填', trigger: 'blur'},
            {min: 2, max: 10, message: '长度2~10', trigger: 'blur'},
        ]
    },
    formSource: saveFormSource,
    formData: Vue.reactive(saveFormSource()),
    formClear: () => Object.assign(save.formData, saveFormSource())
}

//menu.js资源
const menuFormSource = () => ({id: null, menu: []})
export const menu = {
    formData: Vue.reactive(menuFormSource()),
    formClear: () => Object.assign(menu.formData, menuFormSource())
}

//module.js资源
const moduleFormSource = () => ({id: null, module: []})
export const module = {
    formData: Vue.reactive(moduleFormSource()),
    formClear: () => Object.assign(module.formData, moduleFormSource())
}