importCSS("/page/role_manager/css/index.css");
import api from "../config/api.js";

export default {
    template: `
    <div id="role_manager" lanmu="role_manager">
        <mySearch @add="tableOperation.addClick" @search="loadData.loadTable"></mySearch>
        <myTable ref="myTableRef" @page-change="loadData.loadTable">
            <el-table-column type="index" label="序号" width="80" align="center" />
            <el-table-column prop="name" label="角色名" align="center"/>
            <el-table-column label="操作" width="340" align="center">
                <template  #default="scope">
                    <el-button type="success" size="small" @click="tableOperation.menuClick(scope.row)">菜单</el-button>
                    <el-button type="info" size="small" @click="tableOperation.moduleClick(scope.row)">模块</el-button>
                    <el-button type="warning" size="small" @click="tableOperation.editClick(scope.row)">编辑</el-button>
                    <confirmButton @ok="tableOperation.del" :row="scope.row">删除</confirmButton>
                </template>
            </el-table-column>
        </myTable>
        <saveForm ref="saveFormRef" @save="tableOperation.save"></saveForm>
        <menuForm ref="menuFormRef" @save="tableOperation.menu"></menuForm>
        <moduleForm ref="moduleFormRef" @save="tableOperation.module"></moduleForm>
    </div>`,
    setup(props, context) {
        const myTableRef = Vue.ref(), saveFormRef = Vue.ref(), menuFormRef = Vue.ref(), moduleFormRef = Vue.ref();
        let myTable, saveForm, menuForm, moduleForm;
        const loadData = {
            loadTable: async () => {
                myTable.loading = true
                let json = {
                    pageIndex: myTable.page.index,
                    pageSize: myTable.page.size,
                }
                const {page, list} = await api.queryTable(json)
                myTable.page.index = page.index
                myTable.page.total = page.total
                list.forEach(e => {
                    e.menu = e.menuId ? e.menuId.split(",").map(e => Number(e)) : []
                    e.module = e.moduleId ? e.moduleId.split(",").map(e => Number(e)) : []
                })
                myTable.tableData = list
                myTable.loading = false
            }
        }
        const tableOperation = {
            addClick() {
                saveForm.title = "新增角色"
                saveForm.formClear()
                saveForm.visible = true
            },
            editClick({id, name}) {
                saveForm.title = "编辑角色"
                saveForm.formClear()
                Object.assign(saveForm.formData, {id, name})
                saveForm.visible = true
            },
            async menuClick({id, menu}) {
                menuForm.formClear()
                Object.assign(menuForm.formData, {id, menu})
                menuForm.visible = true
                while (!menuForm.treeRef) await sleep(5)
                menuForm.treeRef.setCheckedKeys(menu)
            },
            async moduleClick({id, module}) {
                moduleForm.formClear()
                Object.assign(moduleForm.formData, {id, module})
                moduleForm.visible = true
                while (!moduleForm.treeRef) await sleep(5)
                moduleForm.treeRef.setCheckedKeys(module)
            },
            async save(data) {
                await api.save(data)
                saveForm.visible = false
                await loadData.loadTable()
            },
            async menu(data) {
                await api.setMenu(data)
                menuForm.visible = false
                await loadData.loadTable()
            },
            async module(data) {
                await api.setModule(data)
                moduleForm.visible = false
                await loadData.loadTable()
            },
            async del({id}) {
                await api.del(id)
                await loadData.loadTable()
            }
        }
        const init = (async () => {
            [myTable, saveForm, menuForm, moduleForm] = await getRefs([myTableRef, saveFormRef, menuFormRef, moduleFormRef])
            await loadData.loadTable()
        })()
        return {
            myTableRef,
            saveFormRef,
            menuFormRef,
            moduleFormRef,
            loadData,
            tableOperation
        }
    },
    components: {
        mySearch: (await import('/component/search/js/index.js')).default,
        myTable: (await import('/component/table/js/index.js')).default,
        confirmButton: (await import('/component/confirm_button/js/index.js')).default,
        saveForm: (await import('/page/role_manager/js/save.js')).default,
        menuForm: (await import('/page/role_manager/js/menu.js')).default,
        moduleForm: (await import('/page/role_manager/js/module.js')).default,
    }
}