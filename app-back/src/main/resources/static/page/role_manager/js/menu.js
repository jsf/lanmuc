import {menu as form} from "../config/index.js";
import api from "../config/api.js"

export default {
    template: `
         <el-dialog
            v-model="visible"
            title="设置菜单权限"
            width="30%"
          >
            <el-tree
              ref="treeRef"
              :data="treeData"
              show-checkbox
              node-key="id"
              default-expand-all
              :default-checked-keys="formData.menu"
              :props="{ label: 'title' }"
              show-checkbox
              check-strictly
            />
            <template #footer>
              <span class="dialog-footer">
                <el-button @click="visible=false">取消</el-button>
                <el-button type="primary" @click="operation.save">保存</el-button>
              </span>
            </template>
        </el-dialog>
    `,
    setup(props, context) {
        const treeRef = Vue.ref()
        const visible = Vue.ref(false)
        const treeData = Vue.ref([])
        const operation = {
            save: async () => {
                context.emit("save", {id: form.formData.id, menu: (await getRef(treeRef)).getCheckedKeys()})
            }
        }
        const init = (async () => {
            treeData.value = await api.queryMenu()
        })()
        //由于el-tree的数据无法更新，关闭后清除选中数据
        Vue.watch(visible, async (v) => v || (await getRef(treeRef)).setCheckedNodes([]))
        return Object.assign({
            treeRef,
            visible,
            treeData,
            operation
        }, form)
    }
}