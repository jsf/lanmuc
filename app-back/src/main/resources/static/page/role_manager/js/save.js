import {save as form} from "../config/index.js";

export default {
    template: `
         <el-dialog
            v-model="visible"
            :title="title"
            width="30%"
          >
            <el-form :model="formData" :rules="formRules" ref="formRef" label-width="80px">
                <el-form-item label="角色名" prop="name">
                    <el-input clearable v-model="formData.name" />
                </el-form-item>
            </el-form>
            <template #footer>
              <span class="dialog-footer">
                <el-button @click="visible=false">取消</el-button>
                <el-button type="primary" @click="operation.save">保存</el-button>
              </span>
            </template>
        </el-dialog>
    `,
    setup(props, context) {
        const formRef = Vue.ref(null);
        const visible = Vue.ref(false)
        const title = Vue.ref("")
        const operation = {
            save: async () => {
                (await getRef(formRef)).validate(async (valid) => {
                    if (valid) {
                        context.emit("save", form.formData)
                    }
                })
            }
        }
        Vue.watch(visible, async val => val || (await getRef(formRef)).resetFields())
        return Object.assign({
            visible,
            title,
            formRef,
            operation
        }, form)
    }
}