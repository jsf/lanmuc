importCSS("/page/role_manager/css/index.css");
import api from "../config/api.js";

export default {
    name: "data_manager",
    template: `
    <div id="data_manager" lanmu="data_manager">
        <mySearch @add="tableOperation.addClick" @search="loadData.loadTable"></mySearch>
        <myTable ref="myTableRef" :showPage="false" rowKey="tableName">
            <el-table-column type="expand">
              <template #default="props">
                <div style="font-weight: bolder;padding-left: 60px;display: flex;flex-direction: column;gap: 5px">
                    <div v-for="item in props.row.info">
                        <span style="padding-right: 5px">
                            {{item.name + "：" + item.des }}
                        </span>
                        <el-tag v-text="item.type"></el-tag>
                    </div>
                </div>
              </template>
            </el-table-column>
            <el-table-column prop="tableComment" sortable label="表功能" align="left" width="300"/>
            <el-table-column prop="tableName" label="表名" sortable align="left" width="300" />
            <el-table-column prop="param" label="表字段" sortable align="left" />
        </myTable>
        <saveForm ref="saveFormRef" @save="tableOperation.save"></saveForm>
    </div>`,
    setup(props, context) {
        const myTableRef = Vue.ref(), saveFormRef = Vue.ref();
        let myTable, saveForm, menuForm, moduleForm;
        const loadData = {
            loadTable: async () => {
                myTable.loading = true
                let json = {
                    pageIndex: myTable.page.index,
                    pageSize: myTable.page.size,
                }
                const list = await api.queryTable(json)
                list.forEach(e => e.info = JSON.parse(e.info))
                myTable.tableData = list
                myTable.loading = false
            }
        }
        const tableOperation = {
            addClick() {
                saveForm.title = "新增数据表"
                saveForm.formClear()
                saveForm.visible = true
            },
            async save(data) {
                await api.save(data)
                saveForm.visible = false
                await loadData.loadTable()
            },
        }
        const init = (async () => {
            [myTable, saveForm, menuForm, moduleForm] = await getRefs([myTableRef, saveFormRef])
            await loadData.loadTable()
        })()
        return {
            myTableRef,
            saveFormRef,
            loadData,
            tableOperation
        }
    },
    components: {
        mySearch: (await import('/component/search/js/index.js')).default,
        myTable: (await import('/component/table/js/index.js')).default,
        confirmButton: (await import('/component/confirm_button/js/index.js')).default,
        saveForm: (await import('/page/data_manager/js/form.js')).default,
    }
}