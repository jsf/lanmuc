export default {
    queryTable: async (json) => {
        const {body} = await axios.post("pt/table/list", {})
        return body
    },
    save: async (json) => {
        const {body} = await axios.post("pt/sql/execute", json)
        ElementPlus.ElMessage.success({message: "操作成功", duration: 2000});
        return body
    }
}