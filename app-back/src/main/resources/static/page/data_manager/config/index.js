const formSource = () => ({sql: ''})
export const form = {
    formRules: {
        sql: [
            {required: true, message: 'sql为必填', trigger: 'blur'},
        ]
    },
    formSource,
    formData: Vue.reactive(formSource()),
    formClear: () => Object.assign(form.formData, formSource())
}