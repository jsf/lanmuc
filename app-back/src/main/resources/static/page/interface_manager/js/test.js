import {test as form} from "../config/index.js";
import api from "../config/api.js";

export default {
    template: `
          <el-dialog
            v-model="visible"
            title="接口调用"
            width="80%">
                <el-form  :model="formData" :rules="formRules" ref="formRef" label-width="120px">
                    <el-form-item label="url" prop="url">
                        <el-input disabled v-model="formData.url" placeholder="请输入url" ></el-input>
                    </el-form-item>
                    <el-form-item label="token凭证:" prop="token">
                        <el-input v-model="formData.token" placeholder="请输入token"></el-input>
                    </el-form-item>
                    <el-form-item label="mock数据">
                        <el-button type="primary" @click="operation.create">生成</el-button>
                    </el-form-item>
                    <el-row justify="space-between">
                        <el-col :span="12">
                            <el-form-item label="参数数据:" prop="param">
                                <jsInput ref="inputRef"  v-if="visible" v-model:value="formData.param" style="height: 60vh"></jsInput>                            
                            </el-form-item>
                        </el-col>
                        <el-col :span="12">
                            <el-form-item label="参数格式:" prop="paramType">
                                <jsInput  v-if="visible" v-model:value="formData.paramType" style="height: 60vh"></jsInput>
                            </el-form-item>
                        </el-col>
                    </el-row>
                </el-form>
            <template #footer>
              <span class="dialog-footer">
                <el-button @click="visible=false">取消</el-button>
                <el-button type="primary" @click="operation.test">调用</el-button>
              </span>
            </template>
                <el-dialog
                    v-model="jsonVisible"
                    :title="'接口调用结果，耗时（'+resTime+'ms）'"
                    width="50%">
                        <jsInput  v-if="jsonVisible" v-model:value="res" mode="json" style="height: 60vh"></jsInput>
                </el-dialog>
        </el-dialog>
    `, setup(props, context) {
        const formRef = Vue.ref(null);
        const inputRef = Vue.ref(null);
        const visible = Vue.ref(false)
        const jsonVisible = Vue.ref(false)
        const res = Vue.ref({})
        const resTime = Vue.ref(0)
        const operation = {
            test: async () => {
                (await getRef(formRef)).validate(async (valid) => {
                    if (valid) {
                        let time = new Date().getTime()
                        const data = await api.test(form.formData.url, JSON.parse(form.formData.param))
                        res.value = JSON.stringify(data, null, 4) || {}
                        jsonVisible.value = true
                        resTime.value = new Date().getTime() - time
                        await api.testResult({id: form.formData.id, testResult: data.code.value != 500})
                        context.emit("refresh")
                    }
                })
            },
            create: async () => {
                form.formData.param = JSON.stringify(Mock.mock(parseJson(form.formData.rule)), null, 4);
                (await getRef(inputRef)).setValue(form.formData.param)
            }
        }
        Vue.watch(visible, async val => {
            if (val) {
                const {rule} = await api.getRule({id: form.formData.id})
                form.formData.rule = rule
            }
            val || (await getRef(formRef)).resetFields()
            form.formatJson('param', form.formData.param)
            form.formatJson('paramType', form.formData.paramType)
            form.formData.token = Vue.ref(localStorage.getItem('token'))
            if (!val) jsonVisible.value = false
        })
        return Object.assign({
            visible, jsonVisible, formRef, inputRef, operation, res, resTime
        }, form)
    }, components: {
        jsInput: (await import('/component/js_input/js/index.js')).default,
    }
}