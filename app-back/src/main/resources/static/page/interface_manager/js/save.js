import {save as form} from "../config/index.js";

export default {
    template: `
         <el-dialog
            v-model="visible"
            :title="title"
            width="80%"
          >
            <el-form class="saveForm" :model="formData" :rules="formRules" ref="formRef" label-width="80px">
                <el-row justify="space-between">
                    <el-col :span="6">
                        <el-form-item label="父级接口" prop="pid">
                            <el-cascader style="width: 100%;user-select: none"
                                v-model="formData.pid"  
                                :options="[{id:0,name:'根接口'},...options]" 
                                :props="{label:'name',value:'id',checkStrictly:true,emitPath:false}"}" 
                                clearable 
                            />
                        </el-form-item>
                    </el-col>
                    <el-col :span="6">
                        <el-form-item label="接口名称" prop="name">
                            <el-input clearable v-model="formData.name" />
                        </el-form-item>
                    </el-col>
                    <el-col :span="6">
                        <el-form-item label="接口url" prop="url">
                            <el-input clearable v-model="formData.url" />
                        </el-form-item>
                    </el-col>
                    <el-col :span="3">
                        <el-form-item label="是否开放" prop="open">
                            <el-switch
                                v-model="formData.open"
                                class="ml-2"
                            />
                        </el-form-item>
                    </el-col>
                    <el-col :span="3">
                        <el-form-item label="是否鉴权" prop="authentication">
                            <el-switch
                                v-model="formData.authentication"
                                class="ml-2"
                            />
                        </el-form-item>
                    </el-col>
                </el-row>
                <el-row justify="space-between">
                    <el-col :span="12">
                        <el-form-item label="参数内容\n(req)" prop="param">
                                <jsInput  v-if="visible" v-model:value="formData.param" style="height: 30vh"></jsInput>
                        </el-form-item>
                    </el-col>
                    <el-col :span="12">
                        <el-form-item label="参数校验" prop="paramValid">
                            <jsInput  v-if="visible" v-model:value="formData.paramValid" style="height: 30vh"></jsInput>
                        </el-form-item>
                    </el-col>
                </el-row>
                <el-row justify="space-between">
                    <el-col :span="12">
                        <el-form-item label="执行json\n(res)" prop="json">
                            <jsInput  v-if="visible" v-model:value="formData.json" style="height: 30vh"></jsInput>
                        </el-form-item>
                    </el-col>
                    <el-col :span="12">
                        <el-form-item label="响应内容" prop="response">
                            <jsInput  v-if="visible" v-model:value="formData.response" style="height: 30vh"></jsInput>
                        </el-form-item>
                    </el-col>
                </el-row>
            </el-form>
            <template #footer>
              <span class="dialog-footer">
                <el-button @click="visible=false">取消</el-button>
                <el-button type="primary" @click="operation.save">保存</el-button>
              </span>
            </template>
        </el-dialog>
    `,
    props: {
        options: {
            type: Array,
            default: () => []
        }
    },
    setup(props, context) {
        const formRef = Vue.ref(null);
        const visible = Vue.ref(false)
        const title = Vue.ref("")
        const operation = {
            save: async () => {
                (await getRef(formRef)).validate(async (valid) => {
                    if (valid) {
                        context.emit("save", form.formData)
                    }
                })
            }
        }
        Vue.watch(visible, async val => {
            val || (await getRef(formRef)).resetFields()
            form.formatJson('param', form.formData.param)
            form.formatJson('paramValid', form.formData.paramValid)
        })
        return Object.assign({
            visible,
            title,
            formRef,
            operation
        }, form)
    }, components: {
        jsInput: (await import('/component/js_input/js/index.js')).default,
    }
}