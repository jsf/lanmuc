import {param as form} from "../config/index.js";

export default {
    template: `
          <el-dialog
            v-model="visible"
            title="规则配置"
            width="80%">
                <el-form  :model="formData" :rules="formRules" ref="formRef" label-width="120px">
                    <el-form-item label="url">
                        <el-input disabled v-model="formData.url" placeholder="请输入url" ></el-input>
                    </el-form-item>
                    <el-row justify="space-between">
                        <el-col :span="12">
                            <el-form-item label="入参规则:" prop="req">
                                <jsInput  v-if="visible" v-model:value="formData.req" style="height: 60vh"></jsInput>
                            </el-form-item>
                        </el-col>
                        <el-col :span="12">
                            <el-form-item label="响应格式:" prop="res">
                                <jsInput  v-if="visible" v-model:value="formData.res" style="height: 60vh"></jsInput>
                            </el-form-item>
                        </el-col>
                    </el-row>
                </el-form>
        </el-dialog>
    `, setup(props, context) {
        const formRef = Vue.ref(null);
        const visible = Vue.ref(false)
        Vue.watch(visible, async val => {
            val || (await getRef(formRef)).resetFields()
            form.formatJson('req', form.formData.req)
            form.formatJson('res', form.formData.res)
        })
        return Object.assign({
            visible, formRef
        }, form)
    }, components: {
        jsInput: (await import('/component/js_input/js/index.js')).default,
    }
}