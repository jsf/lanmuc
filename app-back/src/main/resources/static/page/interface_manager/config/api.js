export default {
    queryTable: async () => {
        const {body} = await axios.post("pt/permission/all", {hasOpen: true})
        return body
    }, save: async (json) => {
        const {body} = await axios.post("pt/permission/save", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    }, del: async (id) => {
        const {body} = await axios.post(`pt/permission/del`, {id})
        ElementPlus.ElMessage.success({message: "删除成功", duration: 2000});
        return body
    }, test: async (url, json) => {
        const res = await axios.post(url, json)
        ElementPlus.ElMessage.success({message: "执行成功", duration: 2000});
        return res
    }, copyDir: async (json) => {
        const {body} = await axios.post(`pt/permission/copyDir`, json)
        ElementPlus.ElMessage.success({message: "复制成功", duration: 2000});
        return body
    }, config: async (json) => {
        const {body} = await axios.post(`pt/permission/config`, json)
        ElementPlus.ElMessage.success({message: "配置成功", duration: 2000});
        return body
    }, testResult: async (json) => {
        return await axios.post("pt/permission/saveTestResult", json)
    }, setAutoTest: async (json) => {
        return await axios.post("pt/permission/setAutoTest", json)
    }, swagger: async (url, json) => {
        return await axios.get('v3/api-docs?group=全部')
    }, saveRule: async (json) => {
        const {body} = await axios.post("pt/permission/saveRule", json)
        ElementPlus.ElMessage.success({message: "保存成功", duration: 2000});
        return body
    }, getRule: async (json) => {
        const {body} = await axios.post("pt/permissionRule/get", json)
        return body
    }, queryTableList: async () => {
        const {body} = await axios.post("pt/table/list", {})
        return body
    }, setOpen: async (json) => {
        const res = await axios.post("pt/permission/setOpen", json)
        ElementPlus.ElMessage.success({message: "设置成功", duration: 2000});
        return res
    }, setAuthentication: async (json) => {
        const res = await axios.post("pt/permission/setAuthentication", json)
        ElementPlus.ElMessage.success({message: "设置成功", duration: 2000});
        return res
    }
}