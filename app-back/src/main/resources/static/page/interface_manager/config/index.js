//save.js
const saveFormSource = () => ({
    id: null,
    pid: 0,
    name: "",
    url: "",
    param: "",
    paramValid: "",
    json: "",
    response: "",
    open: false,
    authentication: false,
})
export const save = {
    formatJson(param, val) {
        if (val === '') return
        try {
            save.formData[param] = JSON.stringify(eval(`(${val})`), null, 4)
        } catch {
            ElementPlus.ElMessage.warning({message: "json格式错误", duration: 1500});
        }
    },
    formRules: {
        pid: [{required: true, message: '父级接口为必选', trigger: 'change'}],
        name: [{required: true, message: 'param为必填', trigger: 'blur'}, {
            min: 2,
            max: 32,
            message: '长度2~32',
            trigger: 'blur'
        }],
        url: [{required: true, message: 'url为必填', trigger: 'blur'}],
        open: [{required: true, message: '是否开放为必选', trigger: 'blur'}]
    },
    formSource: saveFormSource,
    formData: Vue.reactive(saveFormSource()),
    formClear: () => Object.assign(save.formData, saveFormSource())
}
//test.js
const testFormSource = () => ({
    url: "",
    param: "",
    paramType: "",
})
export const test = {
    formatJson(param, val) {
        if (val === '') return
        try {
            test.formData[param] = JSON.stringify(eval(`(${val})`), null, 4)
        } catch {
            ElementPlus.ElMessage.warning({message: "json格式错误", duration: 1500});
        }
    },
    formRules: {
        url: [{required: true, message: 'url为必填', trigger: 'blur'}],
        token: [{required: true, message: 'token为必填', trigger: 'blur'}],
        param: [{required: true, message: '参数为必填', trigger: 'blur'}]
    },
    formSource: testFormSource,
    formData: Vue.reactive(testFormSource()),
    formClear: () => Object.assign(test.formData, testFormSource())
}
//rule.js
const ruleFormSource = () => ({
    id: null,
    url: "",
    param: "",
    paramType: "",
    rule: ""
})
export const rule = {
    formatJson(param, val) {
        if (val === '') return
        try {
            rule.formData[param] = JSON.stringify(eval(`(${val})`), null, 4)
        } catch {
            ElementPlus.ElMessage.warning({message: "json格式错误", duration: 1500});
        }
    },
    formRules: {
        rule: [{required: true, message: '规则为必填', trigger: 'blur'}]
    },
    formSource: ruleFormSource,
    formData: Vue.reactive(ruleFormSource()),
    formClear: () => Object.assign(rule.formData, ruleFormSource())
}
//param.js
const paramFormSource = () => ({
    url: "",
    req: "",
    res: ""
})
export const param = {
    formatJson(para, val) {
        if (val === '') return
        try {
            param.formData[para] = JSON.stringify(eval(`(${val})`), null, 4)
        } catch (e) {
            console.error(e)
            ElementPlus.ElMessage.warning({message: "json格式错误", duration: 1500});
        }
    },
    formRules: {},
    formSource: paramFormSource,
    formData: Vue.reactive(paramFormSource()),
    formClear: () => Object.assign(param.formData, paramFormSource())
}