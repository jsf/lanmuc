export default {
    name: 'myMenu',
    template: `{{router}}
        <template v-for="item,i in menus" :key="i">
            <el-sub-menu v-if="item.children" :index="item.id">
                <template #title>
                    <span v-text="item.title"></span>
                </template>
                <myMenu :menus="item.children"></myMenu>
            </el-sub-menu>
            <el-menu-item v-else :index="item.id" @click="link(item.path)">
                <template #title>{{item.title}}</template>
            </el-menu-item>
        </template>
    `,
    props: ['menus'],
    setup() {
        return {
            link(path) {
                router.replace({path: path})
            }
        }
    }
}