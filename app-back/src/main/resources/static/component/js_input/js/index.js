importCSS("/component/js_input/css/index.css");
export default {
    template: `<div :style="style" class="editor" :id="dom" @blur="blur"></div>`, props: {
        style: {
            type: Object, default: () => []
        }, value: {
            type: String, default: () => ''
        }, mode: {
            type: String, default: () => 'javascript'
        }
    }, setup(props, context) {
        const dom = Vue.ref('editor_' + Math.random());
        let editor = null;
        Vue.onMounted(() => {
            editor = ace.edit(dom.value, {
                fontSize: 14,
                theme: "ace/theme/textmate",
                mode: `ace/mode/javascript`,
                tabSize: 4,
                readOnly: false,
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true,
                showPrintMargin: false,
                useWrapMode: true,
            });
            editor.resize();
            editor.setValue(props.value);
            editor.clearSelection();
            editor.getSession().on('change', e => {
                context.emit('update:value', editor.getValue());
            });
            document.getElementById(dom.value).onkeydown = (e) => {
                if (e.key === 'b' && e.ctrlKey) {
                    editor.setValue(js_beautify(editor.getValue(), {indent_size: 2, space_in_empty_paren: true}));
                    e.preventDefault();
                }
            }
        });
        return {
            dom, setValue(value) {
                editor.setValue(value);
                editor.clearSelection();
            }
        }
    }
}