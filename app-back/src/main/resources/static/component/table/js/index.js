importCSS("/component/table/css/index.css");
export default {
    name: 'myTable',
    template: `
        <div id="myTable" lanmu="myTable" v-loading="loading">
            <el-table highlight-current-row ref="tableRef" :default-expand-all="false" :row-key="rowKey" height="100%" border :data="tableData">
                <slot></slot>
            </el-table>
        </div>
        <el-pagination
          v-if="showPage"
          v-model:current-page="page.index"
          v-model:page-size="page.size"
          background
          :page-sizes="[10, 20, 50, 100]"
          layout="sizes, prev, pager, next"
          :total="page.total"
          @size-change="$emit('page-change')"
          @current-change="$emit('page-change')"
        />
    `,
    props: {
        rowKey: {type: String, default: 'id'},
        showPage: {type: Boolean, default: true},
    },
    setup() {
        const tableRef = Vue.ref()
        const tableData = Vue.ref([])
        const loading = Vue.ref(false)
        const page = Vue.reactive({
            index: 1,
            size: 10,
            total: 0
        })
        return {
            loading,
            tableRef,
            tableData,
            page,
        }
    }
}