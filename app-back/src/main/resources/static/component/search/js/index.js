importCSS("/component/search/css/index.css");
export default {
    name: 'mySearch',
    template: `
        <div id="mySearch" lanmu="mySearch">
            <el-button v-if="showAdd" :icon="icon.plus" type="danger" @click="$emit('add')">新增</el-button>
            <div class="search-group">
                <slot></slot>
                <el-button type="primary" :icon="icon.refresh" @click="$emit('search')">搜索</el-button>
            </div>
        </div>
    `,
    props: {
        showAdd: {
            default: true,
            type: Boolean
        }
    },
    data: () => ({
        icon: {
            refresh: ElementPlusIconsVue.Refresh,
            plus: ElementPlusIconsVue.Plus
        }
    }),
}