importCSS("/component/confirm_button/css/index.css");
export default {
    name: 'confirmButton',
    template: `
        <el-popconfirm
            :confirm-button-text="confirmText"
            :cancel-button-text="cancelText"
            :icon="icon?icon:icons.InfoFilled"
            :title="title"
            :width="titleWidth"
            @confirm="$emit('ok',row)"
         >
            <template #reference>
                <el-button :disabled="disabled" :type="type" :size="size" @click=""><slot></slot></el-button>
            </template>
        </el-popconfirm>
    `,
    props: {
        row: {
            default: {},
            type: Object
        },
        type: {
            default: "danger",
            type: String
        },
        disabled: {
            default: false,
            type: Boolean
        },
        size: {
            type: String,
            default: "small"
        },
        title: {
            default: "确定删除吗？",
            type: String
        },
        titleWidth: {
            default: 200,
            type: Number
        },
        icon: {
            default: null,
            type: Object
        },
        confirmText: {
            default: "确定",
            type: String
        },
        cancelText: {
            default: "取消",
            type: String
        }
    },
    data: () => ({
        icons: {
            InfoFilled: ElementPlusIconsVue.InfoFilled
        }
    }),
}