package pr.lanmu.config.jackson;

import cn.hutool.json.JSONUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import pr.lanmu.config.util.Log;

import java.io.IOException;
import java.util.Objects;

public class JsonStrToObjSerializer extends JsonSerializer<Object> {
    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {
        Log.info("json to obj ==> " + value);
        if (Objects.isNull(value)) {
            gen.writeObject(null);
        } else {
            gen.writeObject(JSONUtil.parse(value));
        }
    }
}