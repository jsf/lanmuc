package pr.lanmu.config.function;

/**
 * 函数式接口 无入参无返回
 */
@FunctionalInterface
public interface MyDeal {
    void deal();
}
