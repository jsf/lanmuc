package pr.lanmu.config.function;

/**
 * 函数式接口 有入参无返回
 *
 * @param <T>
 */
@FunctionalInterface
public interface MyConsumer<T> {
    void accept(T t);
}