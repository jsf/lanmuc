package pr.lanmu.config.function;

/**
 * 函数式接口 无入参有返回
 *
 * @param <T>
 */
@FunctionalInterface
public interface MySupplier<T> {
    T get();
}