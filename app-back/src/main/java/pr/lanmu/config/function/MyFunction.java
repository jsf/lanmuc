package pr.lanmu.config.function;

/**
 * 函数式接口 有入参有返回
 *
 * @param <T>
 * @param <R>
 */
@FunctionalInterface
public interface MyFunction<T, R> {
    R apply(T t);
}