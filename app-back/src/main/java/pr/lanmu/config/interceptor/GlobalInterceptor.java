package pr.lanmu.config.interceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import pr.lanmu.config.constant.Code;
import pr.lanmu.config.util.Res;

/**
 * 全局拦截器
 */
@Configuration
@SuppressWarnings("all")
public class GlobalInterceptor implements HandlerInterceptor {

    /**
     * 记录全局拦截记录操作信息和植入主线程变量Current存放用户信息
     *
     * @param req      .
     * @param response .
     * @param handler  .
     * @return .
     */
    @SneakyThrows
    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse response, Object handler) {
        //只处理post请求
        if (!req.getMethod()
                .equals("POST")) {
            Res.json(response, Res.fail(Code.REQUEST_METHOD_ERROR));
            return false;
        }
        return true;
    }
}
