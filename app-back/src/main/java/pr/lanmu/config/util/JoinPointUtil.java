package pr.lanmu.config.util;

import org.aspectj.lang.JoinPoint;

import java.util.Arrays;
import java.util.List;

/**
 * 切面参数提取工具类
 */
public class JoinPointUtil {

    public static Object[] getInfo(JoinPoint joinPoint, boolean argsFlag) {
        String methodName = joinPoint.getSignature()
                .getDeclaringType()
                .getName() + "." + joinPoint
                .getSignature()
                .getName();
        if (argsFlag) {
            Object[] objs = joinPoint.getArgs();
            List<Object> args = Arrays.asList(objs);
            return new Object[]{methodName, args};
        } else {
            return new Object[]{methodName};
        }
    }
}
