package pr.lanmu.config.util;

import cn.hutool.crypto.digest.BCrypt;
import org.springframework.stereotype.Component;

/**
 * 加密工具类
 */
@Component
public class EncodeUtil {

    /**
     * 密码生成
     *
     * @param args .
     */
    public static void main(String[] args) {
        System.out.println(new EncodeUtil().bcryptEncode("aomijia@123"));
    }

    public String bcryptEncode(String txt) {
        return BCrypt.hashpw(txt);
    }

    public Boolean bcryptCheck(String txt, String hash) {
        return BCrypt.checkpw(txt, hash);
    }
}