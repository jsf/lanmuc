package pr.lanmu.config.util;

import lombok.extern.slf4j.Slf4j;

/**
 * 日志工具类
 */
@Slf4j
public class Log {
    public static void info(String msg) {
        log.info(Current.getRequestFlag() + msg);
    }

    public static void info(String msg, Object... obj) {
        log.info(Current.getRequestFlag() + msg, obj);
    }

    public static void info(String msg, Throwable throwable) {
        log.info(Current.getRequestFlag() + msg, throwable);
    }

    public static void warn(String msg) {
        log.info(Current.getRequestFlag() + msg);
        log.warn(Current.getRequestFlag() + msg);
    }

    public static void warn(String msg, Object... obj) {
        log.info(Current.getRequestFlag() + msg, obj);
        log.warn(Current.getRequestFlag() + msg, obj);
    }

    public static void warn(String msg, Throwable throwable) {
        log.info(Current.getRequestFlag() + msg, throwable);
        log.warn(Current.getRequestFlag() + msg, throwable);
    }

    public static void error(String msg) {
        log.info(Current.getRequestFlag() + msg);
        log.error(Current.getRequestFlag() + msg);
    }

    public static void error(String msg, Object... obj) {
        log.info(Current.getRequestFlag() + msg, obj);
        log.error(Current.getRequestFlag() + msg, obj);
    }

    public static void error(String msg, Throwable throwable) {
        log.info(Current.getRequestFlag() + msg, throwable);
        log.error(Current.getRequestFlag() + msg, throwable);
    }

}
