package pr.lanmu.config.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pr.lanmu.common.po.Cron;
import pr.lanmu.config.util.Log;
import pr.lanmu.config.util.QuartzJob;

@Component
@SuppressWarnings("all")
public class ApplicationStartupListener implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private QuartzJob quartzJob;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent ignored) {
        Log.info("加载定时任务");
        Cron.create().where(Cron::getState).eq(true).list().parallelStream().forEach(quartzJob::loadJob);
        Log.info("加载定时任务完成");
    }
}