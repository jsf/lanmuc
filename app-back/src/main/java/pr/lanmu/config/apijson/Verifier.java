package pr.lanmu.config.apijson;

import apijson.orm.AbstractVerifier;
import apijson.orm.Parser;

/**
 * 安全校验器，校验请求参数、角色与权限等
 * 具体见 <a href="https://github.com/Tencent/APIJSON/issues/12">...</a>
 *
 * @author Lemon
 */
public class Verifier extends AbstractVerifier<Long> {

    @Override
    public Parser<Long> createParser() {
        return new SqlParser();
    }


}