package pr.lanmu.config.apijson;

import apijson.RequestMethod;
import apijson.orm.AbstractObjectParser;
import apijson.orm.Join;
import apijson.orm.SQLConfig;
import com.alibaba.fastjson.JSONObject;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.constraints.NotNull;

import java.util.List;

/**
 * 对象解析器，用来简化 Parser
 *
 * @author Lemon
 */
@SuppressWarnings("all")
public class ObjectParser extends AbstractObjectParser {

    public ObjectParser(HttpSession session, @NotNull JSONObject request, String parentPath, SQLConfig arrayConfig
            , boolean isSubquery, boolean isTable, boolean isArrayMainTable) throws Exception {
        super(request, parentPath, arrayConfig, isSubquery, isTable, isArrayMainTable);
    }

    @Override
    public SQLConfig newSQLConfig(RequestMethod method, String table, String alias, JSONObject request, List<Join> joinList, boolean isProcedure) throws Exception {
        return SqlConfig.newSQLConfig(method, table, alias, request, joinList, isProcedure);
    }


}