package pr.lanmu.config.apijson;

import apijson.orm.AbstractSQLExecutor;
import lombok.extern.slf4j.Slf4j;

/**
 * SQL 执行器，支持连接池及多数据源
 * 具体见 <a href="https://github.com/Tencent/APIJSON/issues/151">...</a>
 *
 * @author Lemon
 */
@Slf4j
public class SqlExecutor extends AbstractSQLExecutor {
    static {
        try { //加载驱动程序
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            log.error("加载 MySQL 8 驱动失败，请检查 pom.xml 中 mysql-connector-java 版本是否存在以及可用 ！！！");
        }
    }

}