package pr.lanmu.config.apijson;

import apijson.RequestMethod;
import apijson.orm.AbstractSQLConfig;
import apijson.orm.Join;
import apijson.orm.SQLConfig;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;
import java.util.Map;

@SuppressWarnings("all")
public class SqlConfig extends AbstractSQLConfig {
    static {
        DEFAULT_DATABASE = DATABASE_MYSQL;
        DEFAULT_SCHEMA = DataBaseConfig.config.getDatabase();
    }

    public SqlConfig() {
        super(RequestMethod.GET);
    }

    public SqlConfig(RequestMethod method) {
        super(method);
    }

    public SqlConfig(RequestMethod method, String table) {
        super(method, table);
    }

    public static SQLConfig newSQLConfig(RequestMethod method, String table, String alias, com.alibaba.fastjson.JSONObject request, List<Join> joinList, boolean isProcedure) throws Exception {
        return AbstractSQLConfig.newSQLConfig(method, table, alias, request, joinList, isProcedure, new SimpleCallback<>() {
            @Override
            public SQLConfig getSQLConfig(RequestMethod method, String database, String schema, String datasource, String table) {
                return new SqlConfig(method, table);
            }

            @Override
            public Object newId(RequestMethod method, String database, String schema, String datasource, String table) {
                return null;
            }
        });
    }

    @Override
    public String getDBVersion() {
        return DataBaseConfig.config.getVersion();
    }

    @JSONField(serialize = false)
    @Override
    public String getDBUri() {
        return DataBaseConfig.config.getUrl();
    }

    @JSONField(serialize = false)
    @Override
    public String getDBAccount() {
        return DataBaseConfig.config.getUsername();
    }

    @JSONField(serialize = false)
    @Override
    public String getDBPassword() {
        return DataBaseConfig.config.getPassword();
    }

    @Override
    public boolean isFakeDelete() {
        return false;
    }

    @Override
    public void onFakeDelete(Map<String, Object> map) {

    }

}