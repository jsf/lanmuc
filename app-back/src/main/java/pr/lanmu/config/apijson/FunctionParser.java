package pr.lanmu.config.apijson;

import apijson.RequestMethod;
import apijson.orm.AbstractFunctionParser;
import com.alibaba.fastjson.JSONObject;


/**
 * 可远程调用的函数类，用于自定义业务逻辑处理
 * 具体见 <a href="https://github.com/Tencent/APIJSON/issues/101">...</a>
 *
 * @author Lemon
 */
public class FunctionParser extends AbstractFunctionParser {

    public FunctionParser() {
        this(null, null, 0, null);
    }

    public FunctionParser(RequestMethod method, String tag, int version, JSONObject request) {
        super(method, tag, version, request);
    }


}