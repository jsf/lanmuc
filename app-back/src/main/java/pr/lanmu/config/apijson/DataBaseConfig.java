package pr.lanmu.config.apijson;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.datasource")
public class DataBaseConfig {
    public static DataBaseConfig config;
    private String database;
    private String username;
    private String password;
    private String url;
    private String version = "8.0.23";

    @PostConstruct
    public void init() {
        config = this;
        this.database = this.url.substring(this.url.lastIndexOf("/") + 1, this.url.indexOf("?"));
    }
}
