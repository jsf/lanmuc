package pr.lanmu.config.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 基础分页信息
 */
@Data
@Schema(name = "PageBase", description = "基础分页信息")
public class PageBase {
    @Schema(description = "页码", defaultValue = "1")
    private Integer pageIndex = 1;

    @Schema(description = "一页条数", defaultValue = "10")
    private Integer PageSize = 10;

}