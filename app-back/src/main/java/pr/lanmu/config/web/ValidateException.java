package pr.lanmu.config.web;

public class ValidateException extends RuntimeException {
    public ValidateException(String message) {
        super(message);
    }
}