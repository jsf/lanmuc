package pr.lanmu.config.web;

public class CustomException extends RuntimeException {
    public CustomException(String message) {
        super(message);
    }
}
