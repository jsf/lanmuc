package pr.lanmu.config.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import pr.lanmu.config.interceptor.AuthenticationInterceptor;
import pr.lanmu.config.interceptor.GlobalInterceptor;

import java.io.File;

import static pr.lanmu.AppApplication.FILE_PATH;


/**
 * 配置类
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private GlobalInterceptor globalInterceptor;
    @Autowired
    private AuthenticationInterceptor authenticationInterceptor;

    /**
     * 设置拦截器
     *
     * @param registry .
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加全局拦截器
        registry.addInterceptor(globalInterceptor)
                .addPathPatterns("/pt/**")
                .order(0)
                .excludePathPatterns("/pt/file/download/**");
        //添加全局鉴权
        registry.addInterceptor(authenticationInterceptor)
                .addPathPatterns("/pt/**")
                .excludePathPatterns("/pt/file/download/**")
                .order(1);
    }

    /**
     * 设置静态资源
     *
     * @param registry .
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/pt/file/download/**")
                .addResourceLocations("file:" + FILE_PATH + File.separator + "file" + File.separator);
    }
}