package pr.lanmu.config.web;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 分頁信息统一格式类
 *
 * @param <T>
 */
@Data
@Builder
@Schema(name = "CustomPageInfo", description = "分页信息统一格式类")
public class CustomPageInfo<T> {
    @Schema(description = "数据")
    private List<T> list;
    @Schema(description = "分页信息")
    private PageInfo page;

    @Data
    @Builder
    @Schema(name = "PageInfo", description = "分页信息")
    public static class PageInfo {
        @Schema(description = "当前页")
        private int index;
        @Schema(description = "一页条数")
        private int size;
        @Schema(description = "总条数")
        private long total;
    }
}
