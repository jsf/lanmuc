package pr.lanmu.config.mybatis;

import cn.hutool.core.util.ReflectUtil;
import com.mybatisflex.annotation.UpdateListener;
import lombok.SneakyThrows;
import org.apache.commons.lang3.reflect.FieldUtils;
import pr.lanmu.config.util.Current;
import pr.lanmu.config.util.Log;

import java.lang.reflect.Field;
import java.util.Optional;

public class MyUpdateListener implements UpdateListener {
    private static final String UPDATE_TIME = "updateTime";
    private static final String UPDATE_USER_ID = "updateUserId";
    private static final Long SYSTEM = 0L;

    @Override
    @SneakyThrows
    public void onUpdate(Object entity) {
        try {
            Long userId = Optional.ofNullable(Current.getUserId()).orElse(SYSTEM);
            Class<?> clazz = entity.getClass();
            long now = System.currentTimeMillis();
            //更新时间
            Field updateTime = ReflectUtil.getField(clazz, UPDATE_TIME);
            FieldUtils.writeField(updateTime, entity, now, true);
            //更新用户
            Field updateUserId = ReflectUtil.getField(clazz, UPDATE_USER_ID);
            FieldUtils.writeField(updateUserId, entity, userId, true);
        } catch (Exception e) {
            Log.warn("增强插入值属性不存在");
        }
    }
}