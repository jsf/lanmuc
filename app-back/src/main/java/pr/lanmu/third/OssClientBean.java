package pr.lanmu.third;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "third.oss")
public class OssClientBean {

    public static String bucketName;
    public static String url;

    @Value("${third.oss.bucketName}")
    public void setBucketName(String bucketName) {
        OssClientBean.bucketName = bucketName;
    }

    @Value("${third.oss.url}")
    public void setUrl(String url) {
        OssClientBean.url = url;
    }

    @Bean
    @ConditionalOnProperty(name = "upload-oss", havingValue = "true", matchIfMissing = true)
    public OSS oss(@Value("${third.oss.endpoint}") String endpoint,
                   @Value("${third.oss.accessKeyId}") String accessKeyId,
                   @Value("${third.oss.accessKeySecret}") String accessKeySecret) {
        return new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    }
}
