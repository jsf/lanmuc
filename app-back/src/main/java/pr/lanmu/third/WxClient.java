package pr.lanmu.third;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import pr.lanmu.annotation.CustomLog;

import java.util.Map;

@HttpExchange(url = "/wx")
@CustomLog
public interface WxClient {
    @PostExchange(url = "/login")
    String login(@RequestBody Map<String, ?> body);
}