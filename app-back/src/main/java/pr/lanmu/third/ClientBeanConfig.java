package pr.lanmu.third;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class ClientBeanConfig {
    @Bean
    public WxClient wxClient(@Value("${third.wx}") String url) {
        return HttpServiceProxyFactory.builder().exchangeAdapter(WebClientAdapter.create(WebClient.builder().baseUrl(url).build()))
                .build().createClient(WxClient.class);
    }
}
