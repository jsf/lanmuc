package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.RoleModule;

/**
 * 角色模块关系表 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
public interface RoleModuleMapper extends BaseMapper<RoleModule> {

}
