package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.Cron;

/**
 * 定时任务表 映射层。
 *
 * @author pr
 * @since 2023-08-03
 */
public interface CronMapper extends BaseMapper<Cron> {

}
