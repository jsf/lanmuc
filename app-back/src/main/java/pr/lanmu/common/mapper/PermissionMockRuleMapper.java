package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.PermissionMockRule;

/**
 * 接口mock数据规则表 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
public interface PermissionMockRuleMapper extends BaseMapper<PermissionMockRule> {

}
