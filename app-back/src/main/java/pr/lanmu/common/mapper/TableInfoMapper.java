package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.TableInfo;

/**
 * 定时任务表 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
@SuppressWarnings("all")
public interface TableInfoMapper extends BaseMapper<TableInfo> {

}
