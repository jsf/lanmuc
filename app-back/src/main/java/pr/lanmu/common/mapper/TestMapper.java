package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.Test;

/**
 * 测试表 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
@SuppressWarnings("all")
public interface TestMapper extends BaseMapper<Test> {

}
