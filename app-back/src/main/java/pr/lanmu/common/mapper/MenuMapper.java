package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.Menu;

/**
 * 菜单表 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
