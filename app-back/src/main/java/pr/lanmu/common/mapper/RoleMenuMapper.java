package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.RoleMenu;

/**
 * 角色菜单关系表 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
