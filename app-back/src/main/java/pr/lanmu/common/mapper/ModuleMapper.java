package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.Module;

/**
 * 模块管理 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
public interface ModuleMapper extends BaseMapper<Module> {

}
