package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.Permission;

/**
 * 权限表 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}
