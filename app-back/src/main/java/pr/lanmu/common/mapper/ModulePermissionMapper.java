package pr.lanmu.common.mapper;

import com.mybatisflex.core.BaseMapper;
import pr.lanmu.common.po.ModulePermission;

/**
 * 模块权限关系表 映射层。
 *
 * @author pr
 * @since 2023-08-01
 */
public interface ModulePermissionMapper extends BaseMapper<ModulePermission> {

}
