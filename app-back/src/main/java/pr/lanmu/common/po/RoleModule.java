package pr.lanmu.common.po;

import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 角色模块关系表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "角色模块关系表")
@Table(value = "role_module", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class RoleModule extends Model<RoleModule> {

    /**
     * 角色id
     */
    @Schema(description = "角色id")
    private Long roleId;

    /**
     * 模块id
     */
    @Schema(description = "模块id")
    private Long moduleId;

    public static RoleModule create() {
        return new RoleModule();
    }

    public Long getRoleId() {
        return roleId;
    }

    public RoleModule setRoleId(Long roleId) {
        this.roleId = roleId;
        return this;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public RoleModule setModuleId(Long moduleId) {
        this.moduleId = moduleId;
        return this;
    }

}
