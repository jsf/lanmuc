package pr.lanmu.common.po;

import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 角色菜单关系表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "角色菜单关系表")
@Table(value = "role_menu", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class RoleMenu extends Model<RoleMenu> {

    /**
     * 角色id
     */
    @Schema(description = "角色id")
    private Long roleId;

    /**
     * 菜单id
     */
    @Schema(description = "菜单id")
    private Long menuId;

    public static RoleMenu create() {
        return new RoleMenu();
    }

    public Long getRoleId() {
        return roleId;
    }

    public RoleMenu setRoleId(Long roleId) {
        this.roleId = roleId;
        return this;
    }

    public Long getMenuId() {
        return menuId;
    }

    public RoleMenu setMenuId(Long menuId) {
        this.menuId = menuId;
        return this;
    }

}
