package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 接口mock数据规则表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "接口mock数据规则表")
@Table(value = "permission_mock_rule", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class PermissionMockRule extends Model<PermissionMockRule> {

    /**
     * id
     */
    @Id(keyType = KeyType.Auto)
    @Schema(description = "id")
    private Long id;

    /**
     * mock规则
     */
    @Schema(description = "mock规则")
    private String rule;

    /**
     * 状态：无用
     */
    @Schema(description = "状态：无用")
    private Boolean status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Long createTime;

    /**
     * 创建人id
     */
    @Schema(description = "创建人id")
    private Long createUserId;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private Long updateTime;

    /**
     * 更新人id
     */
    @Schema(description = "更新人id")
    private Long updateUserId;

    public static PermissionMockRule create() {
        return new PermissionMockRule();
    }

    public Long getId() {
        return id;
    }

    public PermissionMockRule setId(Long id) {
        this.id = id;
        return this;
    }

    public String getRule() {
        return rule;
    }

    public PermissionMockRule setRule(String rule) {
        this.rule = rule;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public PermissionMockRule setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public PermissionMockRule setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public PermissionMockRule setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public PermissionMockRule setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public PermissionMockRule setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
        return this;
    }

}
