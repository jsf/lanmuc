package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 角色表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "角色表")
@Table(value = "role", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class Role extends Model<Role> {

    /**
     * id
     */
    @Id(keyType = KeyType.Auto)
    @Schema(description = "id")
    private Long id;

    /**
     * 角色名
     */
    @Schema(description = "角色名")
    private String name;

    /**
     * 状态：无用
     */
    @Schema(description = "状态：无用")
    private Boolean status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Long createTime;

    /**
     * 创建人id
     */
    @Schema(description = "创建人id")
    private Long createUserId;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private Long updateTime;

    /**
     * 更新人id
     */
    @Schema(description = "更新人id")
    private Long updateUserId;

    public static Role create() {
        return new Role();
    }

    public Long getId() {
        return id;
    }

    public Role setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public Role setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public Role setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public Role setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public Role setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public Role setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
        return this;
    }

}
