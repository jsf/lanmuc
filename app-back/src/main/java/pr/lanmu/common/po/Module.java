package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 模块管理 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "模块管理")
@Table(value = "module", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class Module extends Model<Module> {

    /**
     * id
     */
    @Id(keyType = KeyType.Auto)
    @Schema(description = "id")
    private Long id;

    /**
     * 模块名
     */
    @Schema(description = "模块名")
    private String name;

    /**
     * 父id
     */
    @Schema(description = "父id")
    private Long pid;

    /**
     * 模块标志
     */
    @Schema(description = "模块标志")
    private String tag;

    /**
     * url路径
     */
    @Schema(description = "url路径")
    private String path;

    /**
     * 组件路径
     */
    @Schema(description = "组件路径")
    private String component;

    /**
     * 拓展信息
     */
    @Schema(description = "拓展信息")
    private String meta;

    /**
     * 排序号
     */
    @Schema(description = "排序号")
    private Integer sort;

    /**
     * 状态：无用
     */
    @Schema(description = "状态：无用")
    private Boolean status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Long createTime;

    /**
     * 创建人id
     */
    @Schema(description = "创建人id")
    private Long createUserId;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private Long updateTime;

    /**
     * 更新人id
     */
    @Schema(description = "更新人id")
    private Long updateUserId;

    public static Module create() {
        return new Module();
    }

    public Long getId() {
        return id;
    }

    public Module setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Module setName(String name) {
        this.name = name;
        return this;
    }

    public Long getPid() {
        return pid;
    }

    public Module setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public Module setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public String getPath() {
        return path;
    }

    public Module setPath(String path) {
        this.path = path;
        return this;
    }

    public String getComponent() {
        return component;
    }

    public Module setComponent(String component) {
        this.component = component;
        return this;
    }

    public String getMeta() {
        return meta;
    }

    public Module setMeta(String meta) {
        this.meta = meta;
        return this;
    }

    public Integer getSort() {
        return sort;
    }

    public Module setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public Module setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public Module setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public Module setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public Module setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public Module setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
        return this;
    }

}
