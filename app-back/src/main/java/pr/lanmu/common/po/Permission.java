package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 权限表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "权限表")
@Table(value = "permission", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class Permission extends Model<Permission> {

    /**
     * 主键id
     */
    @Id(keyType = KeyType.Auto)
    @Schema(description = "主键id")
    private Long id;

    /**
     * 接口路径
     */
    @Schema(description = "接口路径")
    private String url;

    /**
     * 接口名字/组名字
     */
    @Schema(description = "接口名字/组名字")
    private String name;

    /**
     * 父id
     */
    @Schema(description = "父id")
    private Long pid;

    /**
     * 是否低代码
     */
    @Schema(description = "是否低代码")
    private Boolean isLowCode;

    /**
     * 接口类名
     */
    @Schema(description = "接口类名")
    private String controller;

    /**
     * 接口方法名
     */
    @Schema(description = "接口方法名")
    private String method;

    /**
     * 是否开放
     */
    @Schema(description = "是否开放")
    private Boolean open;

    /**
     * 是否鉴权
     */
    @Schema(description = "是否鉴权")
    private Boolean authentication;

    /**
     * 是否执行自动化mock测试
     */
    @Schema(description = "是否执行自动化mock测试")
    private Boolean autoTest;

    /**
     * 测试结果
     */
    @Schema(description = "测试结果")
    private Boolean testResult;

    /**
     * 测试时间
     */
    @Schema(description = "测试时间")
    private Long mockTime;

    /**
     * 状态：无用
     */
    @Schema(description = "状态：无用")
    private Boolean status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Long createTime;

    /**
     * 创建人id
     */
    @Schema(description = "创建人id")
    private Long createUserId;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private Long updateTime;

    /**
     * 更新人id
     */
    @Schema(description = "更新人id")
    private Long updateUserId;

    public static Permission create() {
        return new Permission();
    }

    public Long getId() {
        return id;
    }

    public Permission setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Permission setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getName() {
        return name;
    }

    public Permission setName(String name) {
        this.name = name;
        return this;
    }

    public Long getPid() {
        return pid;
    }

    public Permission setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    public Boolean getIsLowCode() {
        return isLowCode;
    }

    public Permission setIsLowCode(Boolean isLowCode) {
        this.isLowCode = isLowCode;
        return this;
    }

    public String getController() {
        return controller;
    }

    public Permission setController(String controller) {
        this.controller = controller;
        return this;
    }

    public String getMethod() {
        return method;
    }

    public Permission setMethod(String method) {
        this.method = method;
        return this;
    }

    public Boolean getOpen() {
        return open;
    }

    public Permission setOpen(Boolean open) {
        this.open = open;
        return this;
    }

    public Boolean getAuthentication() {
        return authentication;
    }

    public Permission setAuthentication(Boolean authentication) {
        this.authentication = authentication;
        return this;
    }

    public Boolean getAutoTest() {
        return autoTest;
    }

    public Permission setAutoTest(Boolean autoTest) {
        this.autoTest = autoTest;
        return this;
    }

    public Boolean getTestResult() {
        return testResult;
    }

    public Permission setTestResult(Boolean testResult) {
        this.testResult = testResult;
        return this;
    }

    public Long getMockTime() {
        return mockTime;
    }

    public Permission setMockTime(Long mockTime) {
        this.mockTime = mockTime;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public Permission setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public Permission setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public Permission setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public Permission setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public Permission setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
        return this;
    }

}
