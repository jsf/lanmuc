package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 菜单表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "菜单表")
@Table(value = "menu", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class Menu extends Model<Menu> {

    /**
     * id
     */
    @Id(keyType = KeyType.Auto)
    @Schema(description = "id")
    private Long id;

    /**
     * 父id
     */
    @Schema(description = "父id")
    private Long pid;

    /**
     * 路由地址
     */
    @Schema(description = "路由地址")
    private String title;

    /**
     * 菜单
     */
    @Schema(description = "菜单")
    private String name;

    /**
     * 组件路径
     */
    @Schema(description = "组件路径")
    private String path;

    /**
     * 组件路径
     */
    @Schema(description = "组件路径")
    private String component;

    /**
     * 排序号
     */
    @Schema(description = "排序号")
    private Integer sort;

    /**
     * 状态：无用
     */
    @Schema(description = "状态：无用")
    private Boolean status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Long createTime;

    /**
     * 创建人id
     */
    @Schema(description = "创建人id")
    private Long createUserId;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private Long updateTime;

    /**
     * 更新人id
     */
    @Schema(description = "更新人id")
    private Long updateUserId;

    public static Menu create() {
        return new Menu();
    }

    public Long getId() {
        return id;
    }

    public Menu setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getPid() {
        return pid;
    }

    public Menu setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Menu setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getName() {
        return name;
    }

    public Menu setName(String name) {
        this.name = name;
        return this;
    }

    public String getPath() {
        return path;
    }

    public Menu setPath(String path) {
        this.path = path;
        return this;
    }

    public String getComponent() {
        return component;
    }

    public Menu setComponent(String component) {
        this.component = component;
        return this;
    }

    public Integer getSort() {
        return sort;
    }

    public Menu setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public Menu setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public Menu setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public Menu setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public Menu setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public Menu setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
        return this;
    }

}
