package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 测试表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "测试表")
@Table(value = "test", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class Test extends Model<Test> {

    /**
     * 主键id
     */
    @Id(keyType = KeyType.Auto)
    @Schema(description = "主键id")
    private Long id;

    /**
     * 内容
     */
    @Schema(description = "内容")
    private String content;

    /**
     * 状态：无用
     */
    @Schema(description = "状态：无用")
    private Boolean status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Long createTime;

    /**
     * 创建人id
     */
    @Schema(description = "创建人id")
    private Long createUserId;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private Long updateTime;

    /**
     * 更新人id
     */
    @Schema(description = "更新人id")
    private Long updateUserId;

    public static Test create() {
        return new Test();
    }

    public Long getId() {
        return id;
    }

    public Test setId(Long id) {
        this.id = id;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Test setContent(String content) {
        this.content = content;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public Test setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public Test setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public Test setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public Test setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public Test setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
        return this;
    }

}
