package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 低代码接口信息表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "低代码接口信息表")
@Table(value = "low_code_permission", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class LowCodePermission extends Model<LowCodePermission> {

    /**
     * 接口id
     */
    @Id
    @Schema(description = "接口id")
    private Long permissionId;

    /**
     * 参数格式内容
     */
    @Schema(description = "参数格式内容")
    private String param;

    /**
     * 参数检验规则
     */
    @Schema(description = "参数检验规则")
    private String paramValid;

    /**
     * 执行的json
     */
    @Schema(description = "执行的json")
    private String json;

    /**
     * 响应格式内容
     */
    @Schema(description = "响应格式内容")
    private String response;

    /**
     * 状态：优化用
     */
    @Schema(description = "状态：优化用")
    private Boolean status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Long createTime;

    /**
     * 创建人id
     */
    @Schema(description = "创建人id")
    private Long createUserId;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private Long updateTime;

    /**
     * 更新人id
     */
    @Schema(description = "更新人id")
    private Long updateUserId;

    public static LowCodePermission create() {
        return new LowCodePermission();
    }

    public Long getPermissionId() {
        return permissionId;
    }

    public LowCodePermission setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
        return this;
    }

    public String getParam() {
        return param;
    }

    public LowCodePermission setParam(String param) {
        this.param = param;
        return this;
    }

    public String getParamValid() {
        return paramValid;
    }

    public LowCodePermission setParamValid(String paramValid) {
        this.paramValid = paramValid;
        return this;
    }

    public String getJson() {
        return json;
    }

    public LowCodePermission setJson(String json) {
        this.json = json;
        return this;
    }

    public String getResponse() {
        return response;
    }

    public LowCodePermission setResponse(String response) {
        this.response = response;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public LowCodePermission setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public LowCodePermission setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public LowCodePermission setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public LowCodePermission setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public LowCodePermission setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
        return this;
    }

}
