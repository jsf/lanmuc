package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 用户表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "用户表")
@Table(value = "user", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class User extends Model<User> {

    /**
     * id
     */
    @Id(keyType = KeyType.Auto)
    @Schema(description = "id")
    private Long id;

    /**
     * 名字
     */
    @Schema(description = "名字")
    private String name;

    /**
     * 账号
     */
    @Schema(description = "账号")
    private String account;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 角色：逗号分隔
     */
    @Schema(description = "角色：逗号分隔")
    private String roleId;

    /**
     * 开放平台绑定小程序和公众后每个用户特有的id
     */
    @Schema(description = "开放平台绑定小程序和公众后每个用户特有的id")
    private String wxUnionId;

    /**
     * 公众号用户的openId
     */
    @Schema(description = "公众号用户的openId")
    private String wxGzhOpenId;

    /**
     * 小程序用户的openId
     */
    @Schema(description = "小程序用户的openId")
    private String wxXcxOpenId;

    /**
     * 登录域：PC/WX/H5，多个域逗号分隔
     */
    @Schema(description = "登录域：PC/WX/H5，多个域逗号分隔")
    private String domain;

    /**
     * 超管
     */
    @Schema(description = "超管")
    private Boolean admin;

    /**
     * 状态：无用
     */
    @Schema(description = "状态：无用")
    private Boolean status;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Long createTime;

    /**
     * 创建人id
     */
    @Schema(description = "创建人id")
    private Long createUserId;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    private Long updateTime;

    /**
     * 更新人id
     */
    @Schema(description = "更新人id")
    private Long updateUserId;

    public static User create() {
        return new User();
    }

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getAccount() {
        return account;
    }

    public User setAccount(String account) {
        this.account = account;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getRoleId() {
        return roleId;
    }

    public User setRoleId(String roleId) {
        this.roleId = roleId;
        return this;
    }

    public String getWxUnionId() {
        return wxUnionId;
    }

    public User setWxUnionId(String wxUnionId) {
        this.wxUnionId = wxUnionId;
        return this;
    }

    public String getWxGzhOpenId() {
        return wxGzhOpenId;
    }

    public User setWxGzhOpenId(String wxGzhOpenId) {
        this.wxGzhOpenId = wxGzhOpenId;
        return this;
    }

    public String getWxXcxOpenId() {
        return wxXcxOpenId;
    }

    public User setWxXcxOpenId(String wxXcxOpenId) {
        this.wxXcxOpenId = wxXcxOpenId;
        return this;
    }

    public String getDomain() {
        return domain;
    }

    public User setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public User setAdmin(Boolean admin) {
        this.admin = admin;
        return this;
    }

    public Boolean getStatus() {
        return status;
    }

    public User setStatus(Boolean status) {
        this.status = status;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public User setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public User setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public User setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public User setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
        return this;
    }

}
