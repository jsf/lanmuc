package pr.lanmu.common.po;

import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;


/**
 * 表信息视图
 *
 * @author pr
 * @since 2023-08-01
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "定时任务表")
@Table(value = "table_info", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class TableInfo extends Model<TableInfo> {
    @Schema(description = "表名")
    private String tableName;
    @Schema(description = "表说明")
    private String tableComment;
    @Schema(description = "表字段逗号拼接")
    private String param;
    @Schema(description = "表字段信息")
    private String info;
    @Schema(description = "表字段对应描述")
    private String paramInfo;
    @Schema(description = "表字段校验")
    private String paramValid;
    @Schema(description = "表字段规则")
    private String paramMock;
}
