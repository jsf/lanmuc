package pr.lanmu.common.po;

import com.mybatisflex.annotation.Table;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

/**
 * 模块权限关系表 实体类。
 *
 * @author pr
 * @since 2023-08-01
 */
@Schema(name = "模块权限关系表")
@Table(value = "module_permission", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class ModulePermission extends Model<ModulePermission> {

    /**
     * 模块id
     */
    @Schema(description = "模块id")
    private Long moduleId;

    /**
     * 权限id
     */
    @Schema(description = "权限id")
    private Long permissionId;

    public static ModulePermission create() {
        return new ModulePermission();
    }

    public Long getModuleId() {
        return moduleId;
    }

    public ModulePermission setModuleId(Long moduleId) {
        this.moduleId = moduleId;
        return this;
    }

    public Long getPermissionId() {
        return permissionId;
    }

    public ModulePermission setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
        return this;
    }

}
