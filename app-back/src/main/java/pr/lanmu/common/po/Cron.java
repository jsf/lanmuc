package pr.lanmu.common.po;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;
import com.mybatisflex.core.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 定时任务表 实体类。
 *
 * @author pr
 * @since 2024-01-24
 */
@Schema(description = "定时任务表")
@Table(value = "cron", onInsert = MyInsertListener.class, onUpdate = MyUpdateListener.class)
public class Cron extends Model<Cron> {

    /**
     * 主键
     */
    @Id(keyType = KeyType.Auto)
    @Schema(description = "主键")
    private Long id;

    /**
     * 任务名称
     */
    @Schema(description = "任务名称")
    private String name;

    /**
     * 任务标识
     */
    @Schema(description = "任务标识")
    private String tag;

    /**
     * 定时规则
     */
    @Schema(description = "定时规则")
    private String code;

    /**
     * 调用路径
     */
    @Schema(description = "调用路径")
    private String url;

    /**
     * 调用凭证
     */
    @Schema(description = "调用凭证")
    private String token;

    /**
     * 入参
     */
    @Schema(description = "入参")
    private String json;

    /**
     * 执行版本
     */
    @Schema(description = "执行版本")
    private Long version;

    /**
     * 任务状态：是否启用
     */
    @Schema(description = "任务状态：是否启用")
    private Boolean state;

    public static Cron create() {
        return new Cron();
    }

    public Long getId() {
        return id;
    }

    public Cron setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Cron setName(String name) {
        this.name = name;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public Cron setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Cron setCode(String code) {
        this.code = code;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Cron setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getToken() {
        return token;
    }

    public Cron setToken(String token) {
        this.token = token;
        return this;
    }

    public String getJson() {
        return json;
    }

    public Cron setJson(String json) {
        this.json = json;
        return this;
    }

    public Long getVersion() {
        return version;
    }

    public Cron setVersion(Long version) {
        this.version = version;
        return this;
    }

    public Boolean getState() {
        return state;
    }

    public Cron setState(Boolean state) {
        this.state = state;
        return this;
    }

}
