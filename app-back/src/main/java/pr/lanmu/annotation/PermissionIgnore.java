package pr.lanmu.annotation;

import java.lang.annotation.*;

/**
 * 自定义接口权限不入库注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface PermissionIgnore {
}
