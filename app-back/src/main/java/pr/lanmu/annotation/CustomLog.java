package pr.lanmu.annotation;

import java.lang.annotation.*;

/**
 * 自定义日志注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface CustomLog {
}