package pr.lanmu.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import pr.lanmu.config.util.Current;
import pr.lanmu.config.util.Res;
import pr.lanmu.service.FileService;

@Tag(name = "文件相关接口")
@RestController
@RequestMapping("pt/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @Value("${upload-oss}")
    private Boolean uploadOss;

    /**
     * 文件上传接口
     *
     * @param request 文件
     * @return 文件路径
     */
    @Operation(summary = "文件上传")
    @Parameter(ref = "file", name = "文件")
    @PostMapping("upload")
    public Res<?> upload(@Parameter(hidden = true) MultipartHttpServletRequest request) {
        return uploadOss ? fileService.ossUpload(request, Current.getUserId()) : fileService.localUpload(request, Current.getUserId());
    }
}
