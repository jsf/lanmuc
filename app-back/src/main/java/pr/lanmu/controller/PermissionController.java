package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.config.util.Res;
import pr.lanmu.controller.request.PermissionReq;
import pr.lanmu.controller.response.PermissionRes;
import pr.lanmu.service.PermissionService;

import java.util.List;


@RestController
@Tag(name = "接口管理")
@RequestMapping("pt/permission")
public class PermissionController {
    @Autowired
    private PermissionService service;


    @Operation(summary = "获取所有接口树")
    @PostMapping("all")
    public Res<List<PermissionRes.all>> all(@RequestBody @Validated PermissionReq.all param) {
        return service.all(param);
    }

    @Operation(summary = "获取所有接口")
    @PostMapping("list")
    public Res<List<PermissionRes.list>> list(@RequestBody @Validated PermissionReq.list param) {
        return service.list(param);
    }

    @Operation(summary = "保存低代码接口")
    @PostMapping("save")
    public Res<?> save(@RequestBody @Validated PermissionReq.save param) {
        return service.save(param);
    }

    @Operation(summary = "保存mock规则")
    @PostMapping("saveRule")
    public Res<?> saveRule(@RequestBody @Validated PermissionReq.saveRule param) {
        return service.saveRule(param);
    }

    @Operation(summary = "删除低代码接口")
    @PostMapping("del")
    public Res<?> del(@RequestBody @Validated PermissionReq.del param) {
        return service.del(param);
    }

    @Operation(summary = "创建低代码接口并绑定模块")
    @PostMapping("saveAndBingModule")
    public Res<?> saveAndBingModule(@RequestBody @Validated PermissionReq.saveAndBingModule param) {
        return service.saveAndBingModule(param);
    }

    @Operation(summary = "保存接口测试结果")
    @PostMapping("saveTestResult")
    public Res<?> saveTestResult(@RequestBody @Validated PermissionReq.saveTestResult param) {
        return service.saveTestResult(param);
    }

    @Operation(summary = "设置是否开启自动化测试")
    @PostMapping("setAutoTest")
    public Res<?> setAutoTest(@RequestBody @Validated PermissionReq.setAutoTest param) {
        return service.setAutoTest(param);
    }

    @Operation(summary = "设置是否开放")
    @PostMapping("setOpen")
    public Res<?> setOpen(@RequestBody @Validated PermissionReq.setOpen param) {
        return service.setOpen(param);
    }

    @Operation(summary = "设置是否鉴权")
    @PostMapping("setAuthentication")
    public Res<?> setAuthentication(@RequestBody @Validated PermissionReq.setAuthentication param) {
        return service.setAuthentication(param);
    }

    @Operation(summary = "接口目录复制")
    @PostMapping("copyDir")
    public Res<?> copyDir(@RequestBody @Validated PermissionReq.copyDir param) {
        return service.copyDir(param);
    }

    @Operation(summary = "接口配置信息一键使用默认值")
    @PostMapping("config")
    public Res<?> config(@RequestBody @Validated PermissionReq.config param) {
        return service.config(param);
    }
}