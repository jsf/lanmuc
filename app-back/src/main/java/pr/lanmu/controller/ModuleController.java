package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.common.po.Module;
import pr.lanmu.config.util.Res;
import pr.lanmu.controller.request.ModuleReq;
import pr.lanmu.controller.response.ModuleRes;
import pr.lanmu.service.ModuleService;

import java.util.List;


@RestController
@Tag(name = "模块管理")
@RequestMapping("pt/module")
public class ModuleController {
    @Autowired
    private ModuleService service;

    @Operation(summary = "获取全部平台")
    @PostMapping("roots")
    public Res<List<Module>> roots(@RequestBody @Validated ModuleReq.roots param) {
        return service.roots(param);
    }

    @Operation(summary = "获取指定平台模块树")
    @PostMapping("root")
    public Res<List<ModuleRes.modules>> root(@RequestBody @Validated ModuleReq.root param) {
        return service.root(param);
    }

    @Operation(summary = "获取所有模块树")
    @PostMapping("all")
    public Res<List<ModuleRes.modules>> all(@RequestBody @Validated ModuleReq.all param) {
        return service.all(param);
    }

    @Operation(summary = "保存模块")
    @PostMapping("save")
    public Res<?> save(@RequestBody @Validated ModuleReq.save param) {
        return service.save(param);
    }

    @Operation(summary = "绑定接口权限")
    @PostMapping("permission")
    public Res<?> permission(@RequestBody @Validated ModuleReq.permission param) {
        return service.permission(param);
    }

    @Operation(summary = "删除模块")
    @PostMapping("del")
    public Res<?> del(@RequestBody @Validated ModuleReq.del param) {
        return service.del(param);
    }


    @Operation(summary = "查询模块绑定的表和关联的接口及接口的校验规则")
    @PostMapping("getModuleTableInfo")
    public Res<ModuleRes.getModuleTableInfo> getModuleTableInfo(@RequestBody @Validated ModuleReq.getModuleTableInfo param) {
        return service.getModuleTableInfo(param);
    }

}
