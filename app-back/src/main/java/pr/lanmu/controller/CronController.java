package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.config.util.Res;
import pr.lanmu.config.web.CustomPageInfo;
import pr.lanmu.controller.request.CronReq;
import pr.lanmu.controller.response.CronRes;
import pr.lanmu.service.CronService;


@RestController
@Tag(name = "定时任务管理")
@RequestMapping("pt/cron")
public class CronController {
    @Autowired
    private CronService service;

    @Operation(summary = "分页查询定时任务")
    @PostMapping("/list")
    public Res<CustomPageInfo<CronRes.list>> list(@RequestBody @Validated CronReq.list param) {
        return service.list(param);
    }


    @Operation(summary = "新增定时任务")
    @PostMapping("/post")
    public Res<?> post(@RequestBody @Validated CronReq.post param) {
        return service.post(param);
    }

    @Operation(summary = "删除定时任务")
    @PostMapping("/delete")
    public Res<?> delete(@RequestBody @Validated CronReq.delete param) {
        return service.delete(param);
    }

    @Operation(summary = "修改定时任务")
    @PostMapping("/put")
    public Res<?> put(@RequestBody @Validated CronReq.put param) {
        return service.put(param);
    }
    @Operation(summary = "已装载定时任务")
    @PostMapping("/loads")
    public Res<?> loads(@RequestBody @Validated CronReq.loads param) {
        return service.loads(param);
    }
}
