package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.config.util.Res;
import pr.lanmu.controller.request.PermissionRuleReq;
import pr.lanmu.controller.response.PermissionRuleRes;
import pr.lanmu.service.PermissionRuleService;


@RestController
@Tag(name = "权限规则管理")
@RequestMapping("pt/permissionRule")
public class PermissionRuleController {
    @Autowired
    private PermissionRuleService service;

    @Operation(summary = "获取权限规则")
    @PostMapping("get")
    public Res<PermissionRuleRes.get> get(@RequestBody @Validated PermissionRuleReq.get param) {
        return service.get(param);
    }
}