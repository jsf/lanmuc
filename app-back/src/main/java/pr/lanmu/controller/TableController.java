package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.common.po.TableInfo;
import pr.lanmu.config.util.Res;
import pr.lanmu.controller.request.TableReq;
import pr.lanmu.service.TableService;

import java.util.List;


@RestController
@Tag(name = "数据表管理")
@RequestMapping("pt/table")
public class TableController {
    @Autowired
    private TableService service;

    @PostMapping("list")
    @Operation(summary = "查询数据表信息")
    public Res<List<TableInfo>> list(@Validated @RequestBody TableReq.list param) {
        return service.list();
    }
}