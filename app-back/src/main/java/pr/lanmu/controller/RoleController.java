package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.common.po.Role;
import pr.lanmu.config.util.Res;
import pr.lanmu.config.web.CustomPageInfo;
import pr.lanmu.controller.request.RoleReq;
import pr.lanmu.controller.response.RoleRes;
import pr.lanmu.service.RoleService;

import java.util.List;


@RestController
@Tag(name = "角色管理")
@RequestMapping("pt/role")
public class RoleController {
    @Autowired
    private RoleService service;

    @Operation(summary = "分页查询角色")
    @PostMapping("list")
    public Res<CustomPageInfo<RoleRes.list>> list(@RequestBody @Validated RoleReq.list param) {
        return service.list(param);
    }

    @Operation(summary = "保存角色")
    @PostMapping("save")
    public Res<?> save(@RequestBody @Validated RoleReq.save param) {
        return service.save(param);
    }

    @Operation(summary = "设置角色菜单")
    @PostMapping("setMenu")
    public Res<?> setMenu(@RequestBody @Validated RoleReq.setMenu param) {
        return service.setMenu(param);
    }

    @Operation(summary = "设置模块权限")
    @PostMapping("setModule")
    public Res<?> setModule(@RequestBody @Validated RoleReq.setModule param) {
        return service.setModule(param);
    }

    @Operation(summary = "删除角色")
    @PostMapping("del")
    public Res<?> del(@RequestBody @Validated RoleReq.del param) {
        return service.del(param);
    }

    @Operation(summary = "全部角色")
    @PostMapping("all")
    public Res<List<Role>> all(@RequestBody @Validated RoleReq.all param) {
        return service.all(param);
    }
}