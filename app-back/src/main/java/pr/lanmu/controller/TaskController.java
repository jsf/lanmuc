package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.config.util.Res;
import pr.lanmu.service.TaskService;

@RestController
@Tag(name = "任务调度管理")
@RequestMapping("pt/task")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @Operation(summary = "定期清除临时过期文件夹")
    @PostMapping("deleteTempFile")
    public Res<?> deleteTempFile() {
        return taskService.deleteTempFile();
    }

    @Operation(summary = "自动化测试")
    @PostMapping("autoCodeTest")
    public Res<?> autoCodeTest() {
        return taskService.autoCodeTest();
    }
}
