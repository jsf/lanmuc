package pr.lanmu.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.config.util.Res;
import pr.lanmu.controller.request.SQLReq;
import pr.lanmu.service.SQLService;

@Tag(name = "SQL管理")
@RestController
@RequestMapping("pt/sql")
public class SQLController {

    @Autowired
    private SQLService service;

    /**
     * 执行sql
     *
     * @return .
     */
    @Operation(summary = "执行sql")
    @PostMapping("/execute")
    public Res<?> execute(@RequestBody @Validated SQLReq.execute param) {
        return service.execute(param);
    }
}
