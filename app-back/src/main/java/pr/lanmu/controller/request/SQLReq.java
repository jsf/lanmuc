package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


public class SQLReq {
    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "SQLReq-execute")
    public static class execute {
        @NotBlank(message = "sql不能为空")
        @Schema(description = "sql")
        private String sql;
    }
}
