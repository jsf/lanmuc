package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import pr.lanmu.config.param.PageBase;

import java.util.List;

public class RoleReq {
    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "RoleReq-add")
    public static class save {
        @Schema(description = "角色id")
        private Long id;
        @NotBlank
        @Schema(description = "角色名")
        private String name;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "RoleReq-list")
    public static class list extends PageBase {
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "RoleReq-setMenu")
    public static class setMenu {
        @NotNull
        @Schema(description = "角色id")
        private Long id;
        @NotNull
        @Schema(description = "菜单id集合")
        private List<Long> menu;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "RoleReq-setModule")
    public static class setModule {
        @NotNull
        @Schema(description = "角色id")
        private Long id;
        @NotNull
        @Schema(description = "权限id集合")
        private List<Long> module;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "RoleReq-all")
    public static class all {
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "RoleReq-del")
    public static class del {
        @NotNull
        @Schema(description = "角色id")
        private Long id;
    }
}
