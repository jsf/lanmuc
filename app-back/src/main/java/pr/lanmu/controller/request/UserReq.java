package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pr.lanmu.config.constant.Domain;
import pr.lanmu.config.param.PageBase;

import java.util.List;

public class UserReq {
    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-login")
    public static class login {
        @NotBlank
        @Schema(description = "账号")
        private String account;
        @NotBlank
        @Schema(description = "密码")
        private String password;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-list")
    public static class list extends PageBase {
        @Schema(description = "名字/账号")
        private String name;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-add")
    public static class add {
        @NotBlank
        @Schema(description = "账号")
        private String account;
        @NotBlank
        @Schema(description = "密码")
        private String password;
        @NotBlank
        @Schema(description = "名字")
        private String name;
        @Schema(description = "域")
        private String domain = Domain.PC;
        @Schema(description = "角色")
        private List<String> role;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-setRole")
    public static class setRole {
        @NotNull
        @Schema(description = "id")
        private Long id;
        @Schema(description = "角色")
        private List<String> role;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-setPassword")
    public static class setPassword {
        @NotNull
        @Schema(description = "id")
        private Long id;
        @NotBlank
        @Schema(description = "密码")
        private String password;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-changePassword")
    public static class changePassword {
        @NotBlank
        @Schema(description = "旧密码")
        private String oldPassword;
        @NotBlank
        @Schema(description = "新密码")
        private String newPassword;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-menu")
    public static class menu {
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-del")
    public static class del {
        @NotNull(message = "用户id不能为空")
        @Schema(description = "id")
        private Long id;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-userInfo")
    public static class userInfo {
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserReq-menu")
    public static class module {
        @NotNull(message = "模块tag不能为空")
        @Schema(description = "tag")
        private String tag;
    }
}
