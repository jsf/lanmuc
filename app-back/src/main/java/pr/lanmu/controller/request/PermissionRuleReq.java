package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


public class PermissionRuleReq {
    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionRuleReq-get")
    public static class get {
        @NotNull(message = "id不能为空")
        @Schema(description = "id")
        private Long id;
    }
}