package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


public class ProjectReq {
    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ProjectReq-save")
    public static class save {
        @Schema(description = "id")
        private Long id;
        @NotBlank
        @Schema(description = "项目名字")
        private String name;
        @NotBlank
        @Schema(description = "开发平台地址")
        private String url;
    }
}