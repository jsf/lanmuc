package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import pr.lanmu.config.param.PageBase;

public class CronReq {
    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "CronReq-list")
    public static class list extends PageBase {
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "CronReq-post")
    public static class post {
        @NotBlank(message = "任务名称不能为空")
        @Schema(description = "任务名称")
        private String name;
        @NotBlank(message = "任务标识不能为空")
        @Schema(description = "任务标识")
        private String tag;
        @NotBlank(message = "定时规则不能为空")
        @Schema(description = "定时规则")
        private String code;
        @NotBlank(message = "调用路径不能为空")
        @Schema(description = "调用路径")
        private String url;
        @NotBlank(message = "调用凭证不能为空")
        @Schema(description = "调用凭证")
        private String token;
        @NotNull(message = "任务状态不能为空")
        @Schema(description = "任务状态：是否启用")
        private Boolean state;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "CronReq-delete")
    public static class delete {
        @NotNull(message = "主键不能为空")
        @Schema(description = "主键")
        private Long id;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "CronReq-put")
    public static class put extends post {
        @NotNull(message = "主键不能为空")
        @Schema(description = "主键")
        private Long id;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "CronReq-loads")
    public static class loads {
    }
}
