package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

public class ModuleReq {
    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleReq-save")
    public static class save {
        @Schema(description = "id")
        private Long id;
        @NotBlank(message = "模块名不能为空")
        @Schema(description = "模块名")
        private String name;
        @NotNull(message = "父模块不能为空")
        @Schema(description = "父id")
        private Long pid;
        @Schema(description = "url路径")
        private String path;
        @Schema(description = "模块路径")
        private String component;
        @Schema(description = "拓展信息")
        private String meta;
        @Schema(description = "排序号")
        private Integer sort;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleReq-permission")
    public static class permission {
        @NotNull(message = "模块id不能为空")
        @Schema(description = "模块id")
        private Long id;
        @NotNull
        @Schema(description = "接口id")
        private List<Long> permission;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleReq-del")
    public static class del {
        @NotNull(message = "模块id不能为空")
        @Schema(description = "模块id")
        private Long id;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleReq-all")
    public static class all {
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleReq-root")
    public static class root {
        @NotNull(message = "模块id不能为空")
        @Schema(description = "id")
        private Long id;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleReq-roots")
    public static class roots {

    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleReq-getModuleTableInfo")
    public static class getModuleTableInfo {
        @NotNull(message = "模块路径不能为空")
        @Schema(description = "模块路径")
        private String path;
    }
}
