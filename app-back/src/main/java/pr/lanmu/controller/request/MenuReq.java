package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

public class MenuReq {
    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "MenuReq-save")
    public static class save {
        @Schema(description = "菜单id")
        private Long id;
        @NotNull
        @Schema(description = "父菜单id")
        private Long pid;
        @NotBlank
        @Schema(description = "菜单标题")
        private String title;
        @NotBlank
        @Schema(description = "路由名")
        private String name;
        @NotBlank
        @Schema(description = "路由路径")
        private String path;
        @Schema(description = "组件路径")
        private String component;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "MenuReq-setSort")
    public static class setSort {
        @Valid
        @NotNull
        private List<item> list;

        @Data
        @EqualsAndHashCode(callSuper = false)
        @Schema(description = "MenuReq-setSort-item")
        public static class item {
            @NotNull
            @Schema(description = "菜单id")
            private Long id;
            @NotNull
            @Schema(description = "排序")
            private Integer sort;
        }
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "MenuReq-del")
    public static class del {
        @NotNull
        @Schema(description = "菜单id")
        private Long id;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "MenuReq-del")
    public static class all {
    }
}
