package pr.lanmu.controller.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import pr.lanmu.annotation.EnumValue;

import java.util.List;

public class PermissionReq {
    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-save")
    public static class save {
        @Schema(description = "id")
        private Long id;
        @NotNull(message = "父接口不能为空")
        @Schema(description = "父id")
        private Long pid;
        @NotBlank(message = "接口名不能为空")
        @Schema(description = "接口名")
        private String name;
        @NotBlank(message = "url不能为空")
        @Schema(description = "url")
        private String url;
        @Schema(description = "param")
        private String param;
        @Schema(description = "paramValid")
        private String paramValid;
        @Schema(description = "json")
        private String json;
        @Schema(description = "response")
        private String response;
        @NotNull(message = "开放不能为空")
        @Schema(description = "open")
        private Boolean open;
        @NotNull(message = "鉴权不能为空")
        @Schema(description = "是否鉴权")
        private Boolean authentication;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-save")
    public static class saveAndBingModule {
        @NotNull(message = "模块不能为空")
        @Schema(description = "模块id")
        private Long moduleId;
        @NotNull(message = "父接口不能为空")
        @Schema(description = "父id")
        private Long pid;
        @NotBlank(message = "接口名不能为空")
        @Schema(description = "接口名")
        private String name;
        @NotBlank(message = "url不能为空")
        @Schema(description = "url")
        private String url;
        @Schema(description = "param")
        private String param;
        @Schema(description = "paramValid")
        private String paramValid;
        @Schema(description = "json")
        private String json;
        @Schema(description = "response")
        private String response;
        @NotNull(message = "open不能为空")
        @Schema(description = "open")
        private Boolean open;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-del")
    public static class del {
        @NotNull(message = "接口id不能为空")
        @Schema(description = "接口id")
        private Long id;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-all")
    public static class all {
        @Schema(description = "是否包含开放的接口")
        private Boolean hasOpen = false;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-list")
    public static class list {
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-saveRule")
    public static class saveRule {
        @NotNull(message = "接口id不能为空")
        @Schema(description = "接口id")
        private Long id;
        @NotBlank(message = "mock规则不能为空")
        @Schema(description = "mock规则")
        private String rule;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-saveTestResult")
    public static class saveTestResult {
        @NotNull(message = "接口id不能为空")
        @Schema(description = "接口id")
        private Long id;
        @NotNull(message = "测试结果不能为空")
        @Schema(description = "测试结果")
        private Boolean testResult;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-setAutoTest")
    public static class setAutoTest {
        @NotNull(message = "接口id不能为空")
        @Schema(description = "接口id")
        private Long id;
        @NotNull(message = "是否自动化测试不能为空")
        @Schema(description = "是否自动化测试")
        private Boolean autoTest;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-setOpen")
    public static class setOpen {
        @NotNull(message = "接口id不能为空")
        @Schema(description = "接口id")
        private Long id;
        @NotNull(message = "是否开放不能为空")
        @Schema(description = "是否开放")
        private Boolean open;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-setAuthentication")
    public static class setAuthentication {
        @NotNull(message = "接口id不能为空")
        @Schema(description = "接口id")
        private Long id;
        @NotNull(message = "是否鉴权不能为空")
        @Schema(description = "是否鉴权")
        private Boolean authentication;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-copyDir")
    public static class copyDir {
        @NotNull(message = "接口id不能为空")
        @Schema(description = "接口id")
        private Long id;
        @NotBlank(message = "接口名不能为空")
        @Schema(description = "接口名")
        private String name;
        @NotBlank(message = "url不能为空")
        @Schema(description = "url")
        private String url;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionReq-config")
    public static class config {
        @NotNull(message = "接口id不能为空")
        @Schema(description = "接口id")
        private Long id;
        @NotNull(message = "类型不能为空,0新增,1修改")
        @EnumValue(strValues = {"0", "1"}, message = "类型错误")
        @Schema(description = "类型")
        private String type;
        @NotNull(message = "修改项不能为空,0入参,1校验,2规则")
        @Schema(description = "修改项")
        private List<String> info;
        @NotBlank(message = "表名不能为空")
        @Schema(description = "表名")
        private String table;
    }
}