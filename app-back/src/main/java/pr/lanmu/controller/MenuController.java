package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.config.util.Res;
import pr.lanmu.controller.request.MenuReq;
import pr.lanmu.controller.response.MenuRes;
import pr.lanmu.service.MenuService;

import java.util.List;


@RestController
@Tag(name = "菜单管理")
@RequestMapping("pt/menu")
public class MenuController {
    @Autowired
    private MenuService service;

    @Operation(summary = "获取全部菜单")
    @PostMapping("all")
    public Res<List<MenuRes.all>> all(@RequestBody @Validated MenuReq.all param) {
        return service.all(param);
    }

    @Operation(summary = "保存菜单")
    @PostMapping("save")
    public Res<?> save(@RequestBody @Validated MenuReq.save param) {
        return service.save(param);
    }

    @Operation(summary = "删除菜单")
    @PostMapping("del")
    public Res<?> del(@RequestBody @Validated MenuReq.del param) {
        return service.del(param);
    }

    @Operation(summary = "菜单重排序")
    @PostMapping("setSort")
    public Res<?> setSort(@RequestBody MenuReq.setSort param) {
        return service.setSort(param);
    }
}