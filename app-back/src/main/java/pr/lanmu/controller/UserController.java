package pr.lanmu.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pr.lanmu.annotation.PermissionIgnore;
import pr.lanmu.common.po.Menu;
import pr.lanmu.config.base.UserBase;
import pr.lanmu.config.util.Res;
import pr.lanmu.config.web.CustomPageInfo;
import pr.lanmu.controller.request.UserReq;
import pr.lanmu.controller.response.UserRes;
import pr.lanmu.service.UserService;

import java.util.List;


@RestController
@Tag(name = "用户管理")
@RequestMapping("pt/user")
public class UserController {
    @Autowired
    private UserService service;

    @PermissionIgnore
    @Operation(summary = "登录")
    @PostMapping("login")
    public Res<UserRes.login> login(@Validated @RequestBody UserReq.login param) {
        return service.login(param);
    }

    @Operation(summary = "获取登录用户开发者菜单")
    @PostMapping("menu")
    public Res<List<Menu>> menu(@Validated @RequestBody UserReq.menu param) {
        return service.menu(param);
    }

    @Operation(summary = "获取登录用户业务模块")
    @PostMapping("module")
    public Res<List<UserRes.module>> module(@Validated @RequestBody UserReq.module param) {
        return service.module(param);
    }

    @Operation(summary = "分页查询用户")
    @PostMapping("list")
    public Res<CustomPageInfo<UserRes.list>> list(@Validated @RequestBody UserReq.list param) {
        return service.list(param);
    }

    @Operation(summary = "新增用户")
    @PostMapping("add")
    public Res<?> add(@Validated @RequestBody UserReq.add param) {
        return service.add(param);
    }

    @Operation(summary = "删除用户")
    @PostMapping("del")
    public Res<?> del(@Validated @RequestBody UserReq.del param) {
        return service.del(param);
    }

    @Operation(summary = "设置用户角色")
    @PostMapping("setRole")
    public Res<?> setRole(@Validated @RequestBody UserReq.setRole param) {
        return service.setRole(param);
    }

    @Operation(summary = "设置用户密码")
    @PostMapping("setPassword")
    public Res<?> setPassword(@Validated @RequestBody UserReq.setPassword param) {
        return service.setPassword(param);
    }

    @Operation(summary = "修改自己密码")
    @PostMapping("changePassword")
    public Res<?> changePassword(@Validated @RequestBody UserReq.changePassword param) {
        return service.changePassword(param);
    }

    @Operation(summary = "获取用户信息")
    @PostMapping("userInfo")
    public Res<UserBase> userInfo(@Validated @RequestBody UserReq.userInfo param) {
        return service.userInfo(param);
    }
}
