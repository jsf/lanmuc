package pr.lanmu.controller.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

public class ModuleRes {
    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleRes-modules")
    public static class modules extends pr.lanmu.common.po.Module {
        @Schema(description = "子模块")
        private List<modules> children;
        @Schema(description = "接口权限id集合")
        private String permissionId;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "ModuleRes-getModuleTableInfo")
    public static class getModuleTableInfo {
        @Schema(description = "表名")
        private String tableName;
        @Schema(description = "表字段对应描述")
        private Object tableInfo;
        @Schema(description = "接口信息")
        private List<item> permissionInfo;

        @Data
        @Accessors(chain = true)
        @EqualsAndHashCode(callSuper = false)
        @Schema(name = "ModuleRes-getModuleTableInfo-item")
        public static class item {
            @Schema(description = "接口路径")
            private String url;
            @Schema(description = "接口名称")
            private String name;
            @Schema(description = "接口入参描述")
            private Object req;
            @Schema(description = "接口入参校验")
            private Object valid;
            @JsonIgnore
            @Schema(description = "接口入参描述")
            private String paramInfo;
            @JsonIgnore
            @Schema(description = "接口入参校验")
            private String paramValid;
        }
    }
}
