package pr.lanmu.controller.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pr.lanmu.common.po.Permission;

import java.util.List;

public class PermissionRes {
    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionRes-all")
    public static class all extends Permission {
        @Schema(description = "子接口")
        private List<all> children;
        @Schema(description = "参数格式内容")
        private String param;
        @Schema(description = "参数检验规则")
        private String paramValid;
        @Schema(description = "执行的json")
        private String json;
        @Schema(description = "响应格式内容")
        private String response;
    }

    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionRes-list")
    public static class list {
        @Schema(description = "主键id")
        private Long id;
        @Schema(description = "接口路径")
        private String url;
        @Schema(description = "接口名字/组名字")
        private String name;
    }
}
