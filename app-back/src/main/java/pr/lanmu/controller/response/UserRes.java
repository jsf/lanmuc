package pr.lanmu.controller.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import pr.lanmu.common.po.User;

import java.util.List;

public class UserRes {
    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PCUserRes-login")
    public static class login {
        @Schema(description = "登录人信息")
        private User user;
        @Schema(description = "token")
        private String token;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserRes-list")
    public static class list extends User {
        @Schema(description = "角色信息")
        private String roleName;
    }

    @Data
    @Accessors(chain = true)
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "UserRes-module")
    public static class module extends pr.lanmu.common.po.Module {
        @Schema(description = "子模块")
        private List<module> children;
    }
}
