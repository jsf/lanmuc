package pr.lanmu.controller.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pr.lanmu.common.po.Menu;

import java.util.List;

public class MenuRes {
    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "MenuRes-all")
    public static class all extends Menu {
        @Schema(description = "子菜单")
        private List<all> children;
    }
}
