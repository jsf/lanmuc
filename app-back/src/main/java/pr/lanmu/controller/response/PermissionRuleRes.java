package pr.lanmu.controller.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pr.lanmu.common.po.PermissionMockRule;

public class PermissionRuleRes {
    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "PermissionRuleRes-get")
    public static class get extends PermissionMockRule {
    }
}
