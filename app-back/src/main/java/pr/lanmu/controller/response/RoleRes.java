package pr.lanmu.controller.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pr.lanmu.common.po.Role;

public class RoleRes {
    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "RoleRes-list")
    public static class list extends Role {
        @Schema(description = "菜单id")
        private String menuId;
        @Schema(description = "模块id")
        private String moduleId;
    }
}
