package pr.lanmu.controller.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pr.lanmu.common.po.Cron;

public class CronRes {
    @Data
    @EqualsAndHashCode(callSuper = false)
    @Schema(name = "CronRes-list")
    public static class list extends Cron {
    }
}
