package pr.lanmu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pr.lanmu.annotation.CustomLog;
import pr.lanmu.common.mapper.PermissionMockRuleMapper;
import pr.lanmu.config.util.ConvertUtil;
import pr.lanmu.config.util.Res;
import pr.lanmu.controller.request.PermissionRuleReq;
import pr.lanmu.controller.response.PermissionRuleRes;

/**
 * 权限规则服务类
 */
@Service
@CustomLog
public class PermissionRuleService {
    @Autowired
    private PermissionMockRuleMapper permissionMockRuleMapper;

    /**
     * 获取权限的规则
     *
     * @param param .
     * @return .
     */
    public Res<PermissionRuleRes.get> get(PermissionRuleReq.get param) {
        return Res.success(() -> {
            var res = permissionMockRuleMapper.selectOneById(param.getId());
            return ConvertUtil.copy(res, new PermissionRuleRes.get());
        });
    }
}

