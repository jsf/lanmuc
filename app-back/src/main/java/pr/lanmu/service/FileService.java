package pr.lanmu.service;

import cn.hutool.core.io.FileUtil;
import com.aliyun.oss.OSS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import pr.lanmu.AppApplication;
import pr.lanmu.config.util.Log;
import pr.lanmu.config.util.Res;
import pr.lanmu.third.OssClientBean;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

@Service
public class FileService {
    @Value("${spring.application.name}")
    private String ossBasePath;
    @Autowired(required = false)
    private OSS oss;

    public Res<?> localUpload(MultipartHttpServletRequest request, Long modelId) {
        String basePath = AppApplication.FILE_PATH;
        String errorMsg = "文件上传失败";
        try {
            String s = File.separator;
            MultipartFile file = request.getFile("file");
            if (file != null && file.isEmpty()) {
                return Res.fail("空文件错误");
            }
            String fileName;
            if (file != null) fileName = file.getOriginalFilename();
            else return Res.fail("空文件错误");
            String prefixName = "file" + s;
            long time = new Date().getTime();
            String path = prefixName + modelId + s + time + s + fileName;
            String pathShow = "file" + s + "download" + s + modelId + s + time + s + fileName;
            File dest = new File(basePath + s + path);
            if (!dest.getParentFile().exists()) {
                FileUtil.mkdir(dest.getParent());
            }
            try {
                file.transferTo(dest);
            } catch (IOException e) {
                Log.error("上传目标地址：{}", basePath + s + path);
                Log.error(errorMsg, e);
                return Res.fail(errorMsg);
            }
            Map<String, String> data = Map.of("path", pathShow.replace(s, "/"));
            return Res.success(data);
        } catch (Exception e) {
            Log.error(errorMsg);
            return Res.fail(errorMsg);
        }
    }

    public Res<?> ossUpload(MultipartHttpServletRequest request, Long modelId) {
        try {
            String s = "/";
            MultipartFile file = request.getFile("file");
            if (file != null && file.isEmpty()) return Res.fail("空文件错误");
            String fileName;
            if (file != null) fileName = file.getOriginalFilename();
            else return Res.fail("空文件错误");
            String path = ossBasePath + s + modelId + s + System.currentTimeMillis() + s + fileName;
            oss.putObject(OssClientBean.bucketName, path, file.getInputStream());
            Map<String, String> data = Map.of("path", OssClientBean.url + s + path);
            return Res.success(data);
        } catch (Exception e) {
            Log.error("文件上传失败");
            return Res.fail("文件上传失败");
        }
    }
}
