package pr.lanmu.service;

import org.springframework.stereotype.Service;
import pr.lanmu.annotation.CustomLog;
import pr.lanmu.common.po.TableInfo;
import pr.lanmu.config.util.Res;

import java.util.List;

/**
 * 数据库表服务
 */
@Service
@CustomLog
public class TableService {

    public Res<List<TableInfo>> list() {
        return Res.success(new TableInfo().list());
    }
}