package pr.lanmu.dao;

import org.apache.ibatis.annotations.Param;
import pr.lanmu.controller.response.RoleRes;

import java.util.List;

public interface RoleDao {
    List<RoleRes.list> list();

    void delUserRole(@Param("id") Long id);
}