package pr.lanmu.dao;

import org.apache.ibatis.annotations.Param;
import pr.lanmu.controller.response.ModuleRes;

import java.util.List;

public interface ModuleDao {
    List<ModuleRes.modules> modules(@Param("tag") String tag);
}