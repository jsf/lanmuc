package pr.lanmu.dao;

import org.apache.ibatis.annotations.Param;

public interface CronDao {
    void plusVersion(@Param("id") Long id);
}
