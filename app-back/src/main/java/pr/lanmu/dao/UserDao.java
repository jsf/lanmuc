package pr.lanmu.dao;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import pr.lanmu.common.po.Menu;
import pr.lanmu.common.po.Module;
import pr.lanmu.controller.request.UserReq;

import java.util.List;
import java.util.Map;

public interface UserDao {
    @MapKey("id")
    List<Map<String, Object>> list(UserReq.list param);

    List<Menu> menu(@Param("userId") Long userId);

    List<Module> module(@Param("userId") Long userId, @Param("tag") String tag);
}