package pr.lanmu.create;

import cn.hutool.core.io.FileUtil;
import com.mybatisflex.codegen.Generator;
import com.mybatisflex.codegen.config.EntityConfig;
import com.mybatisflex.codegen.config.GlobalConfig;
import com.mybatisflex.codegen.config.TableConfig;
import com.mybatisflex.core.BaseMapper;
import com.zaxxer.hikari.HikariDataSource;
import pr.lanmu.config.mybatis.MyInsertListener;
import pr.lanmu.config.mybatis.MyUpdateListener;

import java.io.File;
import java.util.List;

public class Codegen {

    public static void main(String[] args) {
        //配置数据源
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/app?characterEncoding=utf-8");
        dataSource.setUsername("root");
        dataSource.setPassword("123456");

        GlobalConfig globalConfig = createGlobalConfigUseStyle();

        //通过 datasource 和 globalConfig 创建代码生成器
        Generator generator = new Generator(dataSource,
                globalConfig);

        //生成代码
        generator.generate();

        //修改swagger的注解
        String entityPackage = globalConfig.getEntityPackage();
        //找到java文件的路径
        String entityPath = entityPackage.replace(".", "/");
        //获取到java文件的绝对路径
        String absolutePath = System.getProperty("user.dir") + "/src/main/java/" + entityPath;
        //找到文件夹下所有的文件，读取文件内容
        List<File> files = FileUtil.loopFiles(absolutePath);
        for (File file : files) {
            String content = FileUtil.readUtf8String(file);
            //替换掉原来的注解
            content = content.replace("import io.swagger.annotations.ApiModel;", "import io.swagger.v3.oas.annotations.media.Schema;");
            content = content.replace("import io.swagger.annotations.ApiModelProperty;\n", "");
            content = content.replace("import java.io.Serializable;\n", "");
            content = content.replace("@ApiModel(", "@Schema(description = ");
            content = content.replace("@ApiModelProperty(", "@Schema(description = ");
            //重新写入文件
            FileUtil.writeUtf8String(content, file);
        }
    }

    public static GlobalConfig createGlobalConfigUseStyle() {
        //创建配置内容
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setAuthor("pr");

        //设置table
        TableConfig tableConfig = new TableConfig();
        tableConfig.setInsertListenerClass(MyInsertListener.class);
        tableConfig.setUpdateListenerClass(MyUpdateListener.class);
        globalConfig.setTableConfig(tableConfig);

        //设置根包
        globalConfig.getPackageConfig()
                .setEntityPackage("pr.lanmu.common.po")
                .setMapperPackage("pr.lanmu.common.mapper");

        //设置表前缀和只生成哪些表，setGenerateTable 未配置时，生成所有表
        globalConfig.getStrategyConfig()
                .setGenerateTable("crud");

        //设置生成 entity 并启用 Lombok
        globalConfig.enableEntity()
                .setWithLombok(false)
                .setOverwriteEnable(true)
                .setSwaggerVersion(EntityConfig.SwaggerVersion.FOX)
                .setWithSwagger(true)
                .setWithActiveRecord(true);

        //设置生成 mapper
        globalConfig.enableMapper()
                .setClassSuffix("Mapper")
                .setSuperClass(BaseMapper.class)
                .setOverwriteEnable(true);
        return globalConfig;
    }
}