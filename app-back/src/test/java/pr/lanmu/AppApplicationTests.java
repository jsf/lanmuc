package pr.lanmu;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pr.lanmu.third.WxClient;

import java.io.Serializable;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AppApplicationTests {

    @Autowired
    private WxClient wxClient;

    @Test
    void contextLoads() {
        Map<String, ? extends Serializable> data = Map.of("account", "lanmuc", "password", "123456");
        System.out.println(wxClient.login(data));
    }
}
