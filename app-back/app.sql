/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 80035
 Source Host           : localhost:3306
 Source Schema         : app

 Target Server Type    : MySQL
 Target Server Version : 80035
 File Encoding         : 65001

 Date: 12/07/2024 15:02:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跳转',
  `des` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建用户ID',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'banner管理表-cud' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES (30, 'file/download/1/1720449580684/favicon.ico', '3', '1', 2, 1699793910808, 1, 1720596652599, 1);

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '优惠卷ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `start_time` bigint NOT NULL COMMENT '开始时间',
  `end_time` bigint NOT NULL COMMENT '结束时间',
  `on_shelf` bit(1) NOT NULL DEFAULT b'0' COMMENT '上架',
  `specified_product` bit(1) NOT NULL COMMENT '商品卷',
  `designated_products` json NULL COMMENT '指定商品',
  `amount_full` decimal(10, 2) NOT NULL COMMENT '满额',
  `amount_reduce` decimal(10, 2) NOT NULL COMMENT '减额',
  `quantity` int NOT NULL COMMENT '数量',
  `received_quantity` int NOT NULL DEFAULT 0 COMMENT '已领取数量',
  `sort` int NOT NULL COMMENT '排序',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建用户ID',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_name`(`name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠卷表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES (2, '满100减30通用卷', 1720540800000, 1752163199999, b'1', b'0', '[]', 100.00, 30.00, 100, 0, 1, 1720593180167, 1, 1720595524539, 1);

-- ----------------------------
-- Table structure for course_category
-- ----------------------------
DROP TABLE IF EXISTS `course_category`;
CREATE TABLE `course_category`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `icon_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `is_hot` bit(1) NOT NULL COMMENT '是否热门',
  `sort` bigint NOT NULL COMMENT '排序',
  `pid` bigint NOT NULL COMMENT '父id|',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建用户ID',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 240 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程分类表-crud' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of course_category
-- ----------------------------
INSERT INTO `course_category` VALUES (71, '财会金融', 'file/download/1/1720017275849/default-1000.png', b'0', 1, 0, 1719201381703, 1, 1720017277805, 1);
INSERT INTO `course_category` VALUES (72, '初级会计', 'file/download/1/1720017311844/default-1000.png', b'0', 1, 71, 1719201381723, 1, 1720017313559, 1);
INSERT INTO `course_category` VALUES (73, '中级会计', 'file/download/1/1720017921363/default-1017.png', b'0', 2, 71, 1719201381742, 1, 1720017925887, 1);
INSERT INTO `course_category` VALUES (74, '注册会计', 'file/download/1/1720017958472/default-1017.png', b'0', 3, 71, 1719201381761, 1, 1720017959878, 1);
INSERT INTO `course_category` VALUES (75, '税务师', 'file/download/1/1720017995870/default-1017.png', b'0', 4, 71, 1719201381780, 1, 1720017997354, 1);
INSERT INTO `course_category` VALUES (76, '初级经济师', 'file/download/1/1720018017755/default-1017.png', b'0', 5, 71, 1719201381797, 1, 1720018019174, 1);
INSERT INTO `course_category` VALUES (77, '中级经济师', 'file/download/1/1720018074762/default-1017.png', b'0', 6, 71, 1719201381815, 1, 1720018076569, 1);
INSERT INTO `course_category` VALUES (78, '基金从业', 'file/download/1/1720018194514/default-1017.png', b'0', 7, 71, 1719201381833, 1, 1720018195670, 1);
INSERT INTO `course_category` VALUES (79, '证券从业', 'file/download/1/1720018221512/default-1017.png', b'0', 8, 71, 1719201381850, 1, 1720018222730, 1);
INSERT INTO `course_category` VALUES (80, '银行从业', 'file/download/1/1720018240802/default-1017.png', b'0', 9, 71, 1719201381870, 1, 1720018241783, 1);
INSERT INTO `course_category` VALUES (81, '期货从业', 'file/download/1/1720018265514/default-1017.png', b'0', 10, 71, 1719201381890, 1, 1720018267046, 1);
INSERT INTO `course_category` VALUES (82, '初级审计师', 'file/download/1/1720018287820/default-1017.png', b'0', 11, 71, 1719201381910, 1, 1720018288905, 1);
INSERT INTO `course_category` VALUES (83, '中级审计师', 'file/download/1/1720018309539/default-1017.png', b'0', 12, 71, 1719201381930, 1, 1720018310887, 1);
INSERT INTO `course_category` VALUES (84, '初级管理会计', 'file/download/1/1720018331258/default-1017.png', b'0', 13, 71, 1719201381948, 1, 1720018332165, 1);
INSERT INTO `course_category` VALUES (85, '职业资格', 'file/download/1/1720018347537/default-1017.png', b'0', 2, 0, 1719201381965, 1, 1720018348807, 1);
INSERT INTO `course_category` VALUES (86, '幼儿教师（笔试）', 'file/download/1/1720018562669/default-1017.png', b'0', 1, 85, 1719201381983, 1, 1720018563833, 1);
INSERT INTO `course_category` VALUES (87, '小学教师（笔试）', 'file/download/1/1720018583971/default-1017.png', b'0', 2, 85, 1719201382001, 1, 1720018585034, 1);
INSERT INTO `course_category` VALUES (88, '初中教师（笔试）', 'file/download/1/1720018604371/default-1017.png', b'0', 3, 85, 1719201382020, 1, 1720018605939, 1);
INSERT INTO `course_category` VALUES (89, '高中教师（笔试）', 'file/download/1/1720018626579/default-1017.png', b'0', 4, 85, 1719201382038, 1, 1720018628052, 1);
INSERT INTO `course_category` VALUES (90, '面试', 'file/download/1/1720018643212/default-1017.png', b'0', 5, 85, 1719201382058, 1, 1720018644275, 1);
INSERT INTO `course_category` VALUES (91, '普通话水平测试', 'file/download/1/1720018663633/default-1017.png', b'0', 6, 85, 1719201382076, 1, 1720018664760, 1);
INSERT INTO `course_category` VALUES (92, '导游资格证', 'file/download/1/1720018680247/default-1017.png', b'0', 7, 85, 1719201382093, 1, 1720018681071, 1);
INSERT INTO `course_category` VALUES (93, '初级社会工作师', 'file/download/1/1720018699445/default-1017.png', b'0', 8, 85, 1719201382110, 1, 1720018700672, 1);
INSERT INTO `course_category` VALUES (94, '中级社会工作师', 'file/download/1/1720018718882/default-1017.png', b'0', 9, 85, 1719201382129, 1, 1720018720134, 1);
INSERT INTO `course_category` VALUES (95, '系统集成项目管理工程师（软考中级）', 'file/download/1/1720018745492/default-1017.png', b'0', 10, 85, 1719201382146, 1, 1720018746824, 1);
INSERT INTO `course_category` VALUES (96, '信息系统项目管理师（软考高级）', 'file/download/1/1720018771889/default-1017.png', b'0', 11, 85, 1719201382167, 1, 1720018772933, 1);
INSERT INTO `course_category` VALUES (97, '演出经纪人', 'file/download/1/1720018789702/default-1017.png', b'0', 12, 85, 1719201382186, 1, 1720018790676, 1);
INSERT INTO `course_category` VALUES (98, '学历提升', 'file/download/1/1720018364417/default-1017.png', b'0', 3, 0, 1719201382205, 1, 1720018365831, 1);
INSERT INTO `course_category` VALUES (99, '成人高考', 'file/download/1/1720018816488/default-1017.png', b'0', 1, 98, 1719201382224, 1, 1720018817883, 1);
INSERT INTO `course_category` VALUES (100, '学位英语', 'file/download/1/1720018836806/default-1017.png', b'0', 2, 98, 1719201382243, 1, 1720018838040, 1);
INSERT INTO `course_category` VALUES (101, '研究生考试', 'file/download/1/1720018856362/default-1017.png', b'0', 3, 98, 1719201382264, 1, 1720018857497, 1);
INSERT INTO `course_category` VALUES (102, '自学考试', 'file/download/1/1720018877539/default-1017.png', b'0', 4, 98, 1719201382284, 1, 1720018878724, 1);
INSERT INTO `course_category` VALUES (103, '英语四级', 'file/download/1/1720018897635/default-1017.png', b'0', 5, 98, 1719201382303, 1, 1720018898949, 1);
INSERT INTO `course_category` VALUES (104, '英语六级', 'file/download/1/1720018919455/default-1017.png', b'0', 6, 98, 1719201382322, 1, 1720018920677, 1);
INSERT INTO `course_category` VALUES (105, '军队文职', 'file/download/1/1720018386586/default-1017.png', b'0', 4, 0, 1719201382341, 1, 1720018387679, 1);
INSERT INTO `course_category` VALUES (106, '管理类', 'file/download/1/1720018947145/default-1017.png', b'0', 1, 105, 1719201382360, 1, 1720018948214, 1);
INSERT INTO `course_category` VALUES (107, '经济类', 'file/download/1/1720018963518/default-1017.png', b'0', 2, 105, 1719201382378, 1, 1720018964778, 1);
INSERT INTO `course_category` VALUES (108, '医学类', 'file/download/1/1720018987387/default-1017.png', b'0', 3, 105, 1719201382396, 1, 1720018988895, 1);
INSERT INTO `course_category` VALUES (109, '药学类', 'file/download/1/1720019003343/default-1017.png', b'0', 4, 105, 1719201382413, 1, 1720019004274, 1);
INSERT INTO `course_category` VALUES (110, '理工类', 'file/download/1/1720019024505/default-1017.png', b'0', 5, 105, 1719201382431, 1, 1720019025658, 1);
INSERT INTO `course_category` VALUES (111, '文体教育类', 'file/download/1/1720019041933/default-1017.png', b'0', 6, 105, 1719201382449, 1, 1720019042906, 1);
INSERT INTO `course_category` VALUES (112, '图书档案类', 'file/download/1/1720019060582/default-1017.png', b'0', 7, 105, 1719201382466, 1, 1720019061816, 1);
INSERT INTO `course_category` VALUES (113, '语言类', 'file/download/1/1720019078445/default-1017.png', b'0', 8, 105, 1719201382485, 1, 1720019079782, 1);
INSERT INTO `course_category` VALUES (114, '艺术类', 'file/download/1/1720019100239/default-1017.png', b'0', 9, 105, 1719201382502, 1, 1720019101072, 1);
INSERT INTO `course_category` VALUES (115, '营房管理', 'file/download/1/1720019120213/default-1017.png', b'0', 10, 105, 1719201382521, 1, 1720019121114, 1);
INSERT INTO `course_category` VALUES (116, '炊事员', 'file/download/1/1720019149668/default-1017.png', b'0', 11, 105, 1719201382541, 1, 1720019150862, 1);
INSERT INTO `course_category` VALUES (117, '公务员/文印员', 'file/download/1/1720019178508/default-1017.png', b'0', 12, 105, 1719201382559, 1, 1720019179485, 1);
INSERT INTO `course_category` VALUES (118, '司机/交通运输', 'file/download/1/1720019200507/default-1017.png', b'0', 13, 105, 1719201382576, 1, 1720019201645, 1);
INSERT INTO `course_category` VALUES (119, '保管类', 'file/download/1/1720019219043/default-1017.png', b'0', 14, 105, 1719201382594, 1, 1720019220239, 1);
INSERT INTO `course_category` VALUES (120, '卫生勤务类', 'file/download/1/1720019236839/default-1017.png', b'0', 15, 105, 1719201382612, 1, 1720019237954, 1);
INSERT INTO `course_category` VALUES (121, '工商管理类', 'file/download/1/1720019256692/default-1017.png', b'0', 16, 105, 1719201382634, 1, 1720019257845, 1);
INSERT INTO `course_category` VALUES (122, '建筑工程', 'file/download/1/1720018407470/default-1017.png', b'0', 5, 0, 1719201382652, 1, 1720018408721, 1);
INSERT INTO `course_category` VALUES (123, '一级建造师', 'file/download/1/1720019566795/default-1017.png', b'0', 1, 122, 1719201382670, 1, 1720019567834, 1);
INSERT INTO `course_category` VALUES (124, '二级建造师', 'file/download/1/1720019590220/default-1017.png', b'0', 2, 122, 1719201382688, 1, 1720019591260, 1);
INSERT INTO `course_category` VALUES (125, '一级造价工程师', 'file/download/1/1720019609782/default-1017.png', b'0', 3, 122, 1719201382705, 1, 1720019611018, 1);
INSERT INTO `course_category` VALUES (126, '一级注册消防工程师', 'file/download/1/1720019627799/default-1017.png', b'0', 4, 122, 1719201382725, 1, 1720019628757, 1);
INSERT INTO `course_category` VALUES (127, '二级注册消防工程师', 'file/download/1/1720019645412/default-1017.png', b'0', 5, 122, 1719201382744, 1, 1720019646333, 1);
INSERT INTO `course_category` VALUES (128, '消防设施操作员', 'file/download/1/1720019668150/default-1017.png', b'0', 6, 122, 1719201382764, 1, 1720019669475, 1);
INSERT INTO `course_category` VALUES (129, '监理工程师', 'file/download/1/1720019689861/default-1017.png', b'0', 7, 122, 1719201382783, 1, 1720019691015, 1);
INSERT INTO `course_category` VALUES (130, '中级注册安全工程师', 'file/download/1/1720019708636/default-1017.png', b'0', 8, 122, 1719201382802, 1, 1720019709537, 1);
INSERT INTO `course_category` VALUES (131, '注册城乡规划师', 'file/download/1/1720019727636/default-1017.png', b'0', 9, 122, 1719201382824, 1, 1720019728403, 1);
INSERT INTO `course_category` VALUES (132, '安全员', 'file/download/1/1720018428511/default-1017.png', b'0', 6, 0, 1719201382843, 1, 1720018429568, 1);
INSERT INTO `course_category` VALUES (133, '全国版', 'file/download/1/1720020059074/default-1017.png', b'0', 1, 132, 1719201382862, 1, 1720020060379, 1);
INSERT INTO `course_category` VALUES (134, '江苏省', 'file/download/1/1720020082175/default-1017.png', b'0', 2, 132, 1719201382880, 1, 1720020083145, 1);
INSERT INTO `course_category` VALUES (135, '浙江省', 'file/download/1/1720020105324/default-1017.png', b'0', 3, 132, 1719201382899, 1, 1720020106688, 1);
INSERT INTO `course_category` VALUES (136, '河北省', 'file/download/1/1720020125776/default-1017.png', b'0', 4, 132, 1719201382917, 1, 1720020126608, 1);
INSERT INTO `course_category` VALUES (137, '四川省', 'file/download/1/1720020145448/default-1017.png', b'0', 5, 132, 1719201382934, 1, 1720020146656, 1);
INSERT INTO `course_category` VALUES (138, '广西省', 'file/download/1/1720020163715/default-1017.png', b'0', 6, 132, 1719201382952, 1, 1720020164478, 1);
INSERT INTO `course_category` VALUES (139, '广东省', 'file/download/1/1720020182559/default-1017.png', b'0', 7, 132, 1719201382969, 1, 1720020183368, 1);
INSERT INTO `course_category` VALUES (140, '北京市', 'file/download/1/1720020199373/default-1017.png', b'0', 8, 132, 1719201382987, 1, 1720020200295, 1);
INSERT INTO `course_category` VALUES (141, '天津市', 'file/download/1/1720020218581/default-1017.png', b'0', 9, 132, 1719201383006, 1, 1720020219737, 1);
INSERT INTO `course_category` VALUES (142, '上海市', 'file/download/1/1720020237429/default-1017.png', b'0', 10, 132, 1719201383023, 1, 1720020238661, 1);
INSERT INTO `course_category` VALUES (143, '重庆市', 'file/download/1/1720020261743/default-1017.png', b'0', 11, 132, 1719201383043, 1, 1720020262852, 1);
INSERT INTO `course_category` VALUES (144, '山西省', 'file/download/1/1720020281177/default-1017.png', b'0', 12, 132, 1719201383061, 1, 1720020282061, 1);
INSERT INTO `course_category` VALUES (145, '辽宁省', 'file/download/1/1720020298227/default-1017.png', b'0', 13, 132, 1719201383077, 1, 1720020299344, 1);
INSERT INTO `course_category` VALUES (146, '吉林省', 'file/download/1/1720020316696/default-1017.png', b'0', 14, 132, 1719201383095, 1, 1720020317864, 1);
INSERT INTO `course_category` VALUES (147, '黑龙江省', 'file/download/1/1720020338804/default-1017.png', b'0', 15, 132, 1719201383113, 1, 1720020339467, 1);
INSERT INTO `course_category` VALUES (148, '安徽省', 'file/download/1/1720020358949/default-1017.png', b'0', 16, 132, 1719201383130, 1, 1720020359748, 1);
INSERT INTO `course_category` VALUES (149, '福建省', 'file/download/1/1720020383574/default-1017.png', b'0', 17, 132, 1719201383147, 1, 1720020384476, 1);
INSERT INTO `course_category` VALUES (150, '江西省', 'file/download/1/1720020399789/default-1017.png', b'0', 18, 132, 1719201383168, 1, 1720020400753, 1);
INSERT INTO `course_category` VALUES (151, '山东省', 'file/download/1/1720020420952/default-1017.png', b'0', 19, 132, 1719201383186, 1, 1720020422127, 1);
INSERT INTO `course_category` VALUES (152, '河南省', 'file/download/1/1720020440552/default-1017.png', b'0', 20, 132, 1719201383204, 1, 1720020441311, 1);
INSERT INTO `course_category` VALUES (153, '湖北省', 'file/download/1/1720020456979/default-1017.png', b'0', 21, 132, 1719201383220, 1, 1720020457981, 1);
INSERT INTO `course_category` VALUES (154, '湖南省', 'file/download/1/1720020478160/default-1017.png', b'0', 22, 132, 1719201383236, 1, 1720020478941, 1);
INSERT INTO `course_category` VALUES (155, '海南省', 'file/download/1/1720020494977/default-1017.png', b'0', 23, 132, 1719201383272, 1, 1720020495941, 1);
INSERT INTO `course_category` VALUES (156, '贵州省', 'file/download/1/1720020513649/default-1017.png', b'0', 24, 132, 1719201383312, 1, 1720020514419, 1);
INSERT INTO `course_category` VALUES (157, '云南省', 'file/download/1/1720020530512/default-1017.png', b'0', 25, 132, 1719201383333, 1, 1720020531650, 1);
INSERT INTO `course_category` VALUES (158, '陕西省', 'file/download/1/1720020556288/default-1017.png', b'0', 26, 132, 1719201383350, 1, 1720020557413, 1);
INSERT INTO `course_category` VALUES (159, '甘肃省', 'file/download/1/1720020573295/default-1017.png', b'0', 27, 132, 1719201383367, 1, 1720020574313, 1);
INSERT INTO `course_category` VALUES (160, '青海省', 'file/download/1/1720020591359/default-1017.png', b'0', 28, 132, 1719201383385, 1, 1720020592367, 1);
INSERT INTO `course_category` VALUES (161, '内蒙古', 'file/download/1/1720020610191/default-1017.png', b'0', 29, 132, 1719201383402, 1, 1720020611312, 1);
INSERT INTO `course_category` VALUES (162, '西藏', 'file/download/1/1720020630310/default-1017.png', b'0', 30, 132, 1719201383418, 1, 1720020631106, 1);
INSERT INTO `course_category` VALUES (163, '宁夏', 'file/download/1/1720020653199/default-1017.png', b'0', 31, 132, 1719201383435, 1, 1720020653953, 1);
INSERT INTO `course_category` VALUES (164, '新疆', 'file/download/1/1720020672951/default-1017.png', b'0', 32, 132, 1719201383451, 1, 1720020673924, 1);
INSERT INTO `course_category` VALUES (165, '医药健康', 'file/download/1/1720018446409/default-1017.png', b'0', 7, 0, 1719201383467, 1, 1720018447665, 1);
INSERT INTO `course_category` VALUES (166, '执业药师', 'file/download/1/1720021027063/default-1017.png', b'0', 1, 165, 1719201383484, 1, 1720021027809, 1);
INSERT INTO `course_category` VALUES (167, '临床执业医师', 'file/download/1/1720021049747/default-1017.png', b'0', 2, 165, 1719201383501, 1, 1720021050648, 1);
INSERT INTO `course_category` VALUES (168, '临床执业助理医师', 'file/download/1/1720021112558/default-1017.png', b'0', 3, 165, 1719201383517, 1, 1720021113917, 1);
INSERT INTO `course_category` VALUES (169, '口腔执业医师', 'file/download/1/1720021132276/default-1017.png', b'0', 4, 165, 1719201383535, 1, 1720021132978, 1);
INSERT INTO `course_category` VALUES (170, '口腔执业助理医师', 'file/download/1/1720021148576/default-1017.png', b'0', 5, 165, 1719201383551, 1, 1720021149308, 1);
INSERT INTO `course_category` VALUES (171, '中医执业医师', 'file/download/1/1720021170040/default-1017.png', b'0', 6, 165, 1719201383567, 1, 1720021174623, 1);
INSERT INTO `course_category` VALUES (172, '中医执业助理医师', 'file/download/1/1720021191353/default-1017.png', b'0', 7, 165, 1719201383604, 1, 1720021192164, 1);
INSERT INTO `course_category` VALUES (173, '中西医结合执业医师', 'file/download/1/1720021251258/default-1017.png', b'0', 8, 165, 1719201383620, 1, 1720021252866, 1);
INSERT INTO `course_category` VALUES (174, '中西医结合执业助理医师', 'file/download/1/1720021269635/default-1017.png', b'0', 9, 165, 1719201383637, 1, 1720021270973, 1);
INSERT INTO `course_category` VALUES (175, '卫生护理', 'file/download/1/1720018465262/default-1017.png', b'0', 8, 0, 1719201383655, 1, 1720018466650, 1);
INSERT INTO `course_category` VALUES (176, '执业护士', 'file/download/1/1720021314966/default-1017.png', b'0', 1, 175, 1719201383672, 1, 1720021316826, 1);
INSERT INTO `course_category` VALUES (177, '初级护师(护师)［203］', 'file/download/1/1720021340025/default-1017.png', b'0', 2, 175, 1719201383688, 1, 1720021341022, 1);
INSERT INTO `course_category` VALUES (178, '主管护师(中级)［368］', 'file/download/1/1720021361157/default-1017.png', b'0', 3, 175, 1719201383705, 1, 1720021362530, 1);
INSERT INTO `course_category` VALUES (179, '内科护理（中级）［369］', 'file/download/1/1720021387214/default-1017.png', b'0', 4, 175, 1719201383721, 1, 1720021388727, 1);
INSERT INTO `course_category` VALUES (180, '外科护理（中级）［370］', 'file/download/1/1720021405934/default-1017.png', b'0', 5, 175, 1719201383739, 1, 1720021406972, 1);
INSERT INTO `course_category` VALUES (181, '妇产科护理（中级）［371］', 'file/download/1/1720021423940/default-1017.png', b'0', 6, 175, 1719201383755, 1, 1720021425200, 1);
INSERT INTO `course_category` VALUES (182, '儿科护理（中级）［372］', 'file/download/1/1720021445069/default-1017.png', b'0', 7, 175, 1719201383772, 1, 1720021446799, 1);
INSERT INTO `course_category` VALUES (183, '社区护理（中级）［373］', 'file/download/1/1720021461924/default-1017.png', b'0', 8, 175, 1719201383790, 1, 1720021462874, 1);
INSERT INTO `course_category` VALUES (184, '职业技能', 'file/download/1/1720018486964/default-1017.png', b'0', 9, 0, 1719201383808, 1, 1720018488194, 1);
INSERT INTO `course_category` VALUES (185, '互联网营销师', 'file/download/1/1720024785960/default-1017.png', b'0', 1, 184, 1719201383825, 1, 1720024786697, 1);
INSERT INTO `course_category` VALUES (186, '全媒体运营师', 'file/download/1/1720024807126/default-1017.png', b'0', 2, 184, 1719201383842, 1, 1720024807964, 1);
INSERT INTO `course_category` VALUES (187, '家庭教育指导师', 'file/download/1/1720024824904/default-1017.png', b'0', 3, 184, 1719201383858, 1, 1720024825632, 1);
INSERT INTO `course_category` VALUES (188, '碳排放管理师', 'file/download/1/1720024843782/default-1017.png', b'0', 4, 184, 1719201383875, 1, 1720024844463, 1);
INSERT INTO `course_category` VALUES (189, '三级健康管理师', 'file/download/1/1720024862263/default-1017.png', b'0', 5, 184, 1719201383892, 1, 1720024863020, 1);
INSERT INTO `course_category` VALUES (190, '三级公共营养师', 'file/download/1/1720024878787/default-1017.png', b'0', 6, 184, 1719201383908, 1, 1720024880044, 1);
INSERT INTO `course_category` VALUES (191, '心理咨询师（中科院）', 'file/download/1/1720024895127/default-1017.png', b'0', 7, 184, 1719201383925, 1, 1720024896628, 1);
INSERT INTO `course_category` VALUES (192, '心理咨询师', 'file/download/1/1720024912075/default-1017.png', b'0', 8, 184, 1719201383941, 1, 1720024913042, 1);
INSERT INTO `course_category` VALUES (193, '心理咨询师（二级）', 'file/download/1/1720024928911/default-1017.png', b'0', 9, 184, 1719201383957, 1, 1720024929815, 1);
INSERT INTO `course_category` VALUES (194, '心理咨询师（三级）', 'file/download/1/1720024945553/default-1017.png', b'0', 10, 184, 1719201383973, 1, 1720024946405, 1);
INSERT INTO `course_category` VALUES (195, '二级人力资源', 'file/download/1/1720024962451/default-1017.png', b'0', 11, 184, 1719201383991, 1, 1720024963405, 1);
INSERT INTO `course_category` VALUES (196, '三级人力资源', 'file/download/1/1720024978393/default-1017.png', b'0', 12, 184, 1719201384008, 1, 1720024979146, 1);
INSERT INTO `course_category` VALUES (197, '四级人力资源', 'file/download/1/1720024998215/default-1017.png', b'0', 13, 184, 1719201384025, 1, 1720024999150, 1);
INSERT INTO `course_category` VALUES (198, '保育员', 'file/download/1/1720025015661/default-1017.png', b'0', 14, 184, 1719201384040, 1, 1720025016632, 1);
INSERT INTO `course_category` VALUES (199, '初级育婴师（五级）', 'file/download/1/1720025030750/default-1017.png', b'0', 15, 184, 1719201384057, 1, 1720025031711, 1);
INSERT INTO `course_category` VALUES (200, '中级育婴师（四级）', 'file/download/1/1720025047515/default-1017.png', b'0', 16, 184, 1719201384075, 1, 1720025048668, 1);
INSERT INTO `course_category` VALUES (201, '初级茶艺师', 'file/download/1/1720025067490/default-1017.png', b'0', 17, 184, 1719201384092, 1, 1720025068387, 1);
INSERT INTO `course_category` VALUES (202, '中级茶艺师', 'file/download/1/1720025086010/default-1017.png', b'0', 18, 184, 1719201384109, 1, 1720025086866, 1);
INSERT INTO `course_category` VALUES (203, '初级养老护理员', 'file/download/1/1720025101644/default-1017.png', b'0', 19, 184, 1719201384125, 1, 1720025102415, 1);
INSERT INTO `course_category` VALUES (204, '中级养老护理员', 'file/download/1/1720025117984/default-1017.png', b'0', 20, 184, 1719201384141, 1, 1720025118948, 1);
INSERT INTO `course_category` VALUES (205, '高级养老护理员', 'file/download/1/1720025134541/default-1017.png', b'0', 21, 184, 1719201384158, 1, 1720025135676, 1);
INSERT INTO `course_category` VALUES (206, '公务员', 'file/download/1/1720018506008/default-1017.png', b'0', 10, 0, 1719201384175, 1, 1720018507228, 1);
INSERT INTO `course_category` VALUES (207, '国家公务员', 'file/download/1/1720025157714/default-1017.png', b'0', 1, 206, 1719201384192, 1, 1720025158598, 1);
INSERT INTO `course_category` VALUES (208, '北京公务员', 'file/download/1/1720025176052/default-1017.png', b'0', 2, 206, 1719201384209, 1, 1720025176925, 1);
INSERT INTO `course_category` VALUES (209, 'A类江苏省公务员', 'file/download/1/1720025190211/default-1017.png', b'0', 3, 206, 1719201384226, 1, 1720025190985, 1);
INSERT INTO `course_category` VALUES (210, 'B类江苏省公务员', 'file/download/1/1720025207525/default-1017.png', b'0', 4, 206, 1719201384243, 1, 1720025208651, 1);
INSERT INTO `course_category` VALUES (211, 'C类江苏省公务员', 'file/download/1/1720025225252/default-1017.png', b'0', 5, 206, 1719201384259, 1, 1720025226589, 1);
INSERT INTO `course_category` VALUES (212, '广东省公务员', 'file/download/1/1720025242041/default-1017.png', b'0', 6, 206, 1719201384276, 1, 1720025242842, 1);
INSERT INTO `course_category` VALUES (213, '山东省公务员', 'file/download/1/1720025258549/default-1017.png', b'0', 7, 206, 1719201384293, 1, 1720025259392, 1);
INSERT INTO `course_category` VALUES (214, '河南省公务员', 'file/download/1/1720025292526/default-1017.png', b'0', 8, 206, 1719201384309, 1, 1720025293722, 1);
INSERT INTO `course_category` VALUES (215, '海南省公务员', 'file/download/1/1720025310696/default-1017.png', b'0', 9, 206, 1719201384325, 1, 1720025311258, 1);
INSERT INTO `course_category` VALUES (216, '天津市公务员', 'file/download/1/1720025326641/default-1017.png', b'0', 10, 206, 1719201384341, 1, 1720025327266, 1);
INSERT INTO `course_category` VALUES (217, '上海市公务员', 'file/download/1/1720025342236/default-1017.png', b'0', 11, 206, 1719201384358, 1, 1720025343433, 1);
INSERT INTO `course_category` VALUES (218, '四川省公务员', 'file/download/1/1720025362715/default-1017.png', b'0', 12, 206, 1719201384374, 1, 1720025363505, 1);
INSERT INTO `course_category` VALUES (219, '浙江省公务员', 'file/download/1/1720025383589/default-1017.png', b'0', 13, 206, 1719201384392, 1, 1720025384382, 1);
INSERT INTO `course_category` VALUES (220, '重庆市公务员', 'file/download/1/1720025400948/default-1017.png', b'0', 14, 206, 1719201384409, 1, 1720025401694, 1);
INSERT INTO `course_category` VALUES (221, '河北省公务员', 'file/download/1/1720025415752/default-1017.png', b'0', 15, 206, 1719201384426, 1, 1720025417005, 1);
INSERT INTO `course_category` VALUES (222, '山西省公务员', 'file/download/1/1720025432670/default-1017.png', b'0', 16, 206, 1719201384443, 1, 1720025433533, 1);
INSERT INTO `course_category` VALUES (223, '辽宁省公务员', 'file/download/1/1720025449278/default-1017.png', b'0', 17, 206, 1719201384460, 1, 1720025450575, 1);
INSERT INTO `course_category` VALUES (224, '吉林省公务员', 'file/download/1/1720025465114/default-1017.png', b'0', 18, 206, 1719201384484, 1, 1720025466002, 1);
INSERT INTO `course_category` VALUES (225, '黑龙江省公务员', 'file/download/1/1720025485564/default-1017.png', b'0', 19, 206, 1719201384503, 1, 1720025486477, 1);
INSERT INTO `course_category` VALUES (226, '安徽省公务员', 'file/download/1/1720026560942/default-1017.png', b'0', 20, 206, 1719201384520, 1, 1720026561939, 1);
INSERT INTO `course_category` VALUES (227, '陕西省公务员', 'file/download/1/1720026577295/default-1017.png', b'0', 21, 206, 1719201384537, 1, 1720026578363, 1);
INSERT INTO `course_category` VALUES (228, '湖北省公务员', 'file/download/1/1720026593315/default-1017.png', b'0', 22, 206, 1719201384555, 1, 1720026594165, 1);
INSERT INTO `course_category` VALUES (229, '青海省公务员', 'file/download/1/1720026610014/default-1017.png', b'0', 23, 206, 1719201384572, 1, 1720026610841, 1);
INSERT INTO `course_category` VALUES (230, '湖南省公务员', 'file/download/1/1720026624170/default-1017.png', b'0', 24, 206, 1719201384590, 1, 1720026624988, 1);
INSERT INTO `course_category` VALUES (231, '云南省公务员', 'file/download/1/1720026641689/default-1017.png', b'0', 25, 206, 1719201384607, 1, 1720026642953, 1);
INSERT INTO `course_category` VALUES (232, '江西省公务员', 'file/download/1/1720026658051/default-1017.png', b'0', 26, 206, 1719201384623, 1, 1720026659113, 1);
INSERT INTO `course_category` VALUES (233, '福建省公务员', 'file/download/1/1720026673922/default-1017.png', b'0', 27, 206, 1719201384639, 1, 1720026674824, 1);
INSERT INTO `course_category` VALUES (234, '甘肃省公务员', 'file/download/1/1720026689234/default-1017.png', b'0', 28, 206, 1719201384657, 1, 1720026690316, 1);
INSERT INTO `course_category` VALUES (235, '贵州省公务员', 'file/download/1/1720026709102/default-1017.png', b'0', 29, 206, 1719201384673, 1, 1720026709863, 1);
INSERT INTO `course_category` VALUES (236, '宁夏省公务员', 'file/download/1/1720026722847/default-1017.png', b'0', 30, 206, 1719201384689, 1, 1720026723988, 1);
INSERT INTO `course_category` VALUES (237, '内蒙古公务员', 'https://s3.cn-north-1.amazonaws.com.cn/qingshuxuetang.com/YXK/pageSetting/courseGroup/default/default-3031.png', b'0', 31, 206, 1719201384705, 1, 1720026744102, 1);
INSERT INTO `course_category` VALUES (238, '新疆公务员', 'file/download/1/1720026761723/default-1017.png', b'0', 32, 206, 1719201384722, 1, 1720026762608, 1);
INSERT INTO `course_category` VALUES (239, '西藏公务员', 'file/download/1/1720026777506/default-1017.png', b'0', 33, 206, 1719201384738, 1, 1720026778254, 1);
INSERT INTO `course_category` VALUES (240, '广西公务员', 'file/download/1/1720026793835/default-1017.png', b'0', 34, 206, 1719201384754, 1, 1720026795074, 1);

-- ----------------------------
-- Table structure for cron
-- ----------------------------
DROP TABLE IF EXISTS `cron`;
CREATE TABLE `cron`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务标识',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '定时规则',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '调用路径',
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '调用凭证',
  `json` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '入参',
  `version` bigint NULL DEFAULT 0 COMMENT '执行版本',
  `state` bit(1) NULL DEFAULT b'1' COMMENT '任务状态：是否启用',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tag`(`tag` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cron
-- ----------------------------
INSERT INTO `cron` VALUES (1, '清除过期临时文件夹', 'delete_temp_file', '0 */10 * * * ?', '/pt/task/deleteTempFile', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MX0.SpJrriGA4UQQ6gpy4sN6evMsTWjaxaqjjcUhschZl_4', NULL, 0, b'0');
INSERT INTO `cron` VALUES (2, '自动化测试', 'auto_code_test', '0 */10 * * * ?', '/pt/task/autoCodeTest', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MX0.SpJrriGA4UQQ6gpy4sN6evMsTWjaxaqjjcUhschZl_4', NULL, 0, b'0');

-- ----------------------------
-- Table structure for crud
-- ----------------------------
DROP TABLE IF EXISTS `crud`;
CREATE TABLE `crud`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `img` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `normal_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '普通文本',
  `number` bigint NULL DEFAULT NULL COMMENT '数字',
  `select_box` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下拉框',
  `rich_text` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '富文本|',
  `file` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件地址',
  `create_time` bigint UNSIGNED NULL DEFAULT NULL COMMENT '创建时间',
  `create_user_id` bigint NULL DEFAULT NULL COMMENT '创建人',
  `update_time` bigint UNSIGNED NULL DEFAULT NULL COMMENT '更新时间',
  `update_user_id` bigint NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '模板表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of crud
-- ----------------------------
INSERT INTO `crud` VALUES (1, 'file/download/1/1720449595068/favicon.ico', 'xxx_a_a', 7, '1', '<p>22</p>', 'file/download/1/1720449600610/favicon.ico', 1699980222619, 1, 1720449602276, 1);

-- ----------------------------
-- Table structure for library
-- ----------------------------
DROP TABLE IF EXISTS `library`;
CREATE TABLE `library`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `position` json NOT NULL COMMENT '职位',
  `position_id` bigint NOT NULL COMMENT '职位id|',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '封面',
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件',
  `sort` int NOT NULL COMMENT '排序',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建用户ID',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '相关题库' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of library
-- ----------------------------
INSERT INTO `library` VALUES (2, '[71, 72]', 72, '初级会计_万人模考_第一轮.docx', 'file/download/1/1720016968353/下载.png', 'file/download/1/1720017038671/初级会计_万人模考_第一轮.docx', 1, 1719362388143, 1, 1720017040147, 1);

-- ----------------------------
-- Table structure for low_code_permission
-- ----------------------------
DROP TABLE IF EXISTS `low_code_permission`;
CREATE TABLE `low_code_permission`  (
  `permission_id` bigint NOT NULL COMMENT '接口id',
  `param` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '参数格式内容',
  `param_valid` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '参数检验规则',
  `json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '执行的json',
  `response` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '响应格式内容',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态：优化用',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建人id',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新人id',
  PRIMARY KEY (`permission_id`) USING BTREE,
  UNIQUE INDEX `permission_id`(`permission_id` ASC) USING BTREE,
  CONSTRAINT `Low_code_permission_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '低代码接口信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of low_code_permission
-- ----------------------------
INSERT INTO `low_code_permission` VALUES (661, NULL, NULL, '', '', b'1', 1699011949217, 1, 1699011949217, 1);
INSERT INTO `low_code_permission` VALUES (662, '{}', '{}', '({\n  \"[]\": {\n    Test: {\n      \"content~\": req.content,\n      \"@order\": \"create_time-\"\n    },\n    User: {\n      \"id@\": \"/Test/create_user_id\",\n      \"@column\": \"id,name,account\"\n    },\n    \"query\": 2,\n    \"page\": (req.pageIndex || 1) - 1,\n    \"count\": req.pageSize || 100\n  },\n  \"total@\": \"/[]/total\"\n})', '({\n  list: res[\'[]\'].map(e => ({\n    ...e.Test,\n    user: e.User\n  })),\n  total: res.total\n})', b'1', 1691042689456, 1, 1691042689456, 1);
INSERT INTO `low_code_permission` VALUES (663, '{\n    \"content\": \"内容\"\n}', '{\n    \"content\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    }\n}', '({\n  Test: {\n    ...req,\n    create_time: new Date().getTime(),\n    create_user_id: system.id\n  },\n  \"sql@\": {\n    with: false,\n    from: \"Test\",\n    \"Test\": {\n      \"@column\": \"id\"\n    }\n  },\n  \"Test:del\": {\n    \"@method\": \"delete\",\n    \"status\": \"0\"\n  },\n  \"Test:put\": {\n    \"@method\": \"put\",\n    \"status\": 0,\n    \"status{}\": \"=1\",\n    \"@combine\": \"status{}\"\n  },\n  \"@explain\": true\n})', 'res', b'1', 1691043725112, 1, 1691043725112, 1);
INSERT INTO `low_code_permission` VALUES (685, '{}', '{}', '(() => {\n  let time = new Date().getTime()\n  return {\n    Test: {\n      content: req.content,\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    },\n    \"Test[]\": req.children.map(e => ({\n      ...e,\n      create_time: time,\n      \"create_user_id@\": \"Test/id\",\n      update_time: time,\n      update_user_id: system.id\n    }))\n  }\n})()', 'res', b'1', 1694437822683, 1, 1694437822683, 1);
INSERT INTO `low_code_permission` VALUES (687, '', '', NULL, NULL, b'1', 1699372819979, 1, 1699372819979, 1);
INSERT INTO `low_code_permission` VALUES (688, '{\n  \"id\": \"主键\",\n  \"des-textArea\": \"描述\",\n  \"img-img\": \"图片\",\n  \"url\": \"跳转\",\n  \"sort-number\": \"排序\"\n}', '{\n  \"id\": {\n    \"type\": \"number\",\n    \"required\": \"true\"\n  },\n  \"des\": {\n    \"type\": \"string\",\n    \"required\": \"true\"\n  },\n  \"img\": {\n    \"type\": \"string\",\n    \"required\": \"true\"\n  },\n  \"url\": {\n    \"type\": \"string\",\n    \"required\": \"true\"\n  },\n  \"sort\": {\n    \"type\": \"number\",\n    \"required\": \"false\"\n  }\n}', '({\n  Banner: {\n    ...req,\n    update_time: new Date().getTime(),\n    update_user_id: system.id\n  }\n})', 'res', b'1', 1720596553364, 1, 1720596553364, 1);
INSERT INTO `low_code_permission` VALUES (689, '{\n    \"pageIndex\": \"页码\",\n    \"pageSize\": \"一页条数\",\n    \"des\": \"描述\",\n    \"create_time-dateTimeRange\": \"创建时间\"\n}', '{\n    \"pageIndex\": {\n        \"required\": false,\n        \"type\": \"number\"\n    },\n    \"pageSize\": {\n        \"required\": false,\n        \"type\": \"number\"\n    }\n}', '({\n  \"[]\": {\n    Banner: {\n      \"des~\": !!req.des ? req.des : null,\n      \"create_time&{}\": (() => {\n        if (!req.create_time) return null;\n        let startTime = req.create_time[0];\n        let endTime = req.create_time[1];\n        let rules = [];\n        if (startTime) rules.push(`>=${moment(startTime).valueOf()}`)\n        if (endTime) rules.push(`<=${moment(endTime).valueOf()}`)\n        return rules.length > 0 ? rules.join(\',\') : null\n      })(),\n      \"@order\": \"sort+,create_time-\",\n    },\n    \"query\": 2,\n    \"page\": (req.pageIndex || 1) - 1,\n    \"count\": req.pageSize || 10\n  },\n  \"total@\": \"/[]/total\"\n})', '({\n  list: res[\'[]\']?.map(e => e.Banner),\n  total: res.total\n})', b'1', 1699372641825, 1, 1699372641825, 1);
INSERT INTO `low_code_permission` VALUES (690, '{\n    \"id\": \"主键id\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": true\n    }\n}', '({\n  Banner: req\n})', 'res', b'1', 1699011418874, 1, 1699011418874, 1);
INSERT INTO `low_code_permission` VALUES (691, '{\n    \"des-textArea\": \"描述\",\n    \"img-img\": \"图片\",\n    \"sort-number\": \"排序\",\n    \"url\": \"跳转\"\n}', '{\n    \"des\": {\n        \"type\": \"string\",\n        \"required\": \"true\",\n        \"colProps\": {\n            \"span\": 2\n        }\n    },\n    \"img\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"url\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": \"false\"\n    }\n}', '(() => {\n  let time = new Date().getTime()\n  return {\n    Banner: {\n      ...req,\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1720596458508, 1, 1720596458508, 1);
INSERT INTO `low_code_permission` VALUES (709, '{\n    \"normal_text\": \"普通文本\",\n    \"number-number\": \"数字\",\n    \"select_box-select\": \"下拉框\",\n    \"rich_text-editor\": \"富文本\",\n    \"img-img\": \"图片地址\",\n    \"file-file\": \"文件地址\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": \"true\"\n    },\n    \"img\": {\n        \"type\": \"string\",\n        \"required\": true\n    },\n    \"file\": {\n        \"type\": \"string\",\n        \"required\": true\n    },\n    \"number\": {\n        \"type\": \"number\",\n        \"required\": true\n    },\n    \"rich_text\": {\n        \"type\": \"string\",\n        \"required\": true,\n        \"width\": 1200\n    },\n    \"select_box\": {\n        \"type\": \"string\",\n        \"required\": true,\n        \"select\": [\n            {\n                \"label\": \"是\",\n                \"value\": \"1\"\n            },\n            {\n                \"label\": \"否\",\n                \"value\": \"0\"\n            }\n        ]\n    },\n    \"normal_text\": {\n        \"type\": \"string\",\n        \"required\": true\n    }\n}', '(() => {\n  let nulls = [];\n  for (let key in req)\n    if (req[key] === null) nulls.push(key)\n  return {\n    Crud: {\n      ...req,\n      \"@null\": nulls.join(\",\"),\n      update_time: new Date().getTime(),\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1720029222326, 1, 1720029222326, 1);
INSERT INTO `low_code_permission` VALUES (710, '{\n    \"pageIndex\": \"页码\",\n    \"pageSize\": \"一页条数\",\n    \"normal_text\": \"普通文本\",\n    \"create_time-dateRange\": \"创建时间\"\n}', '{\n    \"pageIndex\": {\n        \"required\": false,\n        \"type\": \"number\"\n    },\n    \"pageSize\": {\n        \"required\": false,\n        \"type\": \"number\"\n    }\n}', '({\n  \"[]\": {\n    Crud: {\n      \"normal_text~\": !!req.normal_text ? req.normal_text : null,\n      \"create_time&{}\": (() => {\n        if (!req.create_time) return null;\n        let startTime = req.create_time[0];\n        let endTime = req.create_time[1];\n        let rules = [];\n        if (startTime) rules.push(`>=${moment(startTime).valueOf()}`)\n        if (endTime) rules.push(`<=${moment(endTime).valueOf()}`)\n        return rules.length > 0 ? rules.join(\',\') : null\n      })(),\n      \"@order\": \"create_time-\",\n    },\n    \"query\": 2,\n    \"page\": (req.pageIndex || 1) - 1,\n    \"count\": req.pageSize || 10\n  },\n  \"total@\": \"/[]/total\"\n})', '({\n  list: res[\'[]\']?.map(e => e.Crud),\n  total: res.total\n})', b'1', 1718189065109, 1, 1718189065109, 1);
INSERT INTO `low_code_permission` VALUES (711, '{\n    \"id\": \"主键id\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": true\n    }\n}', '({\n  Crud: req\n})', 'res', b'1', 1699979577844, 1, 1699979577844, 1);
INSERT INTO `low_code_permission` VALUES (712, '{\n    \"normal_text\": \"普通文本\",\n    \"number-number\": \"数字\",\n    \"select_box-select\": \"下拉框\",\n    \"rich_text-editor\": \"富文本\",\n    \"img-img\": \"图片地址\",\n    \"file-file\": \"文件地址\"\n}', '{\n    \"img\": {\n        \"type\": \"string\",\n        \"required\": true\n    },\n    \"file\": {\n        \"type\": \"string\",\n        \"required\": true\n    },\n    \"number\": {\n        \"type\": \"number\",\n        \"required\": true\n    },\n    \"rich_text\": {\n        \"type\": \"string\",\n        \"required\": true,\n        \"width\":1200\n    },\n    \"select_box\": {\n        \"type\": \"string\",\n        \"required\": true,\n        \"select\": [\n            {\n                \"label\": \"是\",\n                \"value\": \"1\"\n            },\n            {\n                \"label\": \"否\",\n                \"value\": \"0\"\n            }\n        ]\n    },\n    \"normal_text\": {\n        \"type\": \"string\",\n        \"required\": true\n    }\n}', '(() => {\n  let time = new Date().getTime()\n  return {\n    Crud: {\n      ...req,\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1720029241854, 1, 1720029241854, 1);
INSERT INTO `low_code_permission` VALUES (744, '{\n  \"id\": \"ID\",\n  \"pid-select\": \"父id\",\n  \"icon_img-img\": \"图标\",\n  \"name\": \"名称\",\n  \"sort-number\": \"排序\",\n  \"is_hot-select\": \"是否热门\"\n}', '{\n  \"id\": {\n    \"type\": \"number\",\n    \"required\": true\n  },\n  \"pid\": {\n    \"type\": \"number\",\n    \"required\": true,\n    \"select\": \"/course_category/roots/get\"\n  },\n  \"icon_img\": {\n    \"type\": \"string\",\n    \"required\": \"false\"\n  },\n  \"name\": {\n    \"type\": \"string\",\n    \"required\": true\n  },\n  \"sort\": {\n    \"type\": \"number\",\n    \"required\": true\n  },\n  \"is_hot\": {\n    \"type\": \"boolean\",\n    \"required\": true,\n    \"select\": [{\n        \"label\": \"是\",\n        \"value\": true\n      },\n      {\n        \"label\": \"否\",\n        \"value\": false\n      }\n    ]\n  }\n}', '(() => {\n  let nulls = [];\n  for (let key in req)\n    if (req[key] === null) nulls.push(key)\n  return {\n    Course_category: {\n      ...req,\n      \"@null\": nulls.join(\",\"),\n      update_time: new Date().getTime(),\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1719219216360, 1, 1719219216360, 1);
INSERT INTO `low_code_permission` VALUES (745, '{}', '{}', '({\n  \"[]\": {\n    Course_category: {\n      \"@order\": \"sort+,create_time-\",\n    },\n    \"query\": 2\n  }\n})', '(() => {\r\n    let arr = []\r\n    let list = res[\'[]\']?.map(e => e.Course_category)||[];\r\n    let roots = list.filter(e => {\r\n        e[\'is_hot-render\'] = e.is_hot ? \'是\' : \'否\'\r\n        let flag = e.pid == 0;\r\n        if (flag) {\r\n            return true\r\n        } else {\r\n            arr.push(e)\r\n            return false\r\n        }\r\n    });\r\n    roots.forEach(e => {\r\n        e.children = e.children || []\r\n        for (let i = 0; i < arr.length; i++) {\r\n            let o = arr[i]\r\n            if (o.pid == e.id) {\r\n                arr.splice(i, 1)\r\n                e.children.push(o)\r\n                i--\r\n            }\r\n        }\r\n    })\r\n    return {\r\n        list: roots\r\n    }\r\n})()', b'1', 1719221607907, 1, 1719221607907, 1);
INSERT INTO `low_code_permission` VALUES (746, '{\n    \"id\": \"主键id\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": true\n    }\n}', '({\n  Course_category: req\n})', 'res', b'1', 1718759553477, 1, 1718759553477, 1);
INSERT INTO `low_code_permission` VALUES (747, '{\n    \"pid-select\": \"父id\",\n    \"icon_img-img\": \"图标\",\n    \"name\": \"名称\",\n    \"sort-number\": \"排序\",\n    \"is_hot-select\": \"是否热门\"\n}', '{\n    \"pid\": {\n        \"type\": \"number\",\n        \"required\": true,\n        \"select\": \"/course_category/roots/get\"\n    },\n    \"icon_img\": {\n        \"type\": \"string\",\n        \"required\": \"false\"\n    },\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": true\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": true\n    },\n    \"is_hot\": {\n        \"type\": \"boolean\",\n        \"required\": true,\n        \"select\": [\n            {\n                \"label\": \"是\",\n                \"value\": true\n            },\n            {\n                \"label\": \"否\",\n                \"value\": false\n            }\n        ]\n    }\n}', '(() => {\n  let time = new Date().getTime()\n  return {\n    Course_category: {\n      ...req,\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1719219170079, 1, 1719219170079, 1);
INSERT INTO `low_code_permission` VALUES (749, '{}', '{}', '({\n  \"[]\": {\n    Course_category: {\n      \"pid\": 0,\n      \"@order\": \"sort+,create_time-\",\n      \"@column\": \"id:value,name:label\",\n    },\n    \"query\": 2\n  }\n})', '(() => {\n  let data = res[\'[]\']?.map(e => e.Course_category) || []\n  return [{\n    label: \'根级别\',\n    value: 0\n  }, ...data]\n})()', b'1', 1719218626458, 1, 1719218626458, 1);
INSERT INTO `low_code_permission` VALUES (750, 'null', 'null', NULL, NULL, b'1', 1719221273625, 1, 1719221273625, 1);
INSERT INTO `low_code_permission` VALUES (751, '{\n    \"id\": \"主键\",\n    \"name\": \"姓名\",\n    \"img-img\": \"照片\",\n    \"tag-select\": \"标签\",\n    \"position-cascader\": \"职位\",\n    \"intro-editor\": \"简介\",\n    \"sort-number\": \"排序\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": 1,\n        \"formWidth\": 1000\n    },\n    \"img\": {\n        \"type\": \"string\",\n        \"required\": 1\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"tag\": {\n        \"type\": \"string\",\n        \"required\": \"false\",\n        \"select\": \"/teacher_tag/select/get\",\n        \"multiple\": true\n    },\n    \"intro\": {\n        \"type\": \"string\",\n        \"required\": 1\n    },\n    \"position\": {\n        \"type\": \"string\",\n        \"required\": true,\n        \"cascader\": \"/course_category/cascader/get\"\n    }\n}', '(() => {\n  let nulls = [];\n  for (let key in req)\n    if (req[key] === null) nulls.push(key)\n  return {\n    Teacher: {\n      ...req,\n      position_id: req.position.slice(-1)[0],\n      \"@null\": nulls.join(\",\"),\n      update_time: new Date().getTime(),\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1720594725676, 1, 1720594725676, 1);
INSERT INTO `low_code_permission` VALUES (752, '{\n    \"name-text\": \"姓名\",\n    \"position-cascader\": \"职位\"\n}', '{\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": false\n    },\n    \"position\": {\n        \"type\": \"string\",\n        \"required\": false,\n        \"multiple\": false,\n        \"cascader\": \"/course_category/cascader/get\"\n    }\n}', '({\r\n  \"[]\": {\r\n    Teacher: {\r\n      \"name~\": !!req.name ? req.name : null,\r\n      \"position_id\": (req.position || []).length > 0 ? req.position.slice(-1)[0] : null,\r\n      \"@order\": \"sort+,create_time-\",\r\n      \"Teacher_tag[]\": {\r\n        Teacher_tag: {\r\n          \"id{}@\": \"[]/Teacher/tag\",\r\n          \"@column\": \"name\"\r\n        },\r\n      },\r\n      Course_category: {\r\n        \"id@\": \"[]/Teacher/position_id\",\r\n        \"@column\": \"name\"\r\n      }\r\n    },\r\n    \"query\": 2,\r\n    \"page\": (req.pageIndex || 1) - 1,\r\n    \"count\": req.pageSize || 10\r\n  },\r\n  \"total@\": \"/[]/total\"\r\n})', '({\r\n  list: res[\'[]\']?.map(e => {\r\n    let item = e.Teacher\r\n    item[\'tag-render\'] = item[\'Teacher_tag[]\'].map(o => o.name).join(\",\")\r\n    item[\'position-render\'] = item[\'Course_category\'].name\r\n    delete item[\'Teacher_tag[]\']\r\n    delete item[\'Course_category\']\r\n    return item\r\n  }),\r\n  total: res.total\r\n})', b'1', 1719512656405, 1, 1719512656405, 1);
INSERT INTO `low_code_permission` VALUES (753, '{\n    \"id\": \"主键id\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": true\n    }\n}', '({\n  Teacher: req\n})', 'res', b'1', 1719215753837, 1, 1719215753837, 1);
INSERT INTO `low_code_permission` VALUES (754, '{\n    \"name\": \"姓名\",\n    \"img-img\": \"照片\",\n    \"tag-select\": \"标签\",\n    \"position-cascader\": \"职位\",\n    \"intro-editor\": \"简介\",\n    \"sort-number\": \"排序\"\n}', '{\n  \"name\": {\n    \"type\": \"string\",\n    \"required\": true,\n    \"formWidth\": 1000\n  },\n  \"img\": {\n    \"type\": \"string\",\n    \"required\": true\n  },\n  \"sort\": {\n    \"type\": \"number\",\n    \"required\": true\n  },\n  \"tag\": {\n    \"type\": \"string\",\n    \"required\": false,\n    \"select\": \"/teacher_tag/select/get\",\n    \"multiple\": true\n  },\n  \"intro\": {\n    \"type\": \"string\",\n    \"required\": true\n  },\n  \"position\": {\n    \"type\": \"string\",\n    \"required\": true,\n    \"cascader\": \"/course_category/cascader/get\"\n  }\n}', '(() => {\n  let time = new Date().getTime()\n  return {\n    Teacher: {\n      ...req,\n      position_id: req.position.slice(-1)[0],\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1720575571393, 1, 1720575571393, 1);
INSERT INTO `low_code_permission` VALUES (757, '{\n    \"id\": \"主键\",\n    \"name\": \"名称\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": \"true\"\n    },\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    }\n}', '(() => {\n  let nulls = [];\n  for (let key in req)\n    if (req[key] === null) nulls.push(key)\n  return {\n    Teacher_tag: {\n      ...req,\n      \"@null\": nulls.join(\",\"),\n      update_time: new Date().getTime(),\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1719221313121, 1, 1719221313121, 1);
INSERT INTO `low_code_permission` VALUES (758, '{\n  \"name\": \"名称\"\n}', '{}', '({\n  \"[]\": {\n    Teacher_tag: {\n      \"name~\": !!req.name ? req.name : null,\n      \"@order\": \"update_time-\",\n    },\n    \"query\": 2,\n    \"page\": (req.pageIndex || 1) - 1,\n    \"count\": req.pageSize || 10\n  },\n  \"total@\": \"/[]/total\"\n})', '({\n  list: res[\'[]\']?.map(e => e.Teacher_tag),\n  total: res.total\n})', b'1', 1719362943032, 1, 1719362943032, 1);
INSERT INTO `low_code_permission` VALUES (759, '{\n    \"id\": \"主键id\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": true\n    }\n}', '({\n  Teacher_tag: req\n})', 'res', b'1', 1719221321829, 1, 1719221321829, 1);
INSERT INTO `low_code_permission` VALUES (760, '{\n    \"name\": \"名称\"\n}', '{\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    }\n}', '(() => {\n  let time = new Date().getTime()\n  return {\n    Teacher_tag: {\n      ...req,\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1719221326014, 1, 1719221326014, 1);
INSERT INTO `low_code_permission` VALUES (761, '{}', '{}', '({\n  \"[]\": {\n    Teacher_tag: {\n      \"@order\": \"update_time-\",\n      \"@column\": \"id:value,name:label\",\n    },\n    \"query\": 2\n  }\n})', 'res[\'[]\']?.map(e => {\n  e.Teacher_tag.text = e.Teacher_tag.label\n  return e.Teacher_tag\n}) || []', b'1', 1719504341864, 1, 1719504341864, 1);
INSERT INTO `low_code_permission` VALUES (762, '{}', '{}', '({\n  \"[]\": {\n    Course_category: {\n      \"@column\": \"id:value,pid,name:label\",\n      \"@order\": \"sort+,create_time-\",\n    },\n    \"query\": 2\n  }\n})', '(() => {\r\n    let arr = []\r\n    let list = res[\'[]\']?.map(e => e.Course_category)||[];\r\n    let roots = list.filter(e => {\r\n        let flag = e.pid == 0;\r\n        if (flag) {\r\n            return true\r\n        } else {\r\n            arr.push(e)\r\n            return false\r\n        }\r\n    });\r\n    roots.forEach(e => {\r\n        e.children = e.children || []\r\n        for (let i = 0; i < arr.length; i++) {\r\n            let o = arr[i]\r\n            if (o.pid == e.value) {\r\n                arr.splice(i, 1)\r\n                e.children.push(o)\r\n                i--\r\n            }\r\n        }\r\n    })\r\n    return roots\r\n})()', b'1', 1719301386127, 1, 1719301386127, 1);
INSERT INTO `low_code_permission` VALUES (765, '{\n  \"id\": \"主键\",\n  \"name\": \"名称\",\n  \"position-cascader\": \"职位\",\n  \"img-img\": \"封面\",\n  \"file-file\": \"文件\",\n  \"sort-number\": \"排序\",\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": \"true\"\n    },\n    \"img\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"file\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": \"true\"\n    },\n    \"position\": {\n        \"type\": \"number\",\n        \"required\": \"true\",\n        \"cascader\": \"/course_category/cascader/get\"\n    }\n}', '(() => {\n  let nulls = [];\n  for (let key in req)\n    if (req[key] === null) nulls.push(key)\n  return {\n    Material: {\n      ...req,\n      position_id: req.position.slice(-1)[0],\n      \"@null\": nulls.join(\",\"),\n      update_time: new Date().getTime(),\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1719361465065, 1, 1719361465065, 1);
INSERT INTO `low_code_permission` VALUES (766, '{\n    \"name\": \"名称\",\n    \"position-cascader\": \"职位\"\n}', '{\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": false\n    },\n    \"position\": {\n        \"type\": \"string\",\n        \"required\": false,\n        \"multiple\": false,\n        \"cascader\": \"/course_category/cascader/get\"\n    }\n}', '({\r\n  \"[]\": {\r\n    Material: {\r\n      \"name~\": !!req.name ? req.name : null,\r\n      \"position_id\": (req.position || []).length > 0 ? req.position.slice(-1)[0] : null,\r\n      \"@order\": \"sort+,create_time-\",\r\n      Course_category: {\r\n        \"id@\": \"[]/Material/position_id\",\r\n        \"@column\": \"name\"\r\n      }\r\n    },\r\n    \"query\": 2,\r\n    \"page\": (req.pageIndex || 1) - 1,\r\n    \"count\": req.pageSize || 10\r\n  },\r\n  \"total@\": \"/[]/total\"\r\n})', '({\r\n  list: res[\'[]\']?.map(e => {\r\n    let item = e.Material\r\n    item[\'position-render\'] = item[\'Course_category\'].name\r\n    delete item[\'Course_category\']\r\n    return item\r\n  }),\r\n  total: res.total\r\n})', b'1', 1719512776940, 1, 1719512776940, 1);
INSERT INTO `low_code_permission` VALUES (767, '{\n    \"id\": \"主键id\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": true\n    }\n}', '({\n  Material: req\n})', 'res', b'1', 1719361332221, 1, 1719361332221, 1);
INSERT INTO `low_code_permission` VALUES (768, '{\n  \"name\": \"名称\",\n  \"position-cascader\": \"职位\",\n  \"img-img\": \"封面\",\n  \"file-file\": \"文件\",\n  \"sort-number\": \"排序\",\n}', '{\n    \"img\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"file\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": \"true\"\n    },\n    \"position\": {\n        \"type\": \"number\",\n        \"required\": \"true\",\n        \"cascader\": \"/course_category/cascader/get\"\n    }\n}', '(() => {\n  let time = new Date().getTime()\n  return {\n    Material: {\n      ...req,\n      position_id: req.position.slice(-1)[0],\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1719361473184, 1, 1719361473184, 1);
INSERT INTO `low_code_permission` VALUES (770, '{\n    \"id\": \"主键\",\n    \"name\": \"名称\",\n    \"position-cascader\": \"职位\",\n    \"img-img\": \"封面\",\n    \"file-file\": \"文件\",\n    \"sort-number\": \"排序\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": \"true\"\n    },\n    \"img\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"file\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": \"true\"\n    },\n    \"position\": {\n        \"type\": \"number\",\n        \"required\": \"true\",\n        \"cascader\": \"/course_category/cascader/get\"\n    }\n}', '(() => {\n  let nulls = [];\n  for (let key in req)\n    if (req[key] === null) nulls.push(key)\n  return {\n    Library: {\n      ...req,\n      position_id: req.position.slice(-1)[0],\n      \"@null\": nulls.join(\",\"),\n      update_time: new Date().getTime(),\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1719362319873, 1, 1719362319873, 1);
INSERT INTO `low_code_permission` VALUES (771, '{\n    \"name\": \"名称\",\n    \"position-cascader\": \"职位\"\n}', '{\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": false\n    },\n    \"position\": {\n        \"type\": \"string\",\n        \"required\": false,\n        \"multiple\": false,\n        \"cascader\": \"/course_category/cascader/get\"\n    }\n}', '({\r\n  \"[]\": {\r\n    Library: {\r\n      \"name~\": !!req.name ? req.name : null,\r\n      \"position_id\": (req.position || []).length > 0 ? req.position.slice(-1)[0] : null,\r\n      \"@order\": \"sort+,create_time-\",\r\n      Course_category: {\r\n        \"id@\": \"[]/Library/position_id\",\r\n        \"@column\": \"name\"\r\n      }\r\n    },\r\n    \"query\": 2,\r\n    \"page\": (req.pageIndex || 1) - 1,\r\n    \"count\": req.pageSize || 10\r\n  },\r\n  \"total@\": \"/[]/total\"\r\n})', '({\r\n  list: res[\'[]\']?.map(e => {\r\n    let item = e.Library\r\n    item[\'position-render\'] = item[\'Course_category\'].name\r\n    delete item[\'Course_category\']\r\n    return item\r\n  }),\r\n  total: res.total\r\n})', b'1', 1719512769779, 1, 1719512769779, 1);
INSERT INTO `low_code_permission` VALUES (772, '{\n    \"id\": \"主键id\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": true\n    }\n}', '({\n  Library: req\n})', 'res', b'1', 1719362303186, 1, 1719362303186, 1);
INSERT INTO `low_code_permission` VALUES (773, '{\n    \"name\": \"名称\",\n    \"position-cascader\": \"职位\",\n    \"img-img\": \"封面\",\n    \"file-file\": \"文件\",\n    \"sort-number\": \"排序\"\n}', '{\n    \"img\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"file\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": \"true\"\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": \"true\"\n    },\n    \"position\": {\n        \"type\": \"number\",\n        \"required\": \"true\",\n        \"cascader\": \"/course_category/cascader/get\"\n    }\n}', '(() => {\n  let time = new Date().getTime()\n  return {\n    Library: {\n      ...req,\n      position_id: req.position.slice(-1)[0],\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1719362298894, 1, 1719362298894, 1);
INSERT INTO `low_code_permission` VALUES (775, '{\n    \"id\": \"优惠卷ID\",\n    \"name\": \"名称\",\n    \"quantity-number\": \"数量\",\n    \"start_time-date\": \"开始时间\",\n    \"end_time-date\": \"结束时间\",\n    \"amount_full-number\": \"满额\",\n    \"amount_reduce-number\": \"减额\",\n    \"specified_product-switch\": \"指定商品卷\",\n    \"designated_products\": \"指定商品\",\n    \"on_shelf-switch\": \"上架\",\n    \"sort-number\": \"排序\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": 1,\n        \"formWidth\": 700\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"end_time\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"on_shelf\": {\n        \"type\": \"boolean\",\n        \"required\": 1\n    },\n    \"quantity\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"start_time\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"amount_full\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"amount_reduce\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"received_quantity\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"specified_product\": {\n        \"type\": \"boolean\",\n        \"required\": 1\n    },\n    \"designated_products\": {\n        \"type\": \"string\",\n        \"required\": 0\n    }\n}', '(() => {\n  let nulls = [];\n  let {\n    start_time,\n    end_time\n  } = req\n  delete req.start_time\n  delete req.end_time\n  for (let key in req)\n    if (req[key] === null) nulls.push(key)\n  return {\n    Coupon: {\n      ...req,\n      \"@null\": nulls.join(\",\"),\n      start_time: moment(start_time).startOf(\'day\').valueOf(),\n      end_time: moment(end_time).endOf(\'day\').valueOf(),\n      update_time: new Date().getTime(),\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1720595506660, 1, 1720595506660, 1);
INSERT INTO `low_code_permission` VALUES (776, '{\n    \"pageIndex\": \"页码\",\n    \"pageSize\": \"一页条数\"\n}', '{\n    \"pageIndex\": {\n        \"required\": false,\n        \"type\": \"number\"\n    },\n    \"pageSize\": {\n        \"required\": false,\n        \"type\": \"number\"\n    }\n}', '({\n  \"[]\": {\n    Coupon: {\n      \"@order\": \"sort+,create_time-\",\n    },\n    \"query\": 2,\n    \"page\": (req.pageIndex || 1) - 1,\n    \"count\": req.pageSize || 10\n  },\n  \"total@\": \"/[]/total\"\n})', '({\n  list: res[\'[]\']?.map(e => {\n    let item = e.Coupon\n    item[\'on_shelf-render\'] = item.on_shelf ? \"<a style=\'color:green\'>已上架</a>\" : \"<a style=\'color:red\'>未上架</a>\"\n    item[\'specified_product-render\'] = item.specified_product ? \"商品卷\" : \"通用卷\"\n    return item;\n  }),\n  total: res.total\n})', b'1', 1720595601788, 1, 1720595601788, 1);
INSERT INTO `low_code_permission` VALUES (777, '{\n    \"id\": \"主键id\"\n}', '{\n    \"id\": {\n        \"type\": \"number\",\n        \"required\": true\n    }\n}', '({\n  Coupon: req\n})', 'res', b'1', 1720516111903, 1, 1720516111903, 1);
INSERT INTO `low_code_permission` VALUES (778, '{\n    \"name\": \"名称\",\n    \"quantity-number\": \"数量\",\n    \"start_time-date\": \"开始时间\",\n    \"end_time-date\": \"结束时间\",\n    \"amount_full-number\": \"满额\",\n    \"amount_reduce-number\": \"减额\",\n    \"specified_product-switch\": \"指定商品卷\",\n    \"designated_products\": \"指定商品\",\n    \"on_shelf-switch\": \"上架\",\n    \"sort-number\": \"排序\"\n}', '{\n    \"name\": {\n        \"type\": \"string\",\n        \"required\": 1,\n        \"formWidth\": 700\n    },\n    \"sort\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"end_time\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"on_shelf\": {\n        \"type\": \"boolean\",\n        \"required\": 1\n    },\n    \"quantity\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"start_time\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"amount_full\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"amount_reduce\": {\n        \"type\": \"number\",\n        \"required\": 1\n    },\n    \"specified_product\": {\n        \"type\": \"boolean\",\n        \"required\": 1\n    },\n    \"designated_products\": {\n        \"type\": \"string\",\n        \"required\": 0\n    }\n}', '(() => {\n  let time = new Date().getTime()\n  let {\n    start_time,\n    end_time\n  } = req\n  delete req.start_time\n  delete req.end_time\n  return {\n    Coupon: {\n      ...req,\n      start_time: moment(start_time).startOf(\'day\').valueOf(),\n      end_time: moment(end_time).endOf(\'day\').valueOf(),\n      create_time: time,\n      create_user_id: system.id,\n      update_time: time,\n      update_user_id: system.id\n    }\n  }\n})()', 'res', b'1', 1720595489461, 1, 1720595489461, 1);
INSERT INTO `low_code_permission` VALUES (779, '{}', '{}', '({\n  Crud: {\n    id: 1,\n    \'number+\':1 \n  }\n})', 'res', b'1', 1720766609925, 1, 1720766609925, 1);

-- ----------------------------
-- Table structure for material
-- ----------------------------
DROP TABLE IF EXISTS `material`;
CREATE TABLE `material`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `position` json NOT NULL COMMENT '职位',
  `position_id` bigint NOT NULL COMMENT '职位id|',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '封面',
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件',
  `sort` int NOT NULL COMMENT '排序',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建用户ID',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '考试资料' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of material
-- ----------------------------
INSERT INTO `material` VALUES (1, '[71, 72]', 72, '初级会计《实务》100个卡片考点.pdf', 'file/download/1/1720017097739/下载 (1).png', 'file/download/1/1720017104396/初级会计《实务》100个卡片考点.pdf', 1, 1719361746727, 1, 1720017105533, 1);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` bigint NOT NULL COMMENT '父id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '路由地址',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '组件路径',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `sort` int NULL DEFAULT NULL COMMENT '排序号',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态：无用',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建人id',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新人id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `path`(`path` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (2, 0, '平台用户管理', 'user_manager', '/user_manager', '/page/user_manager', 5, b'1', 0, 0, 1691044219222, 1);
INSERT INTO `menu` VALUES (3, 0, '开发者菜单管理', 'menu_manager', '/menu_manager', '/page/menu_manager', 2, b'1', 0, 0, 1691044219220, 1);
INSERT INTO `menu` VALUES (4, 0, '用户角色管理', 'role_manager', '/role_manager', '/page/role_manager', 3, b'1', 0, 0, 1691044219220, 1);
INSERT INTO `menu` VALUES (29, 0, '接口管理', 'interface_manager', '/interface_manager', '/page/interface_manager', 0, b'1', 1681126774304, 1, 1691044219218, 1);
INSERT INTO `menu` VALUES (31, 0, '模块菜单管理', 'module_manager', '/module_manager', '/page/module_manager', 4, b'1', 1681140612409, 1, 1691044219221, 1);
INSERT INTO `menu` VALUES (34, 0, '数据表管理', 'data_manager', '/data_manager', '/page/data_manager', 1, b'1', 1686151587704, 1, 1691044219219, 1);
INSERT INTO `menu` VALUES (35, 0, '定时任务管理', 'cron_manager', '/cron_manager', '/page/cron_manager', 6, b'1', 1691043960059, 1, 1691044219222, 1);

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模块名',
  `pid` bigint NOT NULL COMMENT '父id',
  `tag` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模块标志',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'url路径',
  `component` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `meta` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '拓展信息',
  `sort` int NULL DEFAULT NULL COMMENT '排序号',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态：无用',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建人id',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新人id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '模块管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES (1, '管理端', 0, 'PC', NULL, NULL, NULL, NULL, b'1', 0, 0, 0, 0);
INSERT INTO `module` VALUES (2, '公众号', 0, 'H5', NULL, NULL, NULL, NULL, b'1', 0, 0, 0, 0);
INSERT INTO `module` VALUES (3, '浏览器', 0, 'WEB', NULL, NULL, NULL, NULL, b'1', 0, 0, 0, 0);
INSERT INTO `module` VALUES (4, '小程序', 0, 'WX', NULL, NULL, NULL, NULL, b'1', 0, 0, 0, 0);
INSERT INTO `module` VALUES (8, 'banner管理', 1, 'PC', '/layout/banner', '/crud/index', '[\'banner\']', 999, b'1', 1699193508769, 1, 1719214883179, 1);
INSERT INTO `module` VALUES (12, '模板管理', 1, 'PC', '/layout/crud', '/crud/index', '[\'crud\']', 999, b'1', 1699979132834, 1, 1719214889521, 1);
INSERT INTO `module` VALUES (13, '课程分类', 1, 'PC', '/layout/course_category', '/crud/index', '[\'course_category\']', 1, b'1', 1718759500134, 1, 1719394478852, 1);
INSERT INTO `module` VALUES (14, '师资团队', 1, 'PC', '/layout/teacher', '/crud/index', '[\'teacher\']', 3, b'1', 1719215375850, 1, 1719394485581, 1);
INSERT INTO `module` VALUES (17, '师资标签', 1, 'PC', '/layout/teacher_tag', '/crud/index', '[\'teacher_tag\']', 2, b'1', 1719221022631, 1, 1719394482023, 1);
INSERT INTO `module` VALUES (18, '报考指南', 1, 'PC', '/layout/guide', '/crud/index', '[\'guide\']', 4, b'1', 1719306485941, 1, 1719394488779, 1);
INSERT INTO `module` VALUES (19, '考试资料', 1, 'PC', '/layout/material', '/crud/index', '[\'material\']', 5, b'1', 1719307494384, 1, 1719394491788, 1);
INSERT INTO `module` VALUES (20, '相关题库', 1, 'PC', '/layout/library', '/crud/index', '[\'library\']', 6, b'1', 1719362151914, 1, 1719394501836, 1);
INSERT INTO `module` VALUES (21, '优惠卷', 1, 'PC', '/layout/coupon', '/crud/index', '[\'coupon\']', 7, b'1', 1720515997830, 1, 1720515997830, 1);

-- ----------------------------
-- Table structure for module_permission
-- ----------------------------
DROP TABLE IF EXISTS `module_permission`;
CREATE TABLE `module_permission`  (
  `module_id` bigint NOT NULL COMMENT '模块id',
  `permission_id` bigint NOT NULL COMMENT '权限id',
  INDEX `module_id`(`module_id` ASC, `permission_id` ASC) USING BTREE,
  INDEX `permission_id`(`permission_id` ASC) USING BTREE,
  CONSTRAINT `Module_permission_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Module_permission_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '模块权限关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of module_permission
-- ----------------------------
INSERT INTO `module_permission` VALUES (8, 687);
INSERT INTO `module_permission` VALUES (8, 688);
INSERT INTO `module_permission` VALUES (8, 689);
INSERT INTO `module_permission` VALUES (8, 690);
INSERT INTO `module_permission` VALUES (8, 691);
INSERT INTO `module_permission` VALUES (12, 708);
INSERT INTO `module_permission` VALUES (12, 708);
INSERT INTO `module_permission` VALUES (12, 708);
INSERT INTO `module_permission` VALUES (12, 709);
INSERT INTO `module_permission` VALUES (12, 709);
INSERT INTO `module_permission` VALUES (12, 709);
INSERT INTO `module_permission` VALUES (12, 709);
INSERT INTO `module_permission` VALUES (12, 710);
INSERT INTO `module_permission` VALUES (12, 710);
INSERT INTO `module_permission` VALUES (12, 710);
INSERT INTO `module_permission` VALUES (12, 710);
INSERT INTO `module_permission` VALUES (12, 711);
INSERT INTO `module_permission` VALUES (12, 711);
INSERT INTO `module_permission` VALUES (12, 711);
INSERT INTO `module_permission` VALUES (12, 711);
INSERT INTO `module_permission` VALUES (12, 712);
INSERT INTO `module_permission` VALUES (12, 712);
INSERT INTO `module_permission` VALUES (12, 712);
INSERT INTO `module_permission` VALUES (12, 712);
INSERT INTO `module_permission` VALUES (13, 743);
INSERT INTO `module_permission` VALUES (13, 743);
INSERT INTO `module_permission` VALUES (13, 744);
INSERT INTO `module_permission` VALUES (13, 744);
INSERT INTO `module_permission` VALUES (13, 745);
INSERT INTO `module_permission` VALUES (13, 745);
INSERT INTO `module_permission` VALUES (13, 746);
INSERT INTO `module_permission` VALUES (13, 746);
INSERT INTO `module_permission` VALUES (13, 747);
INSERT INTO `module_permission` VALUES (13, 747);
INSERT INTO `module_permission` VALUES (13, 749);
INSERT INTO `module_permission` VALUES (14, 750);
INSERT INTO `module_permission` VALUES (14, 751);
INSERT INTO `module_permission` VALUES (14, 752);
INSERT INTO `module_permission` VALUES (14, 753);
INSERT INTO `module_permission` VALUES (14, 754);
INSERT INTO `module_permission` VALUES (17, 756);
INSERT INTO `module_permission` VALUES (17, 757);
INSERT INTO `module_permission` VALUES (17, 758);
INSERT INTO `module_permission` VALUES (17, 759);
INSERT INTO `module_permission` VALUES (17, 760);
INSERT INTO `module_permission` VALUES (19, 764);
INSERT INTO `module_permission` VALUES (19, 765);
INSERT INTO `module_permission` VALUES (19, 766);
INSERT INTO `module_permission` VALUES (19, 767);
INSERT INTO `module_permission` VALUES (19, 768);
INSERT INTO `module_permission` VALUES (20, 769);
INSERT INTO `module_permission` VALUES (20, 770);
INSERT INTO `module_permission` VALUES (20, 771);
INSERT INTO `module_permission` VALUES (20, 772);
INSERT INTO `module_permission` VALUES (20, 773);
INSERT INTO `module_permission` VALUES (21, 774);
INSERT INTO `module_permission` VALUES (21, 775);
INSERT INTO `module_permission` VALUES (21, 776);
INSERT INTO `module_permission` VALUES (21, 777);
INSERT INTO `module_permission` VALUES (21, 778);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接口路径',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接口名字/组名字',
  `pid` bigint NOT NULL COMMENT '父id',
  `is_low_code` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否低代码',
  `controller` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口类名',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接口方法名',
  `open` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否开放',
  `authentication` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否鉴权',
  `auto_test` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否执行自动化mock测试',
  `test_result` bit(1) NOT NULL DEFAULT b'0' COMMENT '测试结果',
  `mock_time` bigint NULL DEFAULT NULL COMMENT '测试时间',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态：无用',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建人id',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新人id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `url`(`url` ASC) USING BTREE,
  INDEX `name`(`name` ASC) USING BTREE,
  INDEX `pid`(`pid` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 780 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (603, 'pt/sql', 'SQL管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (604, 'pt/permission', '接口管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (605, 'pt/permissionRule', '权限规则管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (606, 'pt/file', '文件相关接口', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (607, 'pt/user', '用户管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (608, 'pt/table', '数据表管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (609, 'pt/role', '角色管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (610, 'pt/menu', '菜单管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (611, 'pt/module', '模块管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1690910152301, 0, 1690910152301, 0);
INSERT INTO `permission` VALUES (612, 'pt/permission/all', '获取所有接口树', 604, b'0', 'pr.lanmu.controller.PermissionController', 'all', b'0', b'0', b'0', b'1', 1690943939816, b'1', 1690910152324, 0, 1720753508666, 0);
INSERT INTO `permission` VALUES (613, 'pt/sql/execute', '执行sql', 603, b'0', 'pr.lanmu.controller.SQLController', 'execute', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152324, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (614, 'pt/permission/config', '接口配置信息一键使用默认值', 604, b'0', 'pr.lanmu.controller.PermissionController', 'config', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152324, 0, 1720753508669, 0);
INSERT INTO `permission` VALUES (615, 'pt/permission/del', '删除低代码接口', 604, b'0', 'pr.lanmu.controller.PermissionController', 'del', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152324, 0, 1720753508666, 0);
INSERT INTO `permission` VALUES (616, 'pt/permission/saveAndBingModule', '创建低代码接口并绑定模块', 604, b'0', 'pr.lanmu.controller.PermissionController', 'saveAndBingModule', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152324, 0, 1720753508669, 0);
INSERT INTO `permission` VALUES (617, 'pt/permission/saveTestResult', '保存接口测试结果', 604, b'0', 'pr.lanmu.controller.PermissionController', 'saveTestResult', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152324, 0, 1720753508657, 0);
INSERT INTO `permission` VALUES (618, 'pt/permission/setAutoTest', '设置是否开启自动化测试', 604, b'0', 'pr.lanmu.controller.PermissionController', 'setAutoTest', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152324, 0, 1720753508666, 0);
INSERT INTO `permission` VALUES (619, 'pt/permission/saveRule', '保存mock规则', 604, b'0', 'pr.lanmu.controller.PermissionController', 'saveRule', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152324, 0, 1720753508625, 0);
INSERT INTO `permission` VALUES (620, 'pt/permission/save', '保存低代码接口', 604, b'0', 'pr.lanmu.controller.PermissionController', 'save', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152324, 0, 1720753508655, 0);
INSERT INTO `permission` VALUES (621, 'pt/permissionRule/get', '获取权限规则', 605, b'0', 'pr.lanmu.controller.PermissionRuleController', 'get', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152325, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (622, 'pt/file/upload', '文件上传', 606, b'0', 'pr.lanmu.controller.FileController', 'upload', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152327, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (623, 'pt/user/setRole', '设置用户角色', 607, b'0', 'pr.lanmu.controller.UserController', 'setRole', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152328, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (624, 'pt/table/list', '查询数据表信息', 608, b'0', 'pr.lanmu.controller.TableController', 'list', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152330, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (625, 'pt/role/del', '删除角色', 609, b'0', 'pr.lanmu.controller.RoleController', 'del', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152331, 0, 1720753508615, 0);
INSERT INTO `permission` VALUES (626, 'pt/menu/del', '删除菜单', 610, b'0', 'pr.lanmu.controller.MenuController', 'del', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152333, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (627, 'pt/module/all', '获取所有模块树', 611, b'0', 'pr.lanmu.controller.ModuleController', 'all', b'0', b'0', b'0', b'1', 1698864966939, b'1', 1690910152334, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (628, 'pt/menu/all', '获取全部菜单', 610, b'0', 'pr.lanmu.controller.MenuController', 'all', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152336, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (629, 'pt/permission/copyDir', '接口目录复制', 604, b'0', 'pr.lanmu.controller.PermissionController', 'copyDir', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152337, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (630, 'pt/role/save', '保存角色', 609, b'0', 'pr.lanmu.controller.RoleController', 'save', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152338, 0, 1720753508635, 0);
INSERT INTO `permission` VALUES (631, 'pt/role/list', '分页查询角色', 609, b'0', 'pr.lanmu.controller.RoleController', 'list', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152340, 0, 1720753508631, 0);
INSERT INTO `permission` VALUES (632, 'pt/user/module', '获取登录用户业务模块', 607, b'0', 'pr.lanmu.controller.UserController', 'module', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152342, 0, 1720753508639, 0);
INSERT INTO `permission` VALUES (633, 'pt/module/save', '保存模块', 611, b'0', 'pr.lanmu.controller.ModuleController', 'save', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152346, 0, 1720753508655, 0);
INSERT INTO `permission` VALUES (634, 'pt/user/list', '分页查询用户', 607, b'0', 'pr.lanmu.controller.UserController', 'list', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152347, 0, 1720753508615, 0);
INSERT INTO `permission` VALUES (635, 'pt/role/setModule', '设置模块权限', 609, b'0', 'pr.lanmu.controller.RoleController', 'setModule', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152349, 0, 1720753508631, 0);
INSERT INTO `permission` VALUES (636, 'pt/menu/save', '保存菜单', 610, b'0', 'pr.lanmu.controller.MenuController', 'save', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152348, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (637, 'pt/menu/setSort', '菜单重排序', 610, b'0', 'pr.lanmu.controller.MenuController', 'setSort', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152350, 0, 1720753508605, 0);
INSERT INTO `permission` VALUES (638, 'pt/user/setPassword', '设置用户密码', 607, b'0', 'pr.lanmu.controller.UserController', 'setPassword', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152351, 0, 1720753508629, 0);
INSERT INTO `permission` VALUES (639, 'pt/user/menu', '获取登录用户开发者菜单', 607, b'0', 'pr.lanmu.controller.UserController', 'menu', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152352, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (640, 'pt/role/setMenu', '设置角色菜单', 609, b'0', 'pr.lanmu.controller.RoleController', 'setMenu', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152353, 0, 1720753508639, 0);
INSERT INTO `permission` VALUES (641, 'pt/module/del', '删除模块', 611, b'0', 'pr.lanmu.controller.ModuleController', 'del', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152356, 0, 1720753508647, 0);
INSERT INTO `permission` VALUES (642, 'pt/module/permission', '绑定接口权限', 611, b'0', 'pr.lanmu.controller.ModuleController', 'permission', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152357, 0, 1720753508650, 0);
INSERT INTO `permission` VALUES (643, 'pt/user/del', '删除用户', 607, b'0', 'pr.lanmu.controller.UserController', 'del', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152358, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (644, 'pt/role/all', '全部角色', 609, b'0', 'pr.lanmu.controller.RoleController', 'all', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152358, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (645, 'pt/user/changePassword', '修改自己密码', 607, b'0', 'pr.lanmu.controller.UserController', 'changePassword', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152358, 0, 1720753508625, 0);
INSERT INTO `permission` VALUES (646, 'pt/module/roots', '获取全部平台', 611, b'0', 'pr.lanmu.controller.ModuleController', 'roots', b'0', b'0', b'0', b'1', 1698864909693, b'1', 1690910152361, 0, 1720753508610, 0);
INSERT INTO `permission` VALUES (647, 'pt/user/add', '新增用户', 607, b'0', 'pr.lanmu.controller.UserController', 'add', b'0', b'0', b'0', b'0', NULL, b'1', 1690910152361, 0, 1720753508625, 0);
INSERT INTO `permission` VALUES (648, 'pt/module/root', '获取指定平台模块树', 611, b'0', 'pr.lanmu.controller.ModuleController', 'root', b'0', b'0', b'0', b'1', 1698890339032, b'1', 1690910152361, 0, 1720753508643, 0);
INSERT INTO `permission` VALUES (649, 'pt/user/login', '登录', 607, b'0', 'pr.lanmu.controller.UserController', 'login', b'1', b'0', b'0', b'0', NULL, b'1', 1690910152364, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (651, 'pt/user/userInfo', '获取用户信息', 607, b'0', 'pr.lanmu.controller.UserController', 'userInfo', b'0', b'0', b'0', b'1', 1691138586626, b'1', 1690910904019, 0, 1720753508639, 0);
INSERT INTO `permission` VALUES (661, 'pt/fzmb', '复杂模板', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1691042599657, 1, 1699011949215, 1);
INSERT INTO `permission` VALUES (662, 'pt/fzmb/get', '关联查询', 661, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1691347315945, b'1', 1691042689455, 1, 1691042689455, 1);
INSERT INTO `permission` VALUES (663, 'pt/fzmb/post', '多种操作', 661, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1691043729117, b'1', 1691043283767, 1, 1691043725110, 1);
INSERT INTO `permission` VALUES (664, 'pt/cron', '定时任务管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1691045394046, 0, 1691045394046, 0);
INSERT INTO `permission` VALUES (667, 'pt/cron/delete', '删除定时任务', 664, b'0', 'pr.lanmu.controller.CronController', 'delete', b'0', b'0', b'0', b'0', NULL, b'1', 1691046868304, 0, 1720753508604, 0);
INSERT INTO `permission` VALUES (668, 'pt/cron/list', '分页查询定时任务', 664, b'0', 'pr.lanmu.controller.CronController', 'list', b'0', b'0', b'0', b'0', NULL, b'1', 1691046868341, 0, 1720753508655, 0);
INSERT INTO `permission` VALUES (669, 'pt/cron/put', '修改定时任务', 664, b'0', 'pr.lanmu.controller.CronController', 'put', b'0', b'0', b'0', b'0', NULL, b'1', 1691046868345, 0, 1720753508661, 0);
INSERT INTO `permission` VALUES (670, 'pt/cron/post', '新增定时任务', 664, b'0', 'pr.lanmu.controller.CronController', 'post', b'0', b'0', b'0', b'0', NULL, b'1', 1691046868349, 0, 1720753508669, 0);
INSERT INTO `permission` VALUES (671, 'pt/task', '任务调度管理', 0, b'0', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1691091128679, 0, 1691091128679, 0);
INSERT INTO `permission` VALUES (672, 'pt/task/deleteTempFile', '定期清除临时过期文件夹', 671, b'0', 'pr.lanmu.controller.TaskController', 'deleteTempFile', b'0', b'0', b'0', b'1', 1691388586994, b'1', 1691091128741, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (685, 'pt/fzmb/addOneByOneId/post', '关联新增', 661, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1694437844403, b'1', 1694435380463, 1, 1694437822677, 1);
INSERT INTO `permission` VALUES (686, 'pt/permission/list', '获取所有接口', 604, b'0', 'pr.lanmu.controller.PermissionController', 'list', b'0', b'0', b'0', b'1', 1698892099762, b'1', 1698891292310, 0, 1720753508679, 0);
INSERT INTO `permission` VALUES (687, 'pt/banner', 'banner管理', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1699011370668, 1, 1699372819977, 1);
INSERT INTO `permission` VALUES (688, 'pt/banner/put', '修改', 687, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1699011683113, b'1', 1699011370674, 1, 1720596553362, 1);
INSERT INTO `permission` VALUES (689, 'pt/banner/get', '列表', 687, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1699409215829, b'1', 1699011370681, 1, 1699372641822, 1);
INSERT INTO `permission` VALUES (690, 'pt/banner/delete', '删除', 687, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1699011583013, b'1', 1699011370684, 1, 1699011418872, 1);
INSERT INTO `permission` VALUES (691, 'pt/banner/post', '新增', 687, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1699409206818, b'1', 1699011370689, 1, 1720596458506, 1);
INSERT INTO `permission` VALUES (692, 'pt/module/getModuleTableInfo', '查询模块绑定的表和关联的接口及接口的校验规则', 611, b'0', 'pr.lanmu.controller.ModuleController', 'getModuleTableInfo', b'0', b'0', b'0', b'1', 1699193002634, b'1', 1699154144805, 0, 1720753508645, 0);
INSERT INTO `permission` VALUES (708, 'pt/crud', '增删改查', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1699422494651, 1, 1699422494651, 1);
INSERT INTO `permission` VALUES (709, 'pt/crud/put', '修改', 708, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011683113, b'1', 1699422494653, 1, 1720029222323, 1);
INSERT INTO `permission` VALUES (710, 'pt/crud/get', '列表', 708, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1712045024065, b'1', 1699422494657, 1, 1718189065093, 1);
INSERT INTO `permission` VALUES (711, 'pt/crud/delete', '删除', 708, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011583013, b'1', 1699422494662, 1, 1699979577842, 1);
INSERT INTO `permission` VALUES (712, 'pt/crud/post', '新增', 708, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1712120393480, b'1', 1699422494667, 1, 1720029241851, 1);
INSERT INTO `permission` VALUES (718, 'pt/task/autoCodeTest', '自动化测试', 671, b'0', 'pr.lanmu.controller.TaskController', 'autoCodeTest', b'0', b'0', b'0', b'0', NULL, b'1', 1699984303043, 0, 1720753508581, 0);
INSERT INTO `permission` VALUES (719, 'pt/cron/loads', '已装载定时任务', 664, b'0', 'pr.lanmu.controller.CronController', 'loads', b'0', b'0', b'0', b'0', NULL, b'1', 1712044977641, 0, 1720753508645, 0);
INSERT INTO `permission` VALUES (743, 'pt/course_category', '课程分类管理', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1718759537558, 1, 1718759537558, 1);
INSERT INTO `permission` VALUES (744, 'pt/course_category/put', '修改', 743, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011683113, b'1', 1718759537564, 1, 1719219216357, 1);
INSERT INTO `permission` VALUES (745, 'pt/course_category/get', '列表', 743, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712045024065, b'1', 1718759537582, 1, 1719221607905, 1);
INSERT INTO `permission` VALUES (746, 'pt/course_category/delete', '删除', 743, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011583013, b'1', 1718759537587, 1, 1718759553475, 1);
INSERT INTO `permission` VALUES (747, 'pt/course_category/post', '新增', 743, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712120393480, b'1', 1718759537592, 1, 1719219170077, 1);
INSERT INTO `permission` VALUES (749, 'pt/course_category/roots/get', '根级列表', 743, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1718766974431, 1, 1719218626455, 1);
INSERT INTO `permission` VALUES (750, 'pt/teacher', '师资团队管理', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1719215514187, 1, 1719221273621, 1);
INSERT INTO `permission` VALUES (751, 'pt/teacher/put', '修改', 750, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011683113, b'1', 1719215514189, 1, 1720594725674, 1);
INSERT INTO `permission` VALUES (752, 'pt/teacher/get', '列表', 750, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712045024065, b'1', 1719215514208, 1, 1719512656397, 1);
INSERT INTO `permission` VALUES (753, 'pt/teacher/delete', '删除', 750, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011583013, b'1', 1719215514211, 1, 1719215753835, 1);
INSERT INTO `permission` VALUES (754, 'pt/teacher/post', '新增', 750, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712120393480, b'1', 1719215514214, 1, 1720575571391, 1);
INSERT INTO `permission` VALUES (756, 'pt/teacher_tag', '师资标签管理', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1719221261398, 1, 1719221261398, 1);
INSERT INTO `permission` VALUES (757, 'pt/teacher_tag/put', '修改', 756, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011683113, b'1', 1719221261404, 1, 1719221313117, 1);
INSERT INTO `permission` VALUES (758, 'pt/teacher_tag/get', '列表', 756, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712045024065, b'1', 1719221261414, 1, 1719362943030, 1);
INSERT INTO `permission` VALUES (759, 'pt/teacher_tag/delete', '删除', 756, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011583013, b'1', 1719221261419, 1, 1719221321827, 1);
INSERT INTO `permission` VALUES (760, 'pt/teacher_tag/post', '新增', 756, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712120393480, b'1', 1719221261426, 1, 1719221326011, 1);
INSERT INTO `permission` VALUES (761, 'pt/teacher_tag/select/get', '下拉列表', 756, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1719221937365, 1, 1719504341850, 1);
INSERT INTO `permission` VALUES (762, 'pt/course_category/cascader/get', '级联选择', 743, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1719301183176, 1, 1719301386125, 1);
INSERT INTO `permission` VALUES (764, 'pt/material', '考试资料管理', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1719307946358, 1, 1719307946358, 1);
INSERT INTO `permission` VALUES (765, 'pt/material/put', '修改', 764, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011683113, b'1', 1719307946372, 1, 1719361465062, 1);
INSERT INTO `permission` VALUES (766, 'pt/material/get', '列表', 764, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712045024065, b'1', 1719307946403, 1, 1719512776929, 1);
INSERT INTO `permission` VALUES (767, 'pt/material/delete', '删除', 764, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011583013, b'1', 1719307946414, 1, 1719361332218, 1);
INSERT INTO `permission` VALUES (768, 'pt/material/post', '新增', 764, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712120393480, b'1', 1719307946433, 1, 1719361473181, 1);
INSERT INTO `permission` VALUES (769, 'pt/library', '相关题库管理', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1719362171831, 1, 1719362171831, 1);
INSERT INTO `permission` VALUES (770, 'pt/library/put', '修改', 769, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011683113, b'1', 1719362171834, 1, 1719362319871, 1);
INSERT INTO `permission` VALUES (771, 'pt/library/get', '列表', 769, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712045024065, b'1', 1719362171841, 1, 1719512769775, 1);
INSERT INTO `permission` VALUES (772, 'pt/library/delete', '删除', 769, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011583013, b'1', 1719362171846, 1, 1719362303184, 1);
INSERT INTO `permission` VALUES (773, 'pt/library/post', '新增', 769, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712120393480, b'1', 1719362171851, 1, 1719362298891, 1);
INSERT INTO `permission` VALUES (774, 'pt/coupon', '优惠卷管理', 0, b'1', NULL, NULL, b'0', b'0', b'0', b'0', NULL, b'1', 1720516039896, 1, 1720516039896, 1);
INSERT INTO `permission` VALUES (775, 'pt/coupon/put', '修改', 774, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011683113, b'1', 1720516039897, 1, 1720595506656, 1);
INSERT INTO `permission` VALUES (776, 'pt/coupon/get', '列表', 774, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712045024065, b'1', 1720516039905, 1, 1720595601787, 1);
INSERT INTO `permission` VALUES (777, 'pt/coupon/delete', '删除', 774, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1699011583013, b'1', 1720516039909, 1, 1720516111899, 1);
INSERT INTO `permission` VALUES (778, 'pt/coupon/post', '新增', 774, b'1', NULL, NULL, b'0', b'0', b'0', b'0', 1712120393480, b'1', 1720516039912, 1, 1720595489458, 1);
INSERT INTO `permission` VALUES (779, 'pt/fzmb/zizeng/put', '字段自增', 661, b'1', NULL, NULL, b'0', b'0', b'0', b'1', 1720766541307, b'1', 1720750670815, 1, 1720766609924, 1);

-- ----------------------------
-- Table structure for permission_mock_rule
-- ----------------------------
DROP TABLE IF EXISTS `permission_mock_rule`;
CREATE TABLE `permission_mock_rule`  (
  `id` bigint NOT NULL COMMENT 'id',
  `rule` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'mock规则',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态：无用',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建人id',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新人id',
  PRIMARY KEY (`id`) USING BTREE,
  CONSTRAINT `permission_mock_rule_ibfk_1` FOREIGN KEY (`id`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '接口mock数据规则表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of permission_mock_rule
-- ----------------------------
INSERT INTO `permission_mock_rule` VALUES (613, '{\n    \"sql\": null\n}', b'1', 1690912862251, 1, 1690912862251, 1);
INSERT INTO `permission_mock_rule` VALUES (648, '{\n    \"id\": 1\n}', b'1', 1698890292586, 22, 1698890292586, 22);
INSERT INTO `permission_mock_rule` VALUES (663, '{\"content\":\"@cname\"}', b'1', 1691043323632, 1, 1691043398249, 1);
INSERT INTO `permission_mock_rule` VALUES (685, '{\n  \"content\": \"@cname\",\n  \"children|1-10\": [{\n    \"content\": \"@cname\"\n  }]\n}', b'1', 1694435545811, 1, 1694436441521, 1);
INSERT INTO `permission_mock_rule` VALUES (686, '{}', b'1', 1698891607528, 22, 1698891607528, 22);
INSERT INTO `permission_mock_rule` VALUES (688, '{\"id\":\"/[1-9][0-9]{0,2}/\",\"des\":\"@cname\",\"img\":\"@cname\",\"url\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\"}', b'1', 1699011370680, 1, 1699413407908, 1);
INSERT INTO `permission_mock_rule` VALUES (689, '{}', b'1', 1699011619048, 1, 1699011619048, 1);
INSERT INTO `permission_mock_rule` VALUES (690, '{\n  \"id|1-10\": 1\n}', b'1', 1699011370688, 1, 1699011370688, 1);
INSERT INTO `permission_mock_rule` VALUES (691, '{\"des\":\"@cname\",\"img\":\"@cname\",\"url\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\"}', b'1', 1699011370693, 1, 1699413336733, 1);
INSERT INTO `permission_mock_rule` VALUES (692, '{}', b'1', 1699154179938, 1, 1699154179938, 1);
INSERT INTO `permission_mock_rule` VALUES (709, '{\"id\":\"/[1-9][0-9]{0,2}/\",\"img\":\"@cname\",\"file\":\"@cname\",\"number\":\"/[1-9][0-9]{0,2}/\",\"rich_text\":\"@cname\",\"select_box\":\"@cname\",\"normal_text\":\"@cname\"}', b'1', 1699422494657, 1, 1699979527253, 1);
INSERT INTO `permission_mock_rule` VALUES (710, '{}', b'1', 1699422494661, 1, 1699422494661, 1);
INSERT INTO `permission_mock_rule` VALUES (711, '{\n  \"id|1-10\": 1\n}', b'1', 1699422494666, 1, 1699422494666, 1);
INSERT INTO `permission_mock_rule` VALUES (712, '{\"img\":\"@cname\",\"file\":\"@cname\",\"number\":\"/[1-9][0-9]{0,2}/\",\"rich_text\":\"@cname\",\"select_box\":\"@cname\",\"normal_text\":\"@cname\"}', b'1', 1699422494671, 1, 1699979523076, 1);
INSERT INTO `permission_mock_rule` VALUES (744, '{\"id\":\"/[1-9][0-9]{0,2}/\",\"pid\":\"/[1-9][0-9]{0,2}/\",\"icon\":\"@cname\",\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"is_hot\":\"1\"}', b'1', 1718759537581, 1, 1718759587730, 1);
INSERT INTO `permission_mock_rule` VALUES (745, '{}', b'1', 1718759537586, 1, 1718759537586, 1);
INSERT INTO `permission_mock_rule` VALUES (746, '{\n  \"id|1-10\": 1\n}', b'1', 1718759537590, 1, 1718759537590, 1);
INSERT INTO `permission_mock_rule` VALUES (747, '{\"pid\":\"/[1-9][0-9]{0,2}/\",\"icon\":\"@cname\",\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"is_hot\":\"1\"}', b'1', 1718759537595, 1, 1718759581160, 1);
INSERT INTO `permission_mock_rule` VALUES (751, '{\"id\":\"/[1-9][0-9]{0,2}/\",\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"tags\":\"@cname\",\"intro\":\"@cname\",\"position\":\"@cname\"}', b'1', 1719215514206, 1, 1719215860956, 1);
INSERT INTO `permission_mock_rule` VALUES (752, '{}', b'1', 1719215514210, 1, 1719215514210, 1);
INSERT INTO `permission_mock_rule` VALUES (753, '{\n  \"id|1-10\": 1\n}', b'1', 1719215514214, 1, 1719215514214, 1);
INSERT INTO `permission_mock_rule` VALUES (754, '{\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"tags\":\"@cname\",\"intro\":\"@cname\",\"position\":\"@cname\"}', b'1', 1719215514218, 1, 1719215855194, 1);
INSERT INTO `permission_mock_rule` VALUES (757, '{\"id\":\"/[1-9][0-9]{0,2}/\",\"name\":\"@cname\"}', b'1', 1719221261412, 1, 1719221292834, 1);
INSERT INTO `permission_mock_rule` VALUES (758, '{}', b'1', 1719221261417, 1, 1719221261417, 1);
INSERT INTO `permission_mock_rule` VALUES (759, '{\n  \"id|1-10\": 1\n}', b'1', 1719221261424, 1, 1719221261424, 1);
INSERT INTO `permission_mock_rule` VALUES (760, '{\"name\":\"@cname\"}', b'1', 1719221261432, 1, 1719221289091, 1);
INSERT INTO `permission_mock_rule` VALUES (765, '{\"id\":\"/[1-9][0-9]{0,2}/\",\"img\":\"@cname\",\"file\":\"@cname\",\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"position\":\"/[1-9][0-9]{0,2}/\"}', b'1', 1719307946398, 1, 1719361244118, 1);
INSERT INTO `permission_mock_rule` VALUES (766, '{}', b'1', 1719307946413, 1, 1719307946413, 1);
INSERT INTO `permission_mock_rule` VALUES (767, '{\n  \"id|1-10\": 1\n}', b'1', 1719307946432, 1, 1719307946432, 1);
INSERT INTO `permission_mock_rule` VALUES (768, '{\"img\":\"@cname\",\"file\":\"@cname\",\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"position\":\"/[1-9][0-9]{0,2}/\"}', b'1', 1719307946438, 1, 1719361239642, 1);
INSERT INTO `permission_mock_rule` VALUES (770, '{\"id\":\"/[1-9][0-9]{0,2}/\",\"img\":\"@cname\",\"file\":\"@cname\",\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"position\":\"/[1-9][0-9]{0,2}/\"}', b'1', 1719362171840, 1, 1719362171840, 1);
INSERT INTO `permission_mock_rule` VALUES (771, '{}', b'1', 1719362171845, 1, 1719362171845, 1);
INSERT INTO `permission_mock_rule` VALUES (772, '{\n  \"id|1-10\": 1\n}', b'1', 1719362171849, 1, 1719362171849, 1);
INSERT INTO `permission_mock_rule` VALUES (773, '{\"img\":\"@cname\",\"file\":\"@cname\",\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"position\":\"/[1-9][0-9]{0,2}/\"}', b'1', 1719362171855, 1, 1719362171855, 1);
INSERT INTO `permission_mock_rule` VALUES (775, '{\"id\":\"/[1-9][0-9]{0,2}/\",\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"end_time\":\"/[1-9][0-9]{0,2}/\",\"on_shelf\":\"1\",\"quantity\":\"/[1-9][0-9]{0,2}/\",\"start_time\":\"/[1-9][0-9]{0,2}/\",\"amount_full\":\"/[1-9][0-9]{0,2}/\",\"amount_reduce\":\"/[1-9][0-9]{0,2}/\",\"received_quantity\":\"/[1-9][0-9]{0,2}/\",\"specified_product\":\"1\",\"designated_products\":\"@cname\"}', b'1', 1720516039904, 1, 1720518707475, 1);
INSERT INTO `permission_mock_rule` VALUES (776, '{}', b'1', 1720516039907, 1, 1720516039907, 1);
INSERT INTO `permission_mock_rule` VALUES (777, '{\n  \"id|1-10\": 1\n}', b'1', 1720516039911, 1, 1720516039911, 1);
INSERT INTO `permission_mock_rule` VALUES (778, '{\"name\":\"@cname\",\"sort\":\"/[1-9][0-9]{0,2}/\",\"end_time\":\"/[1-9][0-9]{0,2}/\",\"on_shelf\":\"Number(1)\",\"quantity\":\"/[1-9][0-9]{0,2}/\",\"start_time\":\"/[1-9][0-9]{0,2}/\",\"amount_full\":\"/[1-9][0-9]{0,2}/\",\"amount_reduce\":\"/[1-9][0-9]{0,2}/\",\"received_quantity\":\"/[1-9][0-9]{0,2}/\",\"specified_product\":\"Number(1)\",\"designated_products\":\"[]\"}', b'1', 1720516039915, 1, 1720537019898, 1);
INSERT INTO `permission_mock_rule` VALUES (779, '{\n}', b'1', 1720751980895, 1, 1720753577407, 1);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态：无用',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建人id',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新人id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '管理员', b'1', 0, 0, 1718423083515, 1);

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色id',
  `menu_id` bigint NOT NULL COMMENT '菜单id',
  INDEX `role_id`(`role_id` ASC, `menu_id` ASC) USING BTREE,
  INDEX `menu_id`(`menu_id` ASC) USING BTREE,
  CONSTRAINT `Role_menu_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Role_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色菜单关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES (1, 2);
INSERT INTO `role_menu` VALUES (1, 3);
INSERT INTO `role_menu` VALUES (1, 4);
INSERT INTO `role_menu` VALUES (1, 29);
INSERT INTO `role_menu` VALUES (1, 31);
INSERT INTO `role_menu` VALUES (1, 34);
INSERT INTO `role_menu` VALUES (1, 35);

-- ----------------------------
-- Table structure for role_module
-- ----------------------------
DROP TABLE IF EXISTS `role_module`;
CREATE TABLE `role_module`  (
  `role_id` bigint NOT NULL COMMENT '角色id',
  `module_id` bigint NOT NULL COMMENT '模块id',
  INDEX `role_id`(`role_id` ASC, `module_id` ASC) USING BTREE,
  INDEX `module_id`(`module_id` ASC) USING BTREE,
  CONSTRAINT `Role_module_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Role_module_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色模块关系表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_module
-- ----------------------------
INSERT INTO `role_module` VALUES (1, 8);
INSERT INTO `role_module` VALUES (1, 13);
INSERT INTO `role_module` VALUES (1, 14);
INSERT INTO `role_module` VALUES (1, 17);
INSERT INTO `role_module` VALUES (1, 19);
INSERT INTO `role_module` VALUES (1, 20);
INSERT INTO `role_module` VALUES (1, 21);

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '照片',
  `position` json NULL COMMENT '职位',
  `position_id` bigint NULL DEFAULT NULL COMMENT '职位id|',
  `tag` json NULL COMMENT '标签',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '简介|',
  `sort` bigint NOT NULL COMMENT '排序',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建用户ID',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '师资管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES (2, '胡冬', 'https://s3.cn-north-1.amazonaws.com.cn/qingshuxuetang.com/YXK/teacher/teacher-6.png', '[71, 72]', 72, '[5, 4]', '<p>高分通过税务师、会计师、审计师、注册纳税筹划师，被财政部列为安徽省职称考试金银榜优秀考生，有多年的考证类课程与实操类课程辅导经验，曾在大中型企业及上市公司任职财务经理，具有深湛的财税理论基础及丰富的财税实战经验，擅长用图表“翻译”教材中的重难点。</p>', 1, 1719222282714, 1, 1719394459425, 1);

-- ----------------------------
-- Table structure for teacher_tag
-- ----------------------------
DROP TABLE IF EXISTS `teacher_tag`;
CREATE TABLE `teacher_tag`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建用户ID',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '师资标签表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teacher_tag
-- ----------------------------
INSERT INTO `teacher_tag` VALUES (1, '金牌讲师', 1719221361014, 1, 1719221854679, 1);
INSERT INTO `teacher_tag` VALUES (3, '资深博士', 1719276129782, 1, 1719276129782, 1);
INSERT INTO `teacher_tag` VALUES (4, '逻辑性强', 1719287780869, 1, 1719287780869, 1);
INSERT INTO `teacher_tag` VALUES (5, '接地气', 1719287791217, 1, 1719287791217, 1);
INSERT INTO `teacher_tag` VALUES (6, '专业培训讲师', 1719287806019, 1, 1719287806019, 1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名字',
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `role_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '角色：逗号分隔',
  `wx_union_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '开放平台绑定小程序和公众后每个用户特有的id',
  `wx_gzh_open_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公众号用户的openId',
  `wx_xcx_open_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '小程序用户的openId',
  `domain` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录域：PC/WX/H5，多个域逗号分隔',
  `admin` bit(1) NOT NULL DEFAULT b'0' COMMENT '超管',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态：无用',
  `create_time` bigint NOT NULL COMMENT '创建时间',
  `create_user_id` bigint NOT NULL COMMENT '创建人id',
  `update_time` bigint NOT NULL COMMENT '更新时间',
  `update_user_id` bigint NOT NULL COMMENT '更新人id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account`(`account` ASC) USING BTREE,
  INDEX `account_2`(`account` ASC, `name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '蓝姆c', 'lanmuc', '$2a$10$PBG26WFYhiUfuZXdTXmmD./eC7THuAsy7espJzQHaYOKf7HJufWbG', '1', NULL, NULL, NULL, 'PC,WX,H5', b'1', b'1', 0, 0, 1671282469802, 1);

-- ----------------------------
-- View structure for table_info
-- ----------------------------
DROP VIEW IF EXISTS `table_info`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `table_info` AS select `a`.`TABLE_NAME` AS `table_name`,any_value(`a`.`TABLE_COMMENT`) AS `table_comment`,group_concat(`information_schema`.`b`.`COLUMN_NAME` order by `information_schema`.`b`.`ORDINAL_POSITION` ASC separator ',') AS `param`,json_arrayagg(json_object('name',`information_schema`.`b`.`COLUMN_NAME`,'type',(case when (`information_schema`.`b`.`DATA_TYPE` = 'bit') then 'boolean' when ((`information_schema`.`b`.`DATA_TYPE` like '%char%') or (`information_schema`.`b`.`DATA_TYPE` like '%text%') or (`information_schema`.`b`.`DATA_TYPE` = 'json')) then 'string' when ((`information_schema`.`b`.`DATA_TYPE` like '%int%') or (`information_schema`.`b`.`DATA_TYPE` like '%decimal%')) then 'number' else '' end),'des',`information_schema`.`b`.`COLUMN_COMMENT`,'sort',`information_schema`.`b`.`ORDINAL_POSITION`)) AS `info`,json_objectagg(`information_schema`.`b`.`COLUMN_NAME`,`information_schema`.`b`.`COLUMN_COMMENT`) AS `param_info`,json_objectagg(`information_schema`.`b`.`COLUMN_NAME`,(case when (`information_schema`.`b`.`DATA_TYPE` = 'bit') then 'Number(1)' when ((`information_schema`.`b`.`DATA_TYPE` like '%char%') or (`information_schema`.`b`.`DATA_TYPE` like '%text%')) then '@cname' when (`information_schema`.`b`.`DATA_TYPE` = 'json') then '[]' when ((`information_schema`.`b`.`DATA_TYPE` like '%int%') or (`information_schema`.`b`.`DATA_TYPE` like '%decimal%')) then '/[1-9][0-9]{0,2}/' else '' end)) AS `param_mock`,json_objectagg(`information_schema`.`b`.`COLUMN_NAME`,json_object('type',(case when (`information_schema`.`b`.`DATA_TYPE` = 'bit') then 'boolean' when ((`information_schema`.`b`.`DATA_TYPE` like '%char%') or (`information_schema`.`b`.`DATA_TYPE` like '%text%') or (`information_schema`.`b`.`DATA_TYPE` = 'json')) then 'string' when ((`information_schema`.`b`.`DATA_TYPE` like '%int%') or (`information_schema`.`b`.`DATA_TYPE` like '%decimal%')) then 'number' else '' end),'required',if((`information_schema`.`b`.`IS_NULLABLE` = 'YES'),0,1))) AS `param_valid` from ((select `information_schema`.`tables`.`TABLE_CATALOG` AS `TABLE_CATALOG`,`information_schema`.`tables`.`TABLE_SCHEMA` AS `TABLE_SCHEMA`,`information_schema`.`tables`.`TABLE_NAME` AS `TABLE_NAME`,`information_schema`.`tables`.`TABLE_TYPE` AS `TABLE_TYPE`,`information_schema`.`tables`.`ENGINE` AS `ENGINE`,`information_schema`.`tables`.`VERSION` AS `VERSION`,`information_schema`.`tables`.`ROW_FORMAT` AS `ROW_FORMAT`,`information_schema`.`tables`.`TABLE_ROWS` AS `TABLE_ROWS`,`information_schema`.`tables`.`AVG_ROW_LENGTH` AS `AVG_ROW_LENGTH`,`information_schema`.`tables`.`DATA_LENGTH` AS `DATA_LENGTH`,`information_schema`.`tables`.`MAX_DATA_LENGTH` AS `MAX_DATA_LENGTH`,`information_schema`.`tables`.`INDEX_LENGTH` AS `INDEX_LENGTH`,`information_schema`.`tables`.`DATA_FREE` AS `DATA_FREE`,`information_schema`.`tables`.`AUTO_INCREMENT` AS `AUTO_INCREMENT`,`information_schema`.`tables`.`CREATE_TIME` AS `CREATE_TIME`,`information_schema`.`tables`.`UPDATE_TIME` AS `UPDATE_TIME`,`information_schema`.`tables`.`CHECK_TIME` AS `CHECK_TIME`,`information_schema`.`tables`.`TABLE_COLLATION` AS `TABLE_COLLATION`,`information_schema`.`tables`.`CHECKSUM` AS `CHECKSUM`,`information_schema`.`tables`.`CREATE_OPTIONS` AS `CREATE_OPTIONS`,`information_schema`.`tables`.`TABLE_COMMENT` AS `TABLE_COMMENT` from `information_schema`.`TABLES` where (`information_schema`.`tables`.`TABLE_SCHEMA` = database())) `a` left join `information_schema`.`COLUMNS` `b` on(((`a`.`TABLE_NAME` = `information_schema`.`b`.`TABLE_NAME`) and (`a`.`TABLE_SCHEMA` = `information_schema`.`b`.`TABLE_SCHEMA`)))) where (`a`.`TABLE_TYPE` = 'BASE TABLE') group by `a`.`TABLE_NAME` order by `a`.`TABLE_NAME`;

SET FOREIGN_KEY_CHECKS = 1;
