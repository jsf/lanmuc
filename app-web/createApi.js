import request from 'request';
import fs from 'fs';
import path from 'path';

const createApi = (BASE_URL) => {
    //平台拉去所有接口,写入文件
    request({
        url: `${BASE_URL}pt/permission/list`,
        method: "POST",
        json: true,
        headers: {
            "content-type": "application/json",
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MX0.SpJrriGA4UQQ6gpy4sN6evMsTWjaxaqjjcUhschZl_4"
        },
        body: {}
    }, (error, response, {body}) => {
        //body遍历一行一行写入当前目录下的src/utils/api.js文件
        let str = '';
        body.forEach(({url, name}) => {
            let arr = url.split('/');
            arr.shift();
            let key = arr.join('_');
            str += `/**\n* ${name}\n* @type {string}\n*/\nexport const api_${key} = '${arr.join("/")}';\n`
        })
        fs.writeFileSync(path.resolve('', './src/utils/api.js'), str);
    });
}
createApi(process.argv.pop());