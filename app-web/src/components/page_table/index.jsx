import './index.less';
import {ProTable} from "@ant-design/pro-components";

export default (props) => {
    const {
        request,
        actions,
        showIndex = false,
        operation,
        rowKey = 'id',
        pagination,
        exportData,
        operationWidth,
        hideOperation = false,
        exportDisabled = false,
        importData,
        batchDel,
    } = props;

    let rowSelection = props.rowSelection ? {...props.rowSelection} : false;
    let actionsList = actions ? [...actions] : [];

    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const columns = props.columns ? [...props.columns] : [];
    if (showIndex) {
        columns.unshift({
            title: '序号',
            dataIndex: 'index',
            valueType: 'index',
            width: 80,
        });
    }

    if (operation && !hideOperation) {
        columns.push({
            title: '操作',
            valueType: 'option',
            render: operation,
            align: 'center',
            fixed: 'right',
            width: operationWidth ?? 'auto',
        });
    }


    return (
        <ProTable
            {...props}
            rowKey={rowKey}
            rowSelection={rowSelection}
            pagination={
                pagination === false
                    ? false
                    : {
                        defaultPageSize: 10,
                        showSizeChanger: true,
                    }
            }
            options={false}
            toolbar={{
                actions: actionsList,
            }}
            className={`${props.search === false && 'table-no-query-gutter'} ${
                props.className
            }`}
            search={
                props.search === false
                    ? false
                    : {
                        layout: 'vertical',
                        collapseRender: false,
                        collapsed: false,
                        span: {
                            xs: 12,
                            sm: 12,
                            md: 8,
                            lg: 8,
                            xl: 6,
                            xxl: 6,
                        },
                        ...props.search,
                    }
            }
            columns={columns}
            request={(params) => {
                const query = {...params};
                if (pagination !== false) query.pageIndex = params.current;
                delete query.current;
                setSelectedRowKeys([]);
                return request(query);
            }}
        />
    );
};
