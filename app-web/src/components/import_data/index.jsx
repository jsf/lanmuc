import React, {Fragment, useEffect, useState} from 'react';
import {Button, Form, Typography} from 'antd';
import {FileExcelOutlined, InfoCircleOutlined} from '@ant-design/icons';
import PageModal from '@components/page_modal';
import Upload from '@components/upload';


export default (props) => {
    const {
        title = '导入数据',
        template,
        action,
        accept,
        size,
        requested,
        getTemplate,
        tip,
        templateAction,
        query,
        disabled,
    } = props;

    const [form] = Form.useForm();

    const [visible, setVisible] = useState(false);

    useEffect(() => {
        if (!visible) {
            form.resetFields();
        }
    }, [visible]);

    return (
        <Fragment>
            {!!props.children && (
                <div
                    onClick={() => {
                        if (disabled) return;
                        setVisible(true);
                    }}
                >
                    {props.children}
                </div>
            )}
            <PageModal
                title={title}
                open={visible}
                width={700}
                hideOk
                onCancel={() => setVisible(false)}
            >
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <div
                        style={{display: 'flex', alignItems: 'center', marginBottom: 20}}
                    >
                        {tip || (
                            <Fragment>
                                <InfoCircleOutlined style={{marginRight: 4}}/>
                                <Typography.Text>
                                    上传前请先下载模版，按照要求将内容录入到模版中，该导入为覆盖导入，请谨慎操作！
                                </Typography.Text>
                            </Fragment>
                        )}
                    </div>
                    <Upload
                        onCancel={() => setVisible(false)}
                        action={action}
                        query={query}
                        accept={accept}
                        size={size}
                        requested={requested}
                    />
                    {!templateAction
                        ? (!!template || !!getTemplate) && (
                        <Button
                            style={{marginTop: 20}}
                            onClick={getTemplate}
                            icon={<FileExcelOutlined/>}
                            href={template}
                        >
                            下载模版
                        </Button>
                    )
                        : templateAction}
                </div>
            </PageModal>
        </Fragment>
    );
};
