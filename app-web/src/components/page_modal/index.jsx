import {Button, Modal} from 'antd'
import './index.less'

export default (props) => {
    const {onOk, onCancel, hideOk, confirmLoading, okText} = props
    return (
        <Modal
            destroyOnClose
            width={760}
            maskClosable={false}
            footer={[
                <Button key='cancel' onClick={onCancel}>
                    {hideOk ? '关闭' : '取消'}
                </Button>,
                !hideOk && (
                    <Button key='ok' type='primary' onClick={onOk} loading={confirmLoading}>
                        {okText || '提交'}
                    </Button>
                )
            ]}
            {...props}>
            {props.children}
        </Modal>
    )
}
