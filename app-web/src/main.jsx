import ReactDOM from 'react-dom/client'
import './main.less'
import router from "@routers/index.jsx";
import {RouterProvider} from "react-router";
import {Provider} from 'react-redux'
import store from '@utils/store.js'

//禁止无意义的框架警告
const consoleError = console.error;
console.error = (...args) => {
    const message = args.join(' ');
    if (
        typeof message === 'string' &&
        message.startsWith('Warning: ') &&
        !message.includes('Warning: componentWillReceiveProps has been renamed')
    ) {
        return;
    }
    consoleError.apply(console, args);
};

ReactDOM.createRoot(document.getElementById('root')).render(<>
        <Provider store={store}>
            <RouterProvider router={router}/>
        </Provider>
    </>
)