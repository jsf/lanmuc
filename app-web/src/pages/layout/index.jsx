import {PageContainer, ProLayout} from '@ant-design/pro-components';
import {Avatar, Dropdown, Space} from 'antd';
import {UserOutlined} from '@ant-design/icons';
import {useNavigate} from "react-router-dom";
import {Outlet} from "react-router";
import {HOME_PAGE_TITLE} from "@utils/constants.js";
import {api_user_module} from "@utils/api.js";
import {addInterfaceDataRouter} from "@routers/index.jsx";
import {useDispatch} from "react-redux";

export default () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [open, setOpen] = useState(false);
    const [userDetail, setUserDetail] = useState({});
    const [menus, setMenus] = useState([]);
    const getMenus = async () => {
        let menus = sessionStorage.getItem('menus');
        if (menus) return JSON.parse(menus)
        else {
            menus = await request(api_user_module, {tag: "PC"})
            sessionStorage.setItem('menus', JSON.stringify(menus))
            return menus
        }
    }
    useEffect(() => {
        getMenus().then((menus) => {
            let first = addInterfaceDataRouter(menus)
            if (location.pathname === '/') navigate(first.path, {replace: true})
            setMenus(menus)
        })
        return () => {
        }
    }, []);
    return (
        <div
            id="test-pro-layout"
            style={{
                height: '100%',
            }}>
            <ProLayout
                siderWidth={216}
                title={HOME_PAGE_TITLE}
                fixSiderbar={true}
                logo="/favicon.ico"
                route={{
                    routes: menus
                }}
                actionsRender={() => [<>
                    <Space align="center" size="large">
                        <Dropdown
                            menu={{
                                items: [
                                    {
                                        key: '2',
                                        label: (
                                            <a
                                                onClick={() => {
                                                    localStorage.removeItem('token');
                                                    sessionStorage.clear();
                                                    navigate('/login', {replace: true});
                                                }}
                                            >
                                                退出登录
                                            </a>
                                        ),
                                    },
                                ],
                            }}
                            placement="bottom"
                            arrow>
                            <Space>
                                <Avatar size={32} icon={<UserOutlined/>}/>
                                <span>{localStorage.getItem('name')}</span>
                            </Space>
                        </Dropdown>
                    </Space>
                </>]}
                menuItemRender={(item, dom) => (
                    <a onClick={() => {
                        if (item.path === location.pathname) return;
                        navigate(item.path, {replace: false})
                    }}>
                        {dom}
                    </a>
                )}
                layout="mix"
                location={{
                    pathname: location.pathname,
                }}>
                <PageContainer>
                    <Outlet/>
                </PageContainer>
            </ProLayout>
        </div>
    );
}
