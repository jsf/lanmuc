import {LockOutlined, UserOutlined} from '@ant-design/icons';
import {LoginForm, ProFormCheckbox, ProFormText,} from '@ant-design/pro-components';
import {Form, Tabs, Typography} from 'antd';
import {LOGIN_PAGE_TITLE, TOKEN_NAME} from '@utils/constants.js';
import {api_user_login, api_user_module} from '@utils/api.js';
import {useNavigate} from 'react-router-dom';
import styles from './index.module.less';

export default () => {
    const navigate = useNavigate();
    const [loginType, setLoginType] = useState('account');

    const [form] = Form.useForm()

    useEffect(() => {
        const isRemember = localStorage.getItem('isRemember')
        if (isRemember) {
            const account = localStorage.getItem('account')
            const password = localStorage.getItem('password')
            form.setFieldsValue({
                account,
                password,
                isRemember
            })
        }
    }, [])

    return (
        <div className={styles.login}>
            <LoginForm
                title={LOGIN_PAGE_TITLE}
                form={form}
                onFinish={async (e) => {
                    if (e.isRemember) {
                        localStorage.setItem('account', e.account)
                        localStorage.setItem('password', e.password)
                        localStorage.setItem('isRemember', e.isRemember)
                    } else {
                        ['username', 'password', 'isRemember'].forEach(e => localStorage.removeItem(e))
                    }
                    const body = await request(api_user_login, {
                        account: e.account,
                        password: e.password,
                    });
                    if (body.token) {
                        localStorage.setItem(TOKEN_NAME, body.token);
                        localStorage.setItem('name', body.user.name)
                        const menus = await request(api_user_module, {tag: "PC"})
                        sessionStorage.setItem('menus', JSON.stringify(menus))
                        navigate('/', {replace: true});
                    }
                }}>
                <Tabs
                    centered
                    activeKey={loginType}
                    onChange={(activeKey) => setLoginType(activeKey)}
                    items={[
                        {
                            label: `账号密码登录`,
                            key: 'account',
                        },
                    ]}/>
                <ProFormText
                    name="account"
                    fieldProps={{
                        size: 'large',
                        prefix: <UserOutlined className={'prefixIcon'}/>,
                    }}
                    placeholder={'请输入用户名'}
                    rules={[
                        {
                            required: true,
                            message: '请输入用户名!',
                        },
                    ]}/>
                <ProFormText.Password
                    name="password"
                    fieldProps={{
                        size: 'large',
                        prefix: <LockOutlined className={'prefixIcon'}/>,
                    }}
                    placeholder={'请输入密码'}
                    rules={[
                        {
                            required: true,
                            message: '请输入密码！',
                        },
                    ]}/>
                <div style={{marginBlockEnd: 24}}>
                    <ProFormCheckbox noStyle name="isRemember">
                        记住密码
                    </ProFormCheckbox>
                    <Typography.Text
                        type="secondary"
                        style={{
                            float: 'right',
                        }}>
                        忘记密码，请联系管理员
                    </Typography.Text>
                </div>
            </LoginForm>
        </div>
    );
};
