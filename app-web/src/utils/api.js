/**
* SQL管理
* @type {string}
*/
export const api_sql = 'sql';
/**
* 接口管理
* @type {string}
*/
export const api_permission = 'permission';
/**
* 权限规则管理
* @type {string}
*/
export const api_permissionRule = 'permissionRule';
/**
* 文件相关接口
* @type {string}
*/
export const api_file = 'file';
/**
* 用户管理
* @type {string}
*/
export const api_user = 'user';
/**
* 数据表管理
* @type {string}
*/
export const api_table = 'table';
/**
* 角色管理
* @type {string}
*/
export const api_role = 'role';
/**
* 菜单管理
* @type {string}
*/
export const api_menu = 'menu';
/**
* 模块管理
* @type {string}
*/
export const api_module = 'module';
/**
* 获取所有接口树
* @type {string}
*/
export const api_permission_all = 'permission/all';
/**
* 执行sql
* @type {string}
*/
export const api_sql_execute = 'sql/execute';
/**
* 接口配置信息一键使用默认值
* @type {string}
*/
export const api_permission_config = 'permission/config';
/**
* 删除低代码接口
* @type {string}
*/
export const api_permission_del = 'permission/del';
/**
* 创建低代码接口并绑定模块
* @type {string}
*/
export const api_permission_saveAndBingModule = 'permission/saveAndBingModule';
/**
* 保存接口测试结果
* @type {string}
*/
export const api_permission_saveTestResult = 'permission/saveTestResult';
/**
* 设置是否开启自动化测试
* @type {string}
*/
export const api_permission_setAutoTest = 'permission/setAutoTest';
/**
* 保存mock规则
* @type {string}
*/
export const api_permission_saveRule = 'permission/saveRule';
/**
* 保存低代码接口
* @type {string}
*/
export const api_permission_save = 'permission/save';
/**
* 获取权限规则
* @type {string}
*/
export const api_permissionRule_get = 'permissionRule/get';
/**
* 文件上传
* @type {string}
*/
export const api_file_upload = 'file/upload';
/**
* 设置用户角色
* @type {string}
*/
export const api_user_setRole = 'user/setRole';
/**
* 查询数据表信息
* @type {string}
*/
export const api_table_list = 'table/list';
/**
* 删除角色
* @type {string}
*/
export const api_role_del = 'role/del';
/**
* 删除菜单
* @type {string}
*/
export const api_menu_del = 'menu/del';
/**
* 获取所有模块树
* @type {string}
*/
export const api_module_all = 'module/all';
/**
* 获取全部菜单
* @type {string}
*/
export const api_menu_all = 'menu/all';
/**
* 接口目录复制
* @type {string}
*/
export const api_permission_copyDir = 'permission/copyDir';
/**
* 保存角色
* @type {string}
*/
export const api_role_save = 'role/save';
/**
* 分页查询角色
* @type {string}
*/
export const api_role_list = 'role/list';
/**
* 获取登录用户业务模块
* @type {string}
*/
export const api_user_module = 'user/module';
/**
* 保存模块
* @type {string}
*/
export const api_module_save = 'module/save';
/**
* 分页查询用户
* @type {string}
*/
export const api_user_list = 'user/list';
/**
* 设置模块权限
* @type {string}
*/
export const api_role_setModule = 'role/setModule';
/**
* 保存菜单
* @type {string}
*/
export const api_menu_save = 'menu/save';
/**
* 菜单重排序
* @type {string}
*/
export const api_menu_setSort = 'menu/setSort';
/**
* 设置用户密码
* @type {string}
*/
export const api_user_setPassword = 'user/setPassword';
/**
* 获取登录用户开发者菜单
* @type {string}
*/
export const api_user_menu = 'user/menu';
/**
* 设置角色菜单
* @type {string}
*/
export const api_role_setMenu = 'role/setMenu';
/**
* 删除模块
* @type {string}
*/
export const api_module_del = 'module/del';
/**
* 绑定接口权限
* @type {string}
*/
export const api_module_permission = 'module/permission';
/**
* 删除用户
* @type {string}
*/
export const api_user_del = 'user/del';
/**
* 全部角色
* @type {string}
*/
export const api_role_all = 'role/all';
/**
* 修改自己密码
* @type {string}
*/
export const api_user_changePassword = 'user/changePassword';
/**
* 获取全部平台
* @type {string}
*/
export const api_module_roots = 'module/roots';
/**
* 新增用户
* @type {string}
*/
export const api_user_add = 'user/add';
/**
* 获取指定平台模块树
* @type {string}
*/
export const api_module_root = 'module/root';
/**
* 登录
* @type {string}
*/
export const api_user_login = 'user/login';
/**
* 获取用户信息
* @type {string}
*/
export const api_user_userInfo = 'user/userInfo';
/**
* 复杂模板
* @type {string}
*/
export const api_fzmb = 'fzmb';
/**
* 关联查询
* @type {string}
*/
export const api_fzmb_get = 'fzmb/get';
/**
* 多种操作
* @type {string}
*/
export const api_fzmb_post = 'fzmb/post';
/**
* 定时任务管理
* @type {string}
*/
export const api_cron = 'cron';
/**
* 删除定时任务
* @type {string}
*/
export const api_cron_delete = 'cron/delete';
/**
* 分页查询定时任务
* @type {string}
*/
export const api_cron_list = 'cron/list';
/**
* 修改定时任务
* @type {string}
*/
export const api_cron_put = 'cron/put';
/**
* 新增定时任务
* @type {string}
*/
export const api_cron_post = 'cron/post';
/**
* 任务调度管理
* @type {string}
*/
export const api_task = 'task';
/**
* 定期清除临时过期文件夹
* @type {string}
*/
export const api_task_deleteTempFile = 'task/deleteTempFile';
/**
* 关联新增
* @type {string}
*/
export const api_fzmb_addOneByOneId_post = 'fzmb/addOneByOneId/post';
/**
* 获取所有接口
* @type {string}
*/
export const api_permission_list = 'permission/list';
/**
* banner管理
* @type {string}
*/
export const api_banner = 'banner';
/**
* 修改
* @type {string}
*/
export const api_banner_put = 'banner/put';
/**
* 列表
* @type {string}
*/
export const api_banner_get = 'banner/get';
/**
* 删除
* @type {string}
*/
export const api_banner_delete = 'banner/delete';
/**
* 新增
* @type {string}
*/
export const api_banner_post = 'banner/post';
/**
* 查询模块绑定的表和关联的接口及接口的校验规则
* @type {string}
*/
export const api_module_getModuleTableInfo = 'module/getModuleTableInfo';
/**
* 增删改查
* @type {string}
*/
export const api_crud = 'crud';
/**
* 修改
* @type {string}
*/
export const api_crud_put = 'crud/put';
/**
* 列表
* @type {string}
*/
export const api_crud_get = 'crud/get';
/**
* 删除
* @type {string}
*/
export const api_crud_delete = 'crud/delete';
/**
* 新增
* @type {string}
*/
export const api_crud_post = 'crud/post';
/**
* 自动化测试
* @type {string}
*/
export const api_task_autoCodeTest = 'task/autoCodeTest';
/**
* 已装载定时任务
* @type {string}
*/
export const api_cron_loads = 'cron/loads';
/**
* 课程分类管理
* @type {string}
*/
export const api_course_category = 'course_category';
/**
* 修改
* @type {string}
*/
export const api_course_category_put = 'course_category/put';
/**
* 列表
* @type {string}
*/
export const api_course_category_get = 'course_category/get';
/**
* 删除
* @type {string}
*/
export const api_course_category_delete = 'course_category/delete';
/**
* 新增
* @type {string}
*/
export const api_course_category_post = 'course_category/post';
/**
* 根级列表
* @type {string}
*/
export const api_course_category_roots_get = 'course_category/roots/get';
