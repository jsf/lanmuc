import {configureStore, createSlice} from "@reduxjs/toolkit";

export const {reducer: layoutRender, actions: {deal: layoutRenderAction}} = (() => {
    const name = 'layoutRender';
    const setTrue = (state) => {
        state.value = 1
    }
    return createSlice({
        name, initialState: {value: 0},
        reducers: {
            deal: (state, action) => {
                const [method, ...args] = action.payload
                eval(method)(state, ...args)
            }
        }
    })
})()

export default configureStore({
    reducer: {
        layoutRender,
    }
})