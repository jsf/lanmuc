export const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
export const getRef = async (ref) => {
    while (!ref.value) await sleep(100);
    return ref.value;
};
export const getRefs = async (refs) => {
    return await Promise.all([...refs.map(async (ref) => await getRef(ref))]);
};
export const getYMD = (date) => {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
};
export const getYMDHMS = (date) => {
    return `${getYMD(date)} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
};
