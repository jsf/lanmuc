import {message} from 'antd'
import {BASE_URL} from "@utils/constants.js";

export const request = async (url, body = {}, headers = {
    'Content-Type': 'application/json;charset=utf-8'
}) => {
    let token = localStorage.getItem('token')
    if (token) headers.token = token
    let res = await fetch(new Request(BASE_URL + url, {
        method: 'post',
        headers,
        body: JSON.stringify(body)
    }))
    res = await res.json()
    if (res.code.value !== 200) {
        message.warning(res.code.describe)
        if (res.code === 401) {
            await router.replace({path: '/login'});
            throw new Error(`${res.code.describe},请重新登录`)
        }
        throw new Error('调用异常')
    }
    return res.body
}