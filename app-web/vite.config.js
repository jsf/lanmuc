import {defineConfig, splitVendorChunkPlugin} from 'vite'
import react from '@vitejs/plugin-react'
import autoImport from 'unplugin-auto-import/vite'
import viteCompression from "vite-plugin-compression";

export default defineConfig({
    server: {
        port: 8080,
        proxy: {
            '/pt/': {
                target: 'http://127.0.0.1:8000'
            }
        }
    },
    plugins: [react(),
        splitVendorChunkPlugin(),
        autoImport({
            include: [
                /\.[tj]sx?$/,
            ],
            imports: ['react',
                {'@/utils/echarts.js': ['echarts']},
                {'@/utils/common.js': ['sleep', 'getRef', 'getRefs', 'getYMD', 'getYMDHMS']},
                {'@/utils/request.js': ['request']}
            ],
            resolvers: [],
            dts: './auto-import.d.ts'
        }),
        viteCompression({
            threshold: 10240,
        }),
    ],
    resolve: {
        alias: {
            '@': '/src',
            '@components': '/src/components',
            '@pages': '/src/pages',
            '@utils': '/src/utils',
            '@assets': '/src/assets',
            '@routers': '/src/routers',
        },
    },
})
